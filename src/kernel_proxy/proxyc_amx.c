/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/string.h>
#include <net/netlink.h>

#define DEBUG 1
#define IDENT "proxyc"
#include <mcastd/kernel.h>
#include <mcastd/kernel/proxyc_priv.h>

static int mcastdpc_amx_set_intf(struct nlmsghdr *nlh)
{
	struct mdb_iface *iface;
	struct kvar *v0, *v1, *v2;
	const char *key;
	struct mcastdpc_iface i;
	__u8 *a;
	mdb_lock();
	v0 = NLMSG_DATA(nlh);
	v1 = kvar_list_val(v0, KAMX_OBJECT_PROPERTY_PATH);
	key = kvar_string_val(v1) + strlen("MCASTD.Intf.");
	iface = mdb_iface_find(key);
	if (!iface)
		goto leave;
	v1 = kvar_list_val(v0, KAMX_OBJECT_PROPERTY_PARAMS);
	switch (nlh->nlmsg_type) {
	case MCASTDNL_TYPE_NEWOBJ:
		v2 = kvar_map_find(v1, "ClientEnable");
		i.client_enable = kvar_bool_val(v2);
		v2 = kvar_map_find(v1, "Address");
		a = kvar_addr_val(v2);
		i.addr_set = a ? true : false;
		v2 = kvar_map_find(v1, "Status");
		i.is_up = kvar_bool_val(v2);
		break;
	case MCASTDNL_TYPE_DELOBJ:
		i.client_enable = i.addr_set = i.is_up = false;
		break;
	default:
		goto leave;
	}
	mcastdpc_set_intf(iface, &i);
leave:
	mdb_unlock();
	return 0;
}

int mcastdpc_amx_init(void)
{
	mcastd_object_register("MCASTD.Intf.*", mcastdpc_amx_set_intf);
	return 0;
}

void mcastdpc_amx_cleanup(void)
{
	mcastd_object_unregister("MCASTD.Intf.*", mcastdpc_amx_set_intf);
}

