/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <linux/module.h>
#include <linux/version.h>
#include <linux/netfilter.h>
#include <linux/netfilter/x_tables.h>
#include <linux/netfilter_bridge/ebtables.h>

#define IDENT "snooper"
#include <mcastd/kernel.h>

#include "snooper_igmp.h" /* mcastdsn_igmp_tg() */
#include "snooper_mld.h"  /* mcastdsn_mld_tg()  */

#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,35))
static unsigned int
mcastdsn_ebt_snoop_tg(struct sk_buff *skb, const struct xt_target_param *param)
#else
static unsigned int
mcastdsn_ebt_snoop_tg(struct sk_buff *skb, const struct xt_action_param *par)
#endif
{
	int ret = EBT_CONTINUE;

	switch (skb->protocol) {
	case htons(ETH_P_IP):
		ret = (mcastdsn_igmp_tg(skb, par) == NF_DROP ? EBT_DROP : EBT_CONTINUE);
		break;
	case htons(ETH_P_IPV6):
		ret = (mcastdsn_mld_tg(skb, par) == NF_DROP ? EBT_DROP : EBT_CONTINUE);
		break;
	default:
		break;
	}

	return ret;
}

static struct xt_target mcastdsn_ebt_snoop_tg_reg __read_mostly = {
		.name		= "SNOOP",
		.family		= NFPROTO_BRIDGE,
		.target		= mcastdsn_ebt_snoop_tg,
		.targetsize	= 0,
		.checkentry	= NULL,
		.me		= THIS_MODULE
};


int mcastd_snooper_ebt_init(void)
{
	return xt_register_target(&mcastdsn_ebt_snoop_tg_reg);
}

void mcastd_snooper_ebt_cleanup(void)
{
	xt_unregister_target(&mcastdsn_ebt_snoop_tg_reg);
}
