/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/rculist.h> /* list_for_each_entry_rcu */

#define IDENT "whitelist"

#include <mcastd/kernel/debug.h>
#include "access_control.h" /* MCASTD_ACCESS_OK */
#include "bwctrl.h" /* bwctrl_check_bw_available() */
#include "snooper.h" /* MCASTDSN_PROTO_IGMP */
#include "whitelist.h"

static LIST_HEAD(mcastd_whlist);

static bool s_force_igmp_fwd = false;

static struct list_head *s_mcastdwl_whlists[2];

compile_time_assert(MCASTDSN_PROTO_IGMP == 0, mcastdsn_proto_igmp_is_0);
compile_time_assert(MCASTDSN_PROTO_MLD  == 1, mcastdsn_proto_mld_is_1);


/**
 * Register whitelists.
 *
 * Initially the whitelists are empty.
 */
static void mcastdwl_register_whlists(void)
{
	s_mcastdwl_whlists[MCASTDSN_PROTO_IGMP] = &mcastd_whlist;
	/* Whitelist for MLD: not supported yet */
	s_mcastdwl_whlists[MCASTDSN_PROTO_MLD] = NULL;
}

/**
 * Unregister the whitelist for protocol @a proto, i.e. IGMP or MLD.
 *
 * The function:
 * - removes all entries from the list and deletes each entry.
 * - sets the whitelist pointer for the protocol to NULL.
 */
static void mcastdwl_unregister_whlist(enum mcastdsn_proto proto)
{
	struct list_head *wlist = s_mcastdwl_whlists[proto];
	struct mcastdwl_whlist_entry *e, *n;

	if (!wlist)
		return;

	/*
	 * This function previously had following code within the loop:
	 *   list_del_rcu(&e->head);
	 *   call_rcu(&e->rcu, mcastdwl_free_whlist);
	 * This however could lead to a crash. call_rcu() asks the kernel to call
	 * the function mcastdwl_free_whlist() in the near future. But mcastd-core.ko
	 * might already be unloaded by the the time the kernel calls
	 * mcastdwl_free_whlist().
	 * This function is only called when unloading mcastd-core.ko. Then there is
	 * no risk someone else accesses the whitelist. Therefore, clean up the list
	 * with regular list calls instead of rcu calls to avoid the crash.
	 */
	list_for_each_entry_safe(e, n, wlist, head) {
		list_del(&e->head);
		kfree(e);
	}
	s_mcastdwl_whlists[proto] = NULL;
}

static void mcastdwl_unregister_whlists(void)
{
	mcastdwl_unregister_whlist(MCASTDSN_PROTO_IGMP);
	/* Whitelist for MLD: not supported yet */
	//mcastdwl_unregister_whlist(MCASTDSN_PROTO_MLD);
}

/**
 * Free the whitelist entry which is the containing structure of @a rcu.
 * @rcu: pointer to 'rcu' field of struct mcastdwl_whlist_entry instance to be
 *       freed
 *
 * Before doing the actual freeing, the function applies the removal by
 * declaring the affected groups and all their children expired.
 */
static void mcastdwl_free_whlist(struct rcu_head *rcu)
{
	struct mcastdwl_whlist_entry *e = container_of(rcu, struct mcastdwl_whlist_entry, rcu);
	struct mcastdsn_whlist_entry entry;

	DBGMSG("ifindex=%d, id=%d, vlanid=%d, family=%d",
		 e->ifindex, e->id, e->vlanid, e->start_addr.family);

	entry.ifindex = e->ifindex;
	memcpy(&entry.start_addr, &e->start_addr, sizeof(struct mdb_address));
	memcpy(&entry.end_addr,   &e->end_addr  , sizeof(struct mdb_address));

	mcastdsn_free_whlist_apply(&entry);
	kfree(e);
}


/**
 * Add/update whitelist entry for an interface.
 *
 * @iface:      interface to add/update entry for
 * @id:         id for this entry
 * @vlan_id:    VLAN ID of the MC stream
 * @start_addr: start address of the whitelisted range
 * @end_addr:   end address of the whitelisted range
 * @bw:         imputed group bw in bps
 *
 * If the entry already exists, it is replaced. If it doesn't exist, a new one
 * is added.
 */
int mcastdwl_set_whitelist(const struct mdb_iface *const iface, __u16 id,
                           __u16 vlan_id, __u8 *start_addr, __u8 *end_addr,
                           __u8 *src_addr, __u32 grp_bw)
{
	struct mcastdwl_whlist_entry *e, *ne;
	bool found = false;
	size_t addrsize;
	__be32 *saddr32 = (__be32*)start_addr;
	__be32 *eaddr32 = (__be32*)end_addr;

	struct list_head *list;

	if (iface->family == AF_INET)
		list = s_mcastdwl_whlists[MCASTDSN_PROTO_IGMP];
	else
		list = s_mcastdwl_whlists[MCASTDSN_PROTO_MLD];

	if (!list)
		return -EINVAL;

	/*validate the addresses*/
	switch (iface->family) {
	case AF_INET: {
		if (((saddr32[0] & htonl(0xf0000000)) != htonl(0xe0000000)) ||
			((eaddr32[0] & htonl(0xf0000000)) != htonl(0xe0000000)))
			return -EINVAL;
		if (eaddr32[0] < saddr32[0])
			return -EINVAL;
		break;
	}
	case AF_INET6: {
		unsigned int i;
		for (i = 0; i < 4; i++) {
			if(saddr32[i] != eaddr32[i]) {
				if(eaddr32[i] < saddr32[i])
					return -EINVAL;
			}
		}
		break;
	}
	default:
		return -EINVAL;
	}

	list_for_each_entry_rcu(e, list, head) {
		if (e->id == id) {
			found = true;
			break;
		}
	}

	ne = kzalloc(sizeof(struct mcastdwl_whlist_entry), GFP_KERNEL);
	ne->id = id;
	ne->ifindex = iface->ifindex;
	ne->vlanid = vlan_id;
	ne->bw = grp_bw;
	ne->start_addr.family = iface->family;
	ne->end_addr.family = iface->family;
	ne->src_addr.family = iface->family;
	addrsize = mdb_address_size(&ne->start_addr);
	memcpy(ne->start_addr.data, start_addr, addrsize);
	memcpy(ne->end_addr.data, end_addr, addrsize);

	if (src_addr != NULL)
		memcpy(ne->src_addr.data, src_addr, addrsize);
	else
		memset(ne->src_addr.data, 0, addrsize);

	if (found) {
		list_replace_rcu(&e->head, &ne->head);
		call_rcu(&e->rcu, mcastdwl_free_whlist);
	} else {
		list_add_rcu(&ne->head, list);
	}

	return 0;
}

/**
 * Clear one entry of the whitelist.
 * @iface: interface to clear the entry from
 * @id:    id of the entry
 */
void mcastdwl_clear_whitelist(const struct mdb_iface *const iface, __u16 id)
{
	struct mcastdwl_whlist_entry *e;

	struct list_head *list;

	if (iface->family == AF_INET)
		list = s_mcastdwl_whlists[MCASTDSN_PROTO_IGMP];
	else
		list = s_mcastdwl_whlists[MCASTDSN_PROTO_MLD];

	if (!list)
		return;

	list_for_each_entry(e, list,  head) {
		if ((e->id == id) && (e->ifindex == iface->ifindex)) {
			list_del_rcu(&e->head);
			call_rcu(&e->rcu, mcastdwl_free_whlist);
			break;
		}
	}
}


/**
 * Flush every whitelist entry for this interface
 * @iface: interface to flush
 */
void mcastdwl_flush_whitelist(const struct mdb_iface *const iface)
{
	struct mcastdwl_whlist_entry *e, *n;

	struct list_head *list;

	if (iface->family == AF_INET)
		list = s_mcastdwl_whlists[MCASTDSN_PROTO_IGMP];
	else
		list = s_mcastdwl_whlists[MCASTDSN_PROTO_MLD];

	if (!list)
		return;

	list_for_each_entry_safe(e, n, list,  head) {
		if (e->ifindex == iface->ifindex) {
			list_del_rcu(&e->head);
			call_rcu(&e->rcu, mcastdwl_free_whlist);
		}
	}
}

/**
 * Get whitelist from interface
 * @iface:    pointer to the mdb_iface structure to get the list from
 * @ret_list: head of list, the list will be initialized and filled with
 *            elements. This must be freed with mcastdwl_free_whitelist once
 *            you're done with the list.
 */
void mcastdwl_get_whitelist(const struct mdb_iface *const iface,
                            struct list_head *ret_list)
{
	struct mcastdwl_whlist_entry *e, *ne;
	struct list_head *l;

	if (iface->family == AF_INET)
		l = s_mcastdwl_whlists[MCASTDSN_PROTO_IGMP];
	else
		l = s_mcastdwl_whlists[MCASTDSN_PROTO_MLD];

	if (!l)
		return;

	INIT_LIST_HEAD(ret_list);

	rcu_read_lock();
	/*copy the matching elements to the result linked list*/
	list_for_each_entry_rcu(e, l,  head) {
		if ((e->ifindex == iface->ifindex)) {
			ne = kzalloc(sizeof(struct mcastdwl_whlist_entry), GFP_KERNEL);
			ne->id = e->id;
			ne->vlanid = e->vlanid;
			ne->ifindex = iface->ifindex;
			memcpy(&ne->start_addr, &e->start_addr, sizeof(struct mdb_address));
			memcpy(&ne->end_addr, &e->end_addr, sizeof(struct mdb_address));
			memcpy(&ne->src_addr, &e->src_addr, sizeof(struct mdb_address));

			list_add(&ne->head, ret_list);
		}
	}
	rcu_read_unlock();
}

/**
 * Free whitelist allocated when calling mcastdwl_get_whitelist
 * @list: head of whitelist to free.
 */
void mcastdwl_free_whitelist(struct list_head *list)
{
	struct mcastdwl_whlist_entry *e, *n;
	list_for_each_entry_safe(e, n, list, head) {
		list_del(&e->head);
		kfree(e);
	}
}


void mcastdwl_set_force_igmp_fwd(bool force_igmp_fwd)
{
	s_force_igmp_fwd = force_igmp_fwd;
}

bool mcastdwl_get_force_igmp_fwd(void)
{
	return s_force_igmp_fwd;
}

/**
 * mcastdwl_whitelist_empty() - Return 1 if whitelist is empty, else 0
 *
 * Context: non-blocking
 */
int mcastdwl_whitelist_empty(void)
{
	return list_empty(&mcastd_whlist);
}

/**
 * mcastdwl_lookup_bw() - Return MC BW imputed by group
 * @addr: MC address
 *
 * Context: process context
 *
 * Return: bandwidth in bps
 */
__u32 mcastdwl_lookup_bw(__be32 addr)
{
	struct mcastdwl_whlist_entry *whlist;
	__u32 bw = 0;
	__be32 addr_cpu; /* group address in host byte order */

	if (list_empty(&mcastd_whlist))
		return 0;

	addr_cpu = ntohl(addr);

	rcu_read_lock();
	list_for_each_entry_rcu(whlist, &mcastd_whlist, head) {
		if ((addr_cpu >= ntohl(whlist->start_addr.data32[0])) &&
			(addr_cpu <= ntohl(whlist->end_addr.data32[0]))) {
			bw = whlist->bw;
			break;
		}
	}
	rcu_read_unlock();

	return bw;
}

/**
 * mcastdwl_whitelisted() - Check if subscribing on MC stream is allowed
 * @vlanid: ignored
 * @addr: IPv4 MC address
 * @src_addr: IPv4 address of sender of MC stream
 *
 * Context: non-blocking
 *
 * Return:
 * * MCASTD_ACCESS_OK if subscription is allowed
 * * MCASTD_ACCESS_OK_UNAUTHORIZED if subscription is denied because the MC
 *   address is not in the whitelist, but UnauthorizedJoinRequestBehaviour
 *   is true, indicating the HGW should forward the join upstream.
 * * MCASTD_ACCESS_NOT_IN_WHITELIST if subscription is denied because the MC
 *   address is not in the whitelist
 * * MCASTD_ACCESS_EXCEEDS_MAX_BW if subscription is denied because it would
 *   exceed the max MC BW
 */
int mcastdwl_whitelisted(__u16 vlanid, __be32 addr, __be32 src_addr)
{
	struct mcastdwl_whlist_entry *whlist;
	int ret = MCASTD_ACCESS_NOT_IN_WHITELIST;
	__u32 addr_cpu; /* group address in host byte order */

	if (list_empty(&mcastd_whlist)) {
		/* As long as no whitelist is configured, consider everything whitelisted.
		 */
		return MCASTD_ACCESS_OK;
	}
	addr_cpu = ntohl(addr);

	rcu_read_lock();
	list_for_each_entry_rcu(whlist, &mcastd_whlist, head) {
		/* There's no description of how the sourcelist in an IGMP
		 * message should be matched against the source address in the
		 * whitelist. Naively match source addresses in the IGMP message
		 * independent of INCLUDE/EXCLUDE mode*/
		if ((src_addr != INADDR_ANY) && (whlist->src_addr.data32[0] != INADDR_ANY)
			&& (src_addr != whlist->src_addr.data32[0]))
			continue;

		if ((addr_cpu >= ntohl(whlist->start_addr.data32[0])) &&
			(addr_cpu <= ntohl(whlist->end_addr.data32[0]))) {
			if (bwctrl_check_bw_available(whlist->bw)) {
				ret = MCASTD_ACCESS_OK;
			} else {
				ERRMSG("%pI4: no bw available for new stream!", &addr);
				ret = MCASTD_ACCESS_EXCEEDS_MAX_BW;
			}
			break;
		}
	}
	rcu_read_unlock();

	/*
	 * UnauthorizedJoinRequestBehaviour implements the setting "Unauthorized
	 * join request behaviour" of the ITU-T G.988 (OMCI) standard. According
	 * to G.988, if the ONU receives an IGMP join request for a group that
	 * is not authorized in the dynamic address control list table, it
	 * should never honour the request, whatever the value of
	 * UnauthorizedJoinRequestBehaviour. If that setting is true, the ONU
	 * should only forward the request upstream. So mcastd should normally
	 * only forward such a request, and not update the MDB. This
	 * implementation adds the request to the MDB to trigger the mcastd
	 * client/proxy to forward the request upstream. It sets unauthorized to
	 * true to let the listeners on the MDB know that the request is
	 * unauthorized. The mcastd accelerator drivers should not create an
	 * acceleration rule for such a request.
	 */
	if ((ret == MCASTD_ACCESS_NOT_IN_WHITELIST) && mcastdwl_get_force_igmp_fwd()) {
		DBGMSG("Accept due to UnauthorizedJoinRequestBehaviour");
		ret = MCASTD_ACCESS_OK_UNAUTHORIZED;
	}

	DBGMSG("check whitelisted (addr: %pI4 src: %pI4): %s", &addr, &src_addr,
		(ret > MCASTD_ACCESS_OK_UNAUTHORIZED) ? "REJECT" : "ACCEPT");

	return ret;
}

int mcastd_whitelist_init(void)
{
	mcastdwl_register_whlists();
	return 0;
}

void mcastd_whitelist_cleanup(void)
{
	mcastdwl_unregister_whlists();
}

#endif /* CONFIG_MCASTD_CORE_ACCESS_CONTROL */
