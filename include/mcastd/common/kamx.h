/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __MCASTD_COMMON_KAMX_H__
#define __MCASTD_COMMON_KAMX_H__

#include <linux/errno.h>

#include <mcastd/common/netlink.h>
#include <mcastd/common/kvariant.h>

enum kamx_object_property {
    KAMX_OBJECT_PROPERTY_PATH,   // string
    KAMX_OBJECT_PROPERTY_PARAMS, // map<name, variant>
    KAMX_OBJECT_PROPERTY_COUNT
};

enum kamx_notify_property {
    KAMX_NOTIFY_PROPERTY_PATH,   // string
    KAMX_NOTIFY_PROPERTY_TYPE,   // uint
    KAMX_NOTIFY_PROPERTY_NAME,   // string
    KAMX_NOTIFY_PROPERTY_PARAMS, // map<name, variant>
    KAMX_NOTIFY_PROPERTY_COUNT
};

enum kamx_fcall_property {
    KAMX_FCALL_PROPERTY_PATH,   // string
    KAMX_FCALL_PROPERTY_NAME,   // string
    KAMX_FCALL_PROPERTY_RETVAL, // variant
    KAMX_FCALL_PROPERTY_ARGS,   // map<name, variant>
    KAMX_FCALL_PROPERTY_ERROR,  // uint
    KAMX_FCALL_PROPERTY_COUNT
};

enum kamx_notify_type {
    KAMX_NOTIFY_TYPE_MDB = 101,
    KAMX_NOTIFY_TYPE_QUERY,
    KAMX_NOTIFY_TYPE_EXTRA_MIN
};

static inline int kamx_sanity_check(struct nlmsghdr* nlh) {
    struct kvar* var = NLMSG_DATA(nlh);
    if(nlh->nlmsg_len < NLMSG_LENGTH(sizeof(struct kvarhdr))) {
        return -EINVAL;
    }
    if(nlh->nlmsg_len < NLMSG_LENGTH(KVAR_LENGTH(var))) {
        return -EINVAL;
    }
    if(var->hdr.type != KVARTYPE_LIST) {
        return -EINVAL;
    }
    return kvar_validate(var);
}

#endif
