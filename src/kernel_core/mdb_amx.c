/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/string.h>

#include <mcastd/kernel/mdb.h>
#include <mcastd/kernel/kamx.h>

#define IDENT "mdb"
#include <mcastd/kernel/debug.h>

static int mdb_amx_set_intf(struct nlmsghdr *nlh)
{
	struct mdb_iface *iface;
	struct kvar *v0, *v1, *v2;
	const char *key, *c;
	mdb_lock();
	v0 = NLMSG_DATA(nlh);
	v1 = kvar_list_val(v0, KAMX_OBJECT_PROPERTY_PATH);
	key = kvar_string_val(v1) + strlen("MCASTD.Intf.");
	iface = mdb_iface_find(key);
	v1 = kvar_list_val(v0, KAMX_OBJECT_PROPERTY_PARAMS);
	switch (nlh->nlmsg_type) {
	case MCASTDNL_TYPE_NEWOBJ:
		if (!iface) {
			iface = mdb_iface_create(key);
			mdb_iface_setref(iface, MDB_REF_AMX);
		}
		v2 = kvar_map_find(v1, "NetDevIndex");
		iface->ifindex = kvar_int_val(v2);
		v2 = kvar_map_find(v1, "Family");
		c = kvar_string_val(v2);
		if (!strcmp(c?c:"", "ipv6"))
			iface->family = AF_INET6;
		else
			iface->family = AF_INET;
		v2 = kvar_map_find(v1, "Address");
		iface->address.family = kvar_addr_fam(v2);
		if (kvar_addr_val(v2))
			memcpy(iface->address.data, kvar_addr_val(v2),
			       mdb_address_size(&iface->address));
		v2 = kvar_map_find(v1, "MTU");
		iface->mtu = kvar_int_val(v2);
		break;
	case MCASTDNL_TYPE_DELOBJ:
		if (!iface)
			break;
		iface->ifindex = 0;
		iface->address.family = AF_UNSPEC;
		mdb_iface_clrref(iface, MDB_REF_AMX);
		mdb_iface_release(iface);
		break;
	default:
		break;
	}
	mdb_unlock();
	return 0;
}

static int mdb_amx_list_count(struct list_head *list) {
	struct list_head *it;
	int ret = 0;
	list_for_each(it, list)
		ret++;
	return ret;
}

static int mdb_amx_auxcount(enum mdb_level level) {
	int i, ret = 0;
	for (i = 0; i < MDB_REF_COUNT; i++)
		if (mdb_aux_name(level, i))
			ret++;
	return ret;
}

static int mdb_refs_to_kvar(struct kvarbuf *buf, struct kvar *var, __u32 refs)
{
	static const char *refstr[MDB_REF_COUNT] = {
		[MDB_REF_AMX]          = "amx",
		[MDB_REF_SNOOPER_IN]   = "snooper_in",
		[MDB_REF_SNOOPER_EX]   = "snooper_ex",
		[MDB_REF_SNOOPER_PRIV] = "snooper_priv",
		[MDB_REF_PORT]         = "port",
		[MDB_REF_PROXY_CLIENT] = "proxy_client",
		[MDB_REF_MRT]          = "mrt",
		[MDB_REF_USER]         = "user",
		[MDB_REF_VENDOR_ACCEL] = "vendor_accel",
	};
	int i;
	__u16 pos;
	if (unlikely(!var))
		return -EINVAL;
	pos = (unsigned char *)&var->val - buf->data.raw;
	if (unlikely(pos + 1 > buf->size))
		return -ENOBUFS;
	var->hdr.type = KVARTYPE_STRING;
	var->hdr.cnt = 0;
	var->val.s[0] = '\0';
	for (i = 0; i < MDB_REF_COUNT; i++) {
		if (!(refs & 1 << i))
			continue;
		if (!refstr[i])
			refstr[i] = "<unknown>";
		if (unlikely(pos + var->hdr.cnt + strlen(refstr[i]) + 2 >
				buf->size))
			return -ENOBUFS;
		var->hdr.cnt += sprintf(&var->val.s[var->hdr.cnt], "%s ",
				refstr[i]);
	}
	if (var->hdr.cnt)
		var->val.s[--var->hdr.cnt] = '\0';
	var->hdr.len = var->hdr.cnt + 1;
	return 0;
}

static int mdb_address_to_kvar(struct kvarbuf *buf, struct kvar *var,
                               struct mdb_address *addr)
{
	switch (addr->family) {
	case AF_INET:
	case AF_INET6:
		return kvar_addr_set(buf, var, addr->family, addr->data);
	default:
		return kvar_string_set(buf, var, mdb_address_to_string(addr));
	}
	return -EINVAL;
}

static void mdb_amx_host2var(struct kvarbuf *buf, struct kvar *var,
                             struct mdb_host *host)
{
	struct kvar *v0 = var, *v1;
	int i;
	int auxcount = mdb_amx_auxcount(MDB_LEVEL_HOST);
	kvar_map_init(buf, v0, 4 + auxcount);
	kvar_map_edit(buf, v0, 0, "refs", &v1);
	mdb_refs_to_kvar(buf, v1, host->refs);
	kvar_map_done(buf, v0, 0);
	kvar_map_edit(buf, v0, 1, "change", &v1);
	mdb_refs_to_kvar(buf, v1, host->change);
	kvar_map_done(buf, v0, 1);
	kvar_map_edit(buf, v0, 2, "address", &v1);
	mdb_address_to_kvar(buf, v1, &host->address);
	kvar_map_done(buf, v0, 2);
	kvar_map_edit(buf, v0, 3, "port", &v1);
	kvar_int_set(buf, v1, host->port);
	kvar_map_done(buf, v0, 3);
	for (auxcount = 0, i = 0; i < MDB_REF_COUNT; i++) {
		if (!mdb_aux_name(MDB_LEVEL_HOST, i))
			continue;
		kvar_map_edit(buf, v0, 4 + auxcount,
		              mdb_aux_name(MDB_LEVEL_HOST, i), &v1);
		mdb_aux2var(buf, v1, MDB_LEVEL_HOST, i,
		            mdb_host_auxdata(host, i));
		kvar_map_done(buf, v0, 4 + auxcount++);
	}
}

static void mdb_amx_source2var(struct kvarbuf *buf, struct kvar *var,
                               struct mdb_source *source)
{
	struct kvar *v0 = var, *v1, *v2;
	struct mdb_host *host;
	int i;
	int auxcount = mdb_amx_auxcount(MDB_LEVEL_SOURCE);
	kvar_map_init(buf, v0, 4 + auxcount);
	kvar_map_edit(buf, v0, 0, "refs", &v1);
	mdb_refs_to_kvar(buf, v1, source->refs);
	kvar_map_done(buf, v0, 0);
	kvar_map_edit(buf, v0, 1, "change", &v1);
	mdb_refs_to_kvar(buf, v1, source->change);
	kvar_map_done(buf, v0, 1);
	kvar_map_edit(buf, v0, 2, "address", &v1);
	mdb_address_to_kvar(buf, v1, &source->address);
	kvar_map_done(buf, v0, 2);
	for (auxcount = 0, i = 0; i < MDB_REF_COUNT; i++) {
		if (!mdb_aux_name(MDB_LEVEL_SOURCE, i))
			continue;
		kvar_map_edit(buf, v0, 3 + auxcount,
		              mdb_aux_name(MDB_LEVEL_SOURCE, i), &v1);
		mdb_aux2var(buf, v1, MDB_LEVEL_SOURCE, i,
		            mdb_source_auxdata(source, i));
		kvar_map_done(buf, v0, 3 + auxcount++);
	}
	kvar_map_edit(buf, v0, 3 + auxcount, "hosts", &v1);
	kvar_list_init(buf, v1, mdb_amx_list_count(&source->hosts));
	i = 0;
	list_for_each_entry(host, &source->hosts, head) {
		kvar_list_edit(buf, v1, i, &v2);
		mdb_amx_host2var(buf, v2, host);
		kvar_list_done(buf, v1, i++);
	}
	kvar_map_done(buf, v0, 3 + auxcount);
}

static void mdb_amx_group2var(struct kvarbuf *buf, struct kvar *var,
                              struct mdb_group *group)
{
	struct kvar *v0 = var, *v1, *v2;
	struct mdb_source *source;
	int i;
	int nfields = 4; // nr of non-aux fields: refs, change, address, sources
	int cnt = 0;
	int auxcount = mdb_amx_auxcount(MDB_LEVEL_GROUP);

#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL
	nfields += 2; // bw, unauthorized
#endif
	kvar_map_init(buf, v0, nfields + auxcount);

	kvar_map_edit(buf, v0, cnt, "refs", &v1);
	mdb_refs_to_kvar(buf, v1, group->refs);
	kvar_map_done(buf, v0, cnt++);

	kvar_map_edit(buf, v0, cnt, "change", &v1);
	mdb_refs_to_kvar(buf, v1, group->change);
	kvar_map_done(buf, v0, cnt++);

	kvar_map_edit(buf, v0, cnt, "address", &v1);
	mdb_address_to_kvar(buf, v1, &group->address);
	kvar_map_done(buf, v0, cnt++);

#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL
	kvar_map_edit(buf, v0, cnt, "bw", &v1);
	kvar_int_set(buf, v1, group->bw);
	kvar_map_done(buf, v0, cnt++);

	kvar_map_edit(buf, v0, cnt, "unauthorized", &v1);
	kvar_bool_set(buf, v1, group->unauthorized);
	kvar_map_done(buf, v0, cnt++);
#endif

	for (i = 0; i < MDB_REF_COUNT; i++) {
		if (!mdb_aux_name(MDB_LEVEL_GROUP, i))
			continue;
		kvar_map_edit(buf, v0, cnt,
		              mdb_aux_name(MDB_LEVEL_GROUP, i), &v1);
		mdb_aux2var(buf, v1, MDB_LEVEL_GROUP, i,
		            mdb_group_auxdata(group, i));
		kvar_map_done(buf, v0, cnt++);
	}
	kvar_map_edit(buf, v0, cnt, "sources", &v1);
	kvar_list_init(buf, v1, mdb_amx_list_count(&group->sources));
	i = 0;
	list_for_each_entry(source, &group->sources, head) {
		kvar_list_edit(buf, v1, i, &v2);
		mdb_amx_source2var(buf, v2, source);
		kvar_list_done(buf, v1, i++);
	}
	kvar_map_done(buf, v0, cnt); // no need to increment 'cnt'
}

static void mdb_amx_iface2var(struct kvarbuf *buf, struct kvar *var,
                              struct mdb_iface *iface)
{
	struct kvar *v0 = var, *v1;
	int i;
	int auxcount = mdb_amx_auxcount(MDB_LEVEL_IFACE);
	kvar_map_init(buf, v0, 7 + auxcount);
	kvar_map_edit(buf, v0, 0, "refs", &v1);
	mdb_refs_to_kvar(buf, v1, iface->refs);
	kvar_map_done(buf, v0, 0);
	kvar_map_edit(buf, v0, 1, "change", &v1);
	mdb_refs_to_kvar(buf, v1, iface->change);
	kvar_map_done(buf, v0, 1);
	kvar_map_edit(buf, v0, 2, "name", &v1);
	kvar_string_set(buf, v1, iface->name);
	kvar_map_done(buf, v0, 2);
	kvar_map_edit(buf, v0, 3, "ifindex", &v1);
	kvar_uint_set(buf, v1, iface->ifindex);
	kvar_map_done(buf, v0, 3);
	kvar_map_edit(buf, v0, 4, "address", &v1);
	mdb_address_to_kvar(buf, v1, &iface->address);
	kvar_map_done(buf, v0, 4);
	kvar_map_edit(buf, v0, 5, "mtu", &v1);
	kvar_uint_set(buf, v1, iface->mtu);
	kvar_map_done(buf, v0, 5);
	kvar_map_edit(buf, v0, 6, "family", &v1);
	kvar_uint_set(buf, v1, iface->family);
	kvar_map_done(buf, v0, 6);
	for (auxcount = 0, i = 0; i < MDB_REF_COUNT; i++) {
		if (!mdb_aux_name(MDB_LEVEL_IFACE, i))
			continue;
		kvar_map_edit(buf, v0, 7 + auxcount,
		              mdb_aux_name(MDB_LEVEL_IFACE, i), &v1);
		mdb_aux2var(buf, v1, MDB_LEVEL_IFACE, i,
		            mdb_iface_auxdata(iface, i));
		kvar_map_done(buf, v0, 7 + auxcount++);
	}
}

static void mdb_amx_msource2var(struct kvarbuf *buf, struct kvar *var,
                                struct mdb_source *source)
{
	struct kvar *v0 = var, *v1, *v2;
	int i;
	kvar_map_init(buf, v0, 2);
	kvar_map_edit(buf, v0, 0, "address", &v1);
	mdb_address_to_kvar(buf, v1, &source->address);
	kvar_map_done(buf, v0, 0);
	kvar_map_edit(buf, v0, 1, "ifaces", &v1);
	kvar_map_init(buf, v1, mdb_amx_list_count(&source->mdups) + 1);
	i = 0;
	do {
		kvar_map_edit(buf, v1, i, source->group->iface->name, &v2);
		mdb_refs_to_kvar(buf, v2, source->refs);
		kvar_map_done(buf, v1, i++);
		source = list_entry(source->mdups.next, struct mdb_source,
		                    mdups);
	} while (source->mlead != source);
	kvar_map_done(buf, v0, 1);
}

static void mdb_amx_mgroup2var(struct kvarbuf *buf, struct kvar *var,
                               struct mdb_group *group)
{
	struct kvar *v0 = var, *v1, *v2;
	struct mdb_source *source;
	int i;
	kvar_map_init(buf, v0, 3);
	kvar_map_edit(buf, v0, 0, "address", &v1);
	mdb_address_to_kvar(buf, v1, &group->address);
	kvar_map_done(buf, v0, 0);
	kvar_map_edit(buf, v0, 1, "ifaces", &v1);
	kvar_map_init(buf, v1, mdb_amx_list_count(&group->mdups) + 1);
	i = 0;
	do {
		kvar_map_edit(buf, v1, i, group->iface->name, &v2);
		mdb_refs_to_kvar(buf, v2, group->refs);
		kvar_map_done(buf, v1, i++);
		group = list_entry(group->mdups.next, struct mdb_group, mdups);
	} while (group->mlead != group);
	kvar_map_done(buf, v0, 1);
	kvar_map_edit(buf, v0, 2, "sources", &v1);
	kvar_list_init(buf, v1, mdb_amx_list_count(&group->msources));
	i = 0;
	list_for_each_entry(source, &group->msources, mhead) {
		kvar_list_edit(buf, v1, i, &v2);
		mdb_amx_msource2var(buf, v2, source);
		kvar_list_done(buf, v1, i++);
	}
	kvar_map_done(buf, v0, 2);
}

static int mdb_amx_get_groups(struct nlmsghdr *nlh)
{
	struct mdb_iface *iface;
	const char *key;
	struct kvar *v0, *v1, *v2, *grouparg;
	struct mdb_group *group = NULL;
	struct mdb_address address;
	struct sk_buff *skb;
	struct kvarbuf buf = { .size = MCASTD_MAXNLMSGSIZE };
	int ret = -EINVAL;
	int counter = 0;

	mdb_lock();
	v0 = NLMSG_DATA(nlh);
	v1 = kvar_list_val(v0, KAMX_FCALL_PROPERTY_PATH);
	key = kvar_string_val(v1) + strlen("MCASTD.Intf.");
	iface = mdb_iface_find(key);
	if (!iface)
		goto leave;
	v1 = kvar_list_val(v0, KAMX_FCALL_PROPERTY_ARGS);
	grouparg = v2 = kvar_map_find(v1, "group");
	if (v2 && kvar_addr_val(v2)) {
		memset(&address, 0, sizeof(struct mdb_address));
		address.family = kvar_addr_fam(v2);
		memcpy(address.data, kvar_addr_val(v2),
		       mdb_address_size(&address));
		group = mdb_group_find(iface, &address);
	}
	ret = -ENOMEM;
	if (mcastdnl_make(MCASTDNL_TYPE_FCALL, nlh->nlmsg_pid, nlh->nlmsg_seq,
	                  MCASTD_MAXNLMSGPAYLOAD, &buf.data.nlh, &skb))
		goto leave;
	v0 = NLMSG_DATA(buf.data.nlh);
	kvar_list_init(&buf, v0, KAMX_FCALL_PROPERTY_COUNT);
	kvar_list_edit(&buf, v0, KAMX_FCALL_PROPERTY_RETVAL, &v1);
	if (group) {
		counter = 1;
	} else if (!grouparg) {
		counter = mdb_amx_list_count(&iface->records);
	} else {
		counter = 0;
	}
	kvar_list_init(&buf, v1, counter);
	if (group) {
		kvar_list_edit(&buf, v1, 0, &v2);
		mdb_amx_group2var(&buf, v2, group);
		kvar_list_done(&buf, v1, 0);
	} else if (!grouparg) {
		counter = 0;
		list_for_each_entry(group, &iface->records, head) {
			kvar_list_edit(&buf, v1, counter, &v2);
			mdb_amx_group2var(&buf, v2, group);
			kvar_list_done(&buf, v1, counter++);
		}
	}
	kvar_list_done(&buf, v0, KAMX_FCALL_PROPERTY_RETVAL);
	mcastdnl_unicast(skb, buf.data.nlh, KVAR_LENGTH(v0), nlh->nlmsg_pid);
	ret = 0;
leave:
	mdb_unlock();
	return ret;
}

static int mdb_amx_get_iface(struct nlmsghdr *nlh)
{
	struct mdb_iface *iface;
	const char *key;
	struct kvar *v0, *v1;
	struct sk_buff *skb;
	struct kvarbuf buf = { .size = MCASTD_MAXNLMSGSIZE };
	int ret = -EINVAL;

	mdb_lock();
	v0 = NLMSG_DATA(nlh);
	v1 = kvar_list_val(v0, KAMX_FCALL_PROPERTY_PATH);
	key = kvar_string_val(v1) + strlen("MCASTD.Intf.");
	iface = mdb_iface_find(key);
	if (!iface)
		goto leave;
	ret = -ENOMEM;
	if (mcastdnl_make(MCASTDNL_TYPE_FCALL, nlh->nlmsg_pid, nlh->nlmsg_seq,
	                  MCASTD_MAXNLMSGPAYLOAD, &buf.data.nlh, &skb))
		goto leave;
	v0 = NLMSG_DATA(buf.data.nlh);
	kvar_list_init(&buf, v0, KAMX_FCALL_PROPERTY_COUNT);
	kvar_list_edit(&buf, v0, KAMX_FCALL_PROPERTY_RETVAL, &v1);
	mdb_amx_iface2var(&buf, v1, iface);
	kvar_list_done(&buf, v0, KAMX_FCALL_PROPERTY_RETVAL);
	mcastdnl_unicast(skb, buf.data.nlh, KVAR_LENGTH(v0), nlh->nlmsg_pid);
	ret = 0;
leave:
	mdb_unlock();
	return ret;
}

static int mdb_amx_get_mgroups(struct nlmsghdr *nlh)
{
	struct kvarbuf buf = { .size = MCASTD_MAXNLMSGSIZE };
	struct kvar *v0, *v1, *v2;
	struct mdb_group *group = NULL;
	struct sk_buff *skb;
	int i = 0;
	if (mcastdnl_make(MCASTDNL_TYPE_FCALL, nlh->nlmsg_pid, nlh->nlmsg_seq,
	                  MCASTD_MAXNLMSGPAYLOAD, &buf.data.nlh, &skb))
		return -ENOMEM;
	mdb_lock();
	v0 = NLMSG_DATA(buf.data.nlh);
	kvar_list_init(&buf, v0, KAMX_FCALL_PROPERTY_COUNT);
	kvar_list_edit(&buf, v0, KAMX_FCALL_PROPERTY_RETVAL, &v1);
	kvar_list_init(&buf, v1, mdb_amx_list_count(&mdb_groups));
	i = 0;
	list_for_each_entry(group, &mdb_groups, mhead) {
		kvar_list_edit(&buf, v1, i, &v2);
		mdb_amx_mgroup2var(&buf, v2, group);
		kvar_list_done(&buf, v1, i++);
	}
	kvar_list_done(&buf, v0, KAMX_FCALL_PROPERTY_RETVAL);
	mdb_unlock();
	mcastdnl_unicast(skb, buf.data.nlh, KVAR_LENGTH(v0), nlh->nlmsg_pid);
	return 0;
}


static void mdb_amx_handle_event(struct mdb_event *e)
{
	struct kvar *v0, *v1, *v2;
	struct sk_buff *skb;
	struct kvarbuf buf = { .size = MCASTD_MAXNLMSGSIZE };
	if (mcastdnl_make(MCASTDNL_TYPE_NOTIFY, 0, 0,
	                  MCASTD_MAXNLMSGPAYLOAD, &buf.data.nlh, &skb))
		return;
	v0 = NLMSG_DATA(buf.data.nlh);
	kvar_list_init(&buf, v0, KAMX_NOTIFY_PROPERTY_COUNT);
	/* path */
	kvar_list_edit(&buf, v0, KAMX_NOTIFY_PROPERTY_PATH, &v1);
	v1->hdr.type = KVARTYPE_STRING;
	v1->hdr.cnt = strlen("MCASTD.Intf.") + strlen(e->group->iface->name);
	v1->hdr.len = v1->hdr.cnt + 1;
	strcpy(v1->val.s, "MCASTD.Intf.");
	strcat(v1->val.s, e->group->iface->name);
	kvar_list_done(&buf, v0, KAMX_NOTIFY_PROPERTY_PATH);
	/* type */
	kvar_list_edit(&buf, v0, KAMX_NOTIFY_PROPERTY_TYPE, &v1);
	kvar_uint_set(&buf, v1, KAMX_NOTIFY_TYPE_MDB);
	kvar_list_done(&buf, v0, KAMX_NOTIFY_PROPERTY_TYPE);
	/* name */
	kvar_list_edit(&buf, v0, KAMX_NOTIFY_PROPERTY_NAME, &v1);
	kvar_string_set(&buf, v1, "MDB");
	kvar_list_done(&buf, v0, KAMX_NOTIFY_PROPERTY_NAME);
	/* params */
	kvar_list_edit(&buf, v0, KAMX_NOTIFY_PROPERTY_PARAMS, &v1);
	kvar_map_init(&buf, v1, 1);
	kvar_map_edit(&buf, v1, 0, "group", &v2);
	mdb_amx_group2var(&buf, v2, e->group);
	kvar_map_done(&buf, v1, 0);
	kvar_list_done(&buf, v0, KAMX_NOTIFY_PROPERTY_PARAMS);

	mcastdnl_multicast(skb, buf.data.nlh, KVAR_LENGTH(v0), 0,
	                   MCASTDNL_GROUP_AMX_OPTIONAL, GFP_KERNEL);
}

int mcastd_mdb_amx_init(void) {
	mcastd_object_register("MCASTD.Intf.*", mdb_amx_set_intf);
	mcastd_function_register("MCASTD.Intf.*", "getGroups", mdb_amx_get_groups);
	mcastd_function_register("MCASTD.Intf.*", "getIface", mdb_amx_get_iface);
	mcastd_function_register("MCASTD", "getMGroups", mdb_amx_get_mgroups);
	mdb_lock();
	mdb_subscribe(MDB_PRIORITY_LAST, mdb_amx_handle_event);
	mdb_unlock();
	return 0;
}

void mcastd_mdb_amx_cleanup(void) {
	mdb_lock();
	mdb_unsubscribe(MDB_PRIORITY_LAST, mdb_amx_handle_event);
	mdb_unlock();
	mcastd_object_unregister("MCASTD.Intf.*", mdb_amx_set_intf);
	mcastd_function_unregister("MCASTD.Intf.*", "getGroups", mdb_amx_get_groups);
	mcastd_function_unregister("MCASTD.Intf.*", "getIface", mdb_amx_get_iface);
	mcastd_function_unregister("MCASTD", "getMGroups", mdb_amx_get_mgroups);
}

