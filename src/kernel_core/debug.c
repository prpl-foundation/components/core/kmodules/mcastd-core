/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/list.h>
#include <linux/mutex.h>
#include <linux/version.h>

#if LINUX_VERSION_CODE < KERNEL_VERSION(4,17,0)
#include <linux/netdevice.h> /* NETDEV_DOWN */
#endif

#include <mcastd/kernel/kamx.h>
#include <mcastd/kernel/debug.h>
#include <mcastd/kernel/compat.h>

#define MCASTD_DEBUG_IDENTSIZE 32

struct mcastd_debug_zone {
	struct list_head head;
	char ident[MCASTD_DEBUG_IDENTSIZE];
	int enable;
};

static DEFINE_MUTEX(mcastd_debug_mutex);
atomic_t mcastd_debug_enable = ATOMIC_INIT(0);
static int mcastd_debug_default = 1;
static LIST_HEAD(mcastd_debug_zones);

static void mcastd_debug_lock(void)
{
	mutex_lock(&mcastd_debug_mutex);
}

static void mcastd_debug_unlock(void)
{
	mutex_unlock(&mcastd_debug_mutex);
}

static struct mcastd_debug_zone *mcastd_debug_zone_find(const char *ident)
{
	struct mcastd_debug_zone *zone;
	list_for_each_entry(zone, &mcastd_debug_zones, head) {
		if (!strcmp(zone->ident, ident))
			return zone;
	}
	return NULL;
}

/**
 * mcastd_debug_zone_get() - Return info about certain trace zone.
 * @ident: name of trace zone
 * @notify: if 1, send MC netlink msg to US if there was no mcastd_debug_zone
 *          instance yet with info about the requested trace zone.
 *
 * Return:
 * * pointer to mcastd_debug_zone instance with info about requested trace zone
 *   on success
 * * NULL on error
 */
static struct mcastd_debug_zone *mcastd_debug_zone_get(const char *ident,
                                                       int notify)
{
	struct sk_buff *skb;
	struct kvar *v0, *v1, *v2;
	struct kvarbuf buf = { .size = MCASTD_MAXNLMSGSIZE };
	struct mcastd_debug_zone *zone = mcastd_debug_zone_find(ident);
	if (zone)
		return zone;
	zone = kmalloc(sizeof(struct mcastd_debug_zone), GFP_KERNEL);
	if (!zone) {
		ERRMSG("kmalloc() failed");
		return NULL;
	}
	list_add_tail(&zone->head, &mcastd_debug_zones);
	strlcpy(zone->ident, ident, MCASTD_DEBUG_IDENTSIZE);
	zone->enable = mcastd_debug_default;
	if (!notify)
		return zone;

	/* Send MC netlink msg to notify US about the new trace zone */
	if (mcastdnl_make(MCASTDNL_TYPE_NEWOBJ, 0, 0, MCASTD_MAXNLMSGPAYLOAD,
	                  &buf.data.nlh, &skb))
		return zone;
	v0 = NLMSG_DATA(buf.data.nlh);
	kvar_list_init(&buf, v0, KAMX_OBJECT_PROPERTY_COUNT);
	/* 1) path */
	kvar_list_edit(&buf, v0, KAMX_OBJECT_PROPERTY_PATH, &v1);
	v1->hdr.type = KVARTYPE_STRING;
	v1->hdr.len = sizeof("MCASTD.Debug.Zone.") + strlen(ident);
	v1->hdr.cnt = v1->hdr.len - 1;
	strcpy(v1->val.s, "MCASTD.Debug.Zone.");
	strcat(v1->val.s, ident);
	kvar_list_done(&buf, v0, KAMX_OBJECT_PROPERTY_PATH);
	/* 2) parameters */
	kvar_list_edit(&buf, v0, KAMX_OBJECT_PROPERTY_PARAMS, &v1);
	kvar_map_init(&buf, v1, 1);
	kvar_map_edit(&buf, v1, 0, "Enable", &v2);
	kvar_bool_set(&buf, v2, zone->enable);
	kvar_map_done(&buf, v1, 0);
	kvar_list_done(&buf, v0, KAMX_OBJECT_PROPERTY_PARAMS);
	mcastdnl_multicast(skb, buf.data.nlh, KVAR_LENGTH(v0),
	                   0, MCASTDNL_GROUP_AMX_ALWAYS, GFP_KERNEL);
	return zone;
}

static void debug_zone_release(struct mcastd_debug_zone *zone)
{
	list_del(&zone->head);
	kfree(zone);
}

/**
 * mcastd_debug_msg() - log message and notify US upon 1st msg for that trace zone
 * @ident: name of trace zone
 * @fmt: message to be logged
 *
 * The function logs the message if mcastd_debug_enable is 1 and if the trace zone
 * is enabled. The function sends a MC netlink msg to US if this is the 1st msg
 * for that trace zone.
 */
void mcastd_debug_msg(const char *ident, const char *fmt, ...)
{
	struct mcastd_debug_zone *zone;
	va_list args;
	if (!atomic_read(&mcastd_debug_enable))
		return;
	mcastd_debug_lock();
	zone = mcastd_debug_zone_get(ident, 1);
	if (!zone || !zone->enable)
		goto leave;
	va_start(args, fmt);
	vprintk(fmt, args);
	va_end(args);
leave:
	mcastd_debug_unlock();
}

static int mcastd_debug_set_global(struct nlmsghdr *nlh)
{
	struct kvar *v0, *v1, *v2;
	v0 = NLMSG_DATA(nlh);
	v1 = kvar_list_val(v0, KAMX_OBJECT_PROPERTY_PARAMS);
	v2 = kvar_map_find(v1, "Enable");
	atomic_set(&mcastd_debug_enable, kvar_bool_val(v2));
	return 0;
}

static int mcastd_debug_set_default(struct nlmsghdr *nlh)
{
	struct kvar *v0, *v1, *v2;
	v0 = NLMSG_DATA(nlh);
	v1 = kvar_list_val(v0, KAMX_OBJECT_PROPERTY_PARAMS);
	v2 = kvar_map_find(v1, "Enable");
	mcastd_debug_lock();
	mcastd_debug_default = kvar_bool_val(v2);
	mcastd_debug_unlock();
	return 0;
}

static int mcastd_debug_set_zone(struct nlmsghdr *nlh)
{
	struct mcastd_debug_zone *zone;
	struct kvar *v0, *v1, *v2;
	const char *key;
	v0 = NLMSG_DATA(nlh);
	v1 = kvar_list_val(v0, KAMX_OBJECT_PROPERTY_PATH);
	key = kvar_string_val(v1) + strlen("MCASTD.Debug.Zone.");
	v1 = kvar_list_val(v0, KAMX_OBJECT_PROPERTY_PARAMS);
	mcastd_debug_lock();
	switch (nlh->nlmsg_type) {
	case MCASTDNL_TYPE_NEWOBJ:
		v2 = kvar_map_find(v1, "Enable");
		zone = mcastd_debug_zone_get(key, 0);
		if (zone)
			zone->enable = kvar_bool_val(v2);
		break;
	case MCASTDNL_TYPE_DELOBJ:
		zone = mcastd_debug_zone_find(key);
		if (zone)
			debug_zone_release(zone);
		break;
	default:
		break;
	}
	mcastd_debug_unlock();
	return 0;
}

#if LINUX_VERSION_CODE < KERNEL_VERSION(4,17,0)

/* Linux introduced NETDEV_CHANGEUPPER in 3.11. Define it for older kernels. */
#ifndef NETDEV_CHANGEUPPER
#define NETDEV_CHANGEUPPER 0x0015
#endif

/**
 * netdev_cmd_to_name - return name for a netdev event
 *
 * The Linux kernel added this function in 4.17. Copy-paste the function from
 * Linux to mcastd-core for older kernels, but only add the events which are
 * relevant to mcastd: DOWN, GOING_DOWN, CHANGE and CHANGEUPPER.
 */
const char *netdev_cmd_to_name(unsigned long cmd)
{
#define N(val) 						\
	case NETDEV_##val:				\
		return "NETDEV_" __stringify(val);
	switch (cmd) {
	N(DOWN) N(GOING_DOWN) N(CHANGE) N(CHANGEUPPER)
	}
#undef N
	return "UNKNOWN_NETDEV_EVENT";
}
EXPORT_SYMBOL(netdev_cmd_to_name);
#endif


int mcastd_debug_init(void)
{
	mcastd_object_register("MCASTD.Debug", mcastd_debug_set_global);
	mcastd_object_register("MCASTD.Debug.Zone", mcastd_debug_set_default);
	mcastd_object_register("MCASTD.Debug.Zone.*", mcastd_debug_set_zone);
	return 0;
}

void mcastd_debug_cleanup(void)
{
	mcastd_object_unregister("MCASTD.Debug", mcastd_debug_set_global);
	mcastd_object_unregister("MCASTD.Debug.Zone", mcastd_debug_set_default);
	mcastd_object_unregister("MCASTD.Debug.Zone.*", mcastd_debug_set_zone);
	mcastd_debug_lock();
	while (!list_empty(&mcastd_debug_zones))
		debug_zone_release(list_first_entry(&mcastd_debug_zones,
				struct mcastd_debug_zone, head));
	mcastd_debug_unlock();
}

EXPORT_SYMBOL(mcastd_debug_enable);
EXPORT_SYMBOL(mcastd_debug_msg);
