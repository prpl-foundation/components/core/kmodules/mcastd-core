/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/netdevice.h>

#define IDENT "snooper"
#include <mcastd/kernel.h>

#include "rate_limiting.h"
#include "snooper.h" /* mcastdsn_get_ifindex_of_bridge() */

#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL

#define MICROSEC_IN_SEC  1000000

/*
 * mcastdsn_rate_limit_admit() - Enforce the upstream IGMP/MLD rate.
 *
 * @skb: incoming IGMP/MLD report.
 * @family: indicates if skb is an IGMP or MLD report: AF_INET for IGMP, and
 *          AF_INET6 for MLD.
 *
 * The user can configure an upstream IGMP/MLD rate. The function checks if the
 * snooper should handle the incoming IGMP/MLD report according to that limit or
 * not. If not, the function returns 0 to indicate the report should be silently
 * discarded.
 *
 * Context: non-blocking
 *
 * Return: 1 if IGMP/MLD report can be accepted according to rate limit, else 0.
 */
int mcastdsn_rate_limit_admit(struct sk_buff *skb, u8 family)
{
	int ret = 1; // admit
	int ifindex;
	u32 max_rate;
	struct mdb_iface *iface;
	struct mcastdsn_iface *iaux;
	unsigned long irqflags;

	ktime_t      curTime;
	u64          diffUs;
	unsigned int usPerPacket;
	unsigned int temp32;
	unsigned int burstLimit;

	ifindex = mcastdsn_get_ifindex_of_bridge(skb->dev);

	/* Note: this lock protects against addition/deletion of records, but
	 * doesn't protect against the records changing. For that you need the
	 * mdb_lock, that we can't take in this context. We're in atomic
	 * context and can't schedule...
	 * The mdb records addresses are set at creation, and don't change
	 * over the lifetime. So it should be ok.
	 * */
	spin_lock_irqsave(&mdb_record_spinlock, irqflags);
	list_for_each_entry(iface, &mdb_ifaces, head) {
		if ((iface->family == family) && (iface->ifindex == ifindex))
			break;
	}

	if (iface == NULL)
		goto leave_unlock;

	iaux = mdb_iface_auxdata(iface, MDB_REF_SNOOPER_PRIV);
	if (iaux == NULL)
		goto leave_unlock;
	max_rate = iaux->upstream_rate;
	if (max_rate == 0)
		goto leave_unlock;

	/* add tokens to the bucket - compute in microseconds */
	curTime = ktime_get();
	usPerPacket = (MICROSEC_IN_SEC / max_rate);
	diffUs = ktime_to_us(ktime_sub(curTime, iaux->rate_last_packet));
	iaux->diff_total += diffUs;
	diffUs += iaux->rate_rem_time;

	/* allow 25% burst */
	burstLimit = max_rate >> 2;
	if (0 == burstLimit)
	{
		burstLimit = 1;
	}

	if (diffUs > MICROSEC_IN_SEC)
	{
		iaux->rate_bucket = burstLimit;
		iaux->rate_rem_time = 0;
		iaux->diff_total = 0;
		iaux->pkts_in_second = 1;
	}
	else
	{
		if (iaux->diff_total >= MICROSEC_IN_SEC)
		{
			iaux->diff_total = 0;
			iaux->pkts_in_second = 1;
		}

		temp32 = (unsigned int)diffUs / usPerPacket;
		iaux->rate_bucket += temp32;
		if (temp32)
		{
			iaux->rate_rem_time = diffUs - (temp32 * usPerPacket);
		}
	}

	if (iaux->rate_bucket > burstLimit)
	{
		iaux->rate_bucket = burstLimit;
		iaux->rate_rem_time = 0;
	}

	/* if bucket is empty or packets exceed the rate - drop the packet */
	if ((0 == iaux->rate_bucket) || (iaux->pkts_in_second > max_rate))
	{
		/* discard packet */
		ret = 0;
		goto leave_unlock;
	}

	iaux->rate_bucket--;
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(4,10,0))
	iaux->rate_last_packet = curTime;
#else
	iaux->rate_last_packet.tv64 = curTime.tv64;
#endif
	++iaux->pkts_in_second;

leave_unlock:
	spin_unlock_irqrestore(&mdb_record_spinlock, irqflags);
	DBGMSG("admit: ret: %s", (ret) ? "ACCEPT" : "REJECT");
	return ret;
}

#endif /* CONFIG_MCASTD_CORE_ACCESS_CONTROL */

