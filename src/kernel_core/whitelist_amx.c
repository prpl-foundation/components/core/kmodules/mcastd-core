/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL

#include <linux/kernel.h>
#include <linux/module.h>
#include <net/netlink.h>

#define IDENT "whitelist"

#include <mcastd/kernel/debug.h>
#include <mcastd/kernel/kamx.h> /* mcastd_object_register() */
#include <mcastd/kernel/netlink.h>

#include "whitelist.h"

static int mcastdwl_amx_set_whitelist(struct nlmsghdr *nlh)
{
	struct mdb_iface *iface;
	const char *key;
	struct kvarbuf buf = { .size = MCASTD_MAXNLMSGSIZE };
	struct kvar *v0, *v1, *v2, *v3, *v4;
	struct sk_buff *skb;
	__u16 id, vlan_id;
	__u32 grp_bw;
	__u8 *start_addr, *end_addr, *src_addr;
	int err;
	int ret = -EINVAL;

	mdb_lock();
	v0 = NLMSG_DATA(nlh);
	v1 = kvar_list_val(v0, KAMX_FCALL_PROPERTY_PATH);
	key = kvar_string_val(v1) + strlen("MCASTD.Intf.");
	iface = mdb_iface_find(key);
	if (!iface) {
		ERRMSG("mdb_iface not found for %s", key);
		goto leave;
	}
	v1 = kvar_list_val(v0, KAMX_FCALL_PROPERTY_ARGS);
	v2 = kvar_map_find(v1, "id");
	id = kvar_uint_val(v2);

	v2 = kvar_map_find(v1, "entry");
	v3 = kvar_map_find(v2, "vlanId");

	vlan_id = kvar_uint_val(v3);

	v3 = kvar_map_find(v2, "startAddress");
	if (!kvar_addr_val(v3)) {
		ERRMSG("startAddress not found");
		goto leave;
	}
	if (kvar_addr_fam(v3) != iface->family) {
		ERRMSG("startAddress family doesn't match");
		goto leave;
	}
	start_addr = kvar_addr_val(v3);

	v3 = kvar_map_find(v2, "endAddress");
	if (!kvar_addr_val(v3)) {
		ERRMSG("endAddress not found");
		goto leave;
	}
	if (kvar_addr_fam(v3) != iface->family) {
		ERRMSG("endAddress family doesn't match");
		goto leave;
	}
	end_addr = kvar_addr_val(v3);

	v3 = kvar_map_find(v2, "sourceAddress");
	if (kvar_addr_val(v3)) {
		if (kvar_addr_fam(v3) != iface->family) {
			ERRMSG("endAddress family doesn't match");
			goto leave;
		}
		src_addr = kvar_addr_val(v3);
	} else {
		src_addr = NULL;
	}

	v4 = kvar_map_find(v2, "groupBandwidth");
	if (v4) {
		grp_bw = (__u32)kvar_uint_val(v4);
	} else {
		ERRMSG("groupBandwidth not found!");
		grp_bw = 0;
	}

	DEBMSG("Set whitelist: id        : %d", id);
	DEBMSG("Set whitelist: vlan_id   : %d", vlan_id);
	DEBMSG("Set whitelist: start_addr: %d.%d.%d.%d",
			start_addr[0], start_addr[1], start_addr[2], start_addr[3]);
	DEBMSG("Set whitelist: end_addr  : %d.%d.%d.%d",
			end_addr[0], end_addr[1], end_addr[2], end_addr[3]);
	if (src_addr)
		DEBMSG("Set whitelist: src_addr  : %d.%d.%d.%d",
				src_addr[0], src_addr[1], src_addr[2], src_addr[3]);
	DEBMSG("Set whitelist: grp_bw    : %u", grp_bw);
	err = mcastdwl_set_whitelist(iface, id, vlan_id, start_addr, end_addr, src_addr, grp_bw);

	ret = -ENOMEM;
	if (mcastdnl_make(MCASTDNL_TYPE_FCALL, nlh->nlmsg_pid, nlh->nlmsg_seq,
	                  MCASTD_MAXNLMSGPAYLOAD, &buf.data.nlh, &skb))
		goto leave;
	v0 = NLMSG_DATA(buf.data.nlh);
	kvar_list_init(&buf, v0, KAMX_FCALL_PROPERTY_COUNT);

	if (err) {
		kvar_list_edit(&buf, v0, KAMX_FCALL_PROPERTY_ERROR, &v1);
		kvar_uint_set(&buf, v1, 0x00010001);
		kvar_list_done(&buf, v0, KAMX_FCALL_PROPERTY_ERROR);
	}

	mcastdnl_unicast(skb, buf.data.nlh, KVAR_LENGTH(v0), nlh->nlmsg_pid);
	ret = 0;
leave:
	mdb_unlock();
	return ret;
}

static int mcastdwl_amx_clear_whitelist(struct nlmsghdr *nlh)
{
	struct mdb_iface *iface;
	const char *key;
	struct kvarbuf buf = { .size = MCASTD_MAXNLMSGSIZE };
	struct kvar *v0, *v1, *v2;
	struct sk_buff *skb;
	__u16 id;

	int ret = -EINVAL;

	mdb_lock();
	v0 = NLMSG_DATA(nlh);
	v1 = kvar_list_val(v0, KAMX_FCALL_PROPERTY_PATH);
	key = kvar_string_val(v1) + strlen("MCASTD.Intf.");
	iface = mdb_iface_find(key);
	if (!iface) {
		ERRMSG("mdb_iface not found for %s", key);
		goto leave;
	}
	v1 = kvar_list_val(v0, KAMX_FCALL_PROPERTY_ARGS);
	v2 = kvar_map_find(v1, "id");
	id = kvar_uint_val(v2);

	mcastdwl_clear_whitelist(iface, id);

	ret = -ENOMEM;
	if (mcastdnl_make(MCASTDNL_TYPE_FCALL, nlh->nlmsg_pid, nlh->nlmsg_seq,
	                  MCASTD_MAXNLMSGPAYLOAD, &buf.data.nlh, &skb))
		goto leave;
	v0 = NLMSG_DATA(buf.data.nlh);
	kvar_list_init(&buf, v0, KAMX_FCALL_PROPERTY_COUNT);
	mcastdnl_unicast(skb, buf.data.nlh, KVAR_LENGTH(v0), nlh->nlmsg_pid);
	ret = 0;
leave:
	mdb_unlock();
	return ret;
}

static int mcastdwl_amx_flush_whitelist(struct nlmsghdr *nlh)
{
	struct mdb_iface *iface;
	const char *key;
	struct kvarbuf buf = { .size = MCASTD_MAXNLMSGSIZE };
	struct kvar *v0, *v1;
	struct sk_buff *skb;

	int ret = -EINVAL;

	mdb_lock();
	v0 = NLMSG_DATA(nlh);
	v1 = kvar_list_val(v0, KAMX_FCALL_PROPERTY_PATH);
	key = kvar_string_val(v1) + strlen("MCASTD.Intf.");
	iface = mdb_iface_find(key);
	if (!iface) {
		ERRMSG("mdb_iface not found for %s", key);
		goto leave;
	}

	mcastdwl_flush_whitelist(iface);

	ret = -ENOMEM;
	if (mcastdnl_make(MCASTDNL_TYPE_FCALL, nlh->nlmsg_pid, nlh->nlmsg_seq,
	                  MCASTD_MAXNLMSGPAYLOAD, &buf.data.nlh, &skb))
		goto leave;
	v0 = NLMSG_DATA(buf.data.nlh);
	kvar_list_init(&buf, v0, KAMX_FCALL_PROPERTY_COUNT);
	mcastdnl_unicast(skb, buf.data.nlh, KVAR_LENGTH(v0), nlh->nlmsg_pid);
	ret = 0;
leave:
	mdb_unlock();
	return ret;
}

static void mcastdwl_amx_whlist2var(struct kvarbuf *buf, struct kvar *var,
                                    struct mcastdwl_whlist_entry *entry)
{
	struct kvar *v0 = var, *v1, *v2;

	kvar_map_init(buf, v0, 2);
	kvar_map_edit(buf, v0, 0, "id", &v1);
	kvar_int_set(buf, v1, entry->id);
	kvar_map_done(buf, v0, 0);

	kvar_map_edit(buf, v0, 1, "entry", &v1);
	kvar_map_init(buf, v1, 4);
	kvar_map_edit(buf, v1, 0, "vlanId", &v2);
	kvar_int_set(buf, v2, entry->vlanid);
	kvar_map_done(buf, v1, 0);

	kvar_map_edit(buf, v1, 1, "startAddress", &v2);
	kvar_addr_set(buf, v2, entry->start_addr.family, entry->start_addr.data);
	kvar_map_done(buf, v1, 1);

	kvar_map_edit(buf, v1, 2, "endAddress", &v2);
	kvar_addr_set(buf, v2, entry->end_addr.family, entry->end_addr.data);
	kvar_map_done(buf, v1, 2);

	kvar_map_edit(buf, v1, 3, "sourceAddress", &v2);
	kvar_addr_set(buf, v2, entry->src_addr.family, entry->src_addr.data);
	kvar_map_done(buf, v1, 3);
	kvar_map_done(buf, v0, 1);
}

static int mcastdwl_amx_get_whitelist(struct nlmsghdr *nlh)
{
	struct kvarbuf buf = { .size = MCASTD_MAXNLMSGSIZE };
	struct kvar *v0, *v1, *v2;
	struct mdb_iface *iface = NULL;
	const char *key;
	struct sk_buff *skb;
	struct list_head whlist;
	struct mcastdwl_whlist_entry *e;
	int i = 0, cnt = 0;

	int ret = -EINVAL;

	mdb_lock();
	v0 = NLMSG_DATA(nlh);
	v1 = kvar_list_val(v0, KAMX_FCALL_PROPERTY_PATH);
	key = kvar_string_val(v1) + strlen("MCASTD.Intf.");
	iface = mdb_iface_find(key);
	if (!iface)
		goto leave;

	ret = -ENOMEM;
	if (mcastdnl_make(MCASTDNL_TYPE_FCALL, nlh->nlmsg_pid, nlh->nlmsg_seq,
	                  MCASTD_MAXNLMSGPAYLOAD, &buf.data.nlh, &skb))
		goto leave;
	v0 = NLMSG_DATA(buf.data.nlh);
	kvar_list_init(&buf, v0, KAMX_FCALL_PROPERTY_COUNT);
	kvar_list_edit(&buf, v0, KAMX_FCALL_PROPERTY_RETVAL, &v1);

	mcastdwl_get_whitelist(iface, &whlist);

	/*need to know the count before initializing the list*/
	list_for_each_entry(e, &whlist, head) {
		cnt++;
	}

	kvar_list_init(&buf, v1, cnt);

	i = 0;
	list_for_each_entry(e, &whlist, head) {
			kvar_list_edit(&buf, v1, i, &v2);
			mcastdwl_amx_whlist2var(&buf, v2, e);
			kvar_list_done(&buf, v1, i++);
	}
	mcastdwl_free_whitelist(&whlist);

	kvar_list_done(&buf, v0, KAMX_FCALL_PROPERTY_RETVAL);
	mcastdnl_unicast(skb, buf.data.nlh, KVAR_LENGTH(v0), nlh->nlmsg_pid);
leave:
	mdb_unlock();
	return 0;
}

static int mcastdwl_amx_set_unauthorized_join_request_behaviour(struct nlmsghdr *nlh)
{
	struct kvar *v0, *v1;
	bool force_igmp_fwd = false;
	v0 = NLMSG_DATA(nlh);
	v1 = kvar_list_val(v0, KAMX_OBJECT_PROPERTY_PARAMS);
	force_igmp_fwd = kvar_bool_val(kvar_map_find(v1, "UnauthorizedJoinRequestBehaviour"));

	DEBMSG("force igmp fwd = %d", force_igmp_fwd);
	mcastdwl_set_force_igmp_fwd(force_igmp_fwd);
	return 0;
}

int mcastd_whitelist_amx_init(void)
{
	mcastd_function_register("MCASTD.Intf.*", "setWhitelist", mcastdwl_amx_set_whitelist);
	mcastd_function_register("MCASTD.Intf.*", "clearWhitelist", mcastdwl_amx_clear_whitelist);
	mcastd_function_register("MCASTD.Intf.*", "flushWhitelist", mcastdwl_amx_flush_whitelist);
	mcastd_function_register("MCASTD.Intf.*", "getWhitelist", mcastdwl_amx_get_whitelist);

	mcastd_object_register("MCASTD", mcastdwl_amx_set_unauthorized_join_request_behaviour);
	return 0;
}

void mcastd_whitelist_amx_cleanup(void)
{
	mcastd_function_unregister("MCASTD.Intf.*", "setWhitelist", mcastdwl_amx_set_whitelist);
	mcastd_function_unregister("MCASTD.Intf.*", "clearWhitelist", mcastdwl_amx_clear_whitelist);
	mcastd_function_unregister("MCASTD.Intf.*", "flushWhitelist", mcastdwl_amx_flush_whitelist);
	mcastd_function_unregister("MCASTD.Intf.*", "getWhitelist", mcastdwl_amx_get_whitelist);

	mcastd_object_unregister("MCASTD", mcastdwl_amx_set_unauthorized_join_request_behaviour);
}

#endif /* CONFIG_MCASTD_CORE_ACCESS_CONTROL */
