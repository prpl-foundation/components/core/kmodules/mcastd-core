/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL

#include <linux/kernel.h>
#include <linux/module.h>
#include <net/netlink.h>

#define IDENT "bwctrl"

#include <mcastd/kernel/debug.h>
#include <mcastd/kernel/kamx.h> /* mcastd_object_register() */
#include <mcastd/kernel/netlink.h>

#include "bwctrl.h" /* bwctrl_get_current_bw() */
#include "snooper_amx.h" /* mcastdsn_amx_send_status_value() */
#include "bwctrl_amx.h"

/**
 * Number of join reports that would have caused exceeding the max MC BW.
 */
static __u32 mcastdsn_amx_bw_exceeded_cntr = 0;


static int mcastdsn_amx_set_max_multicast_bandwidth(struct nlmsghdr *nlh)
{
	struct kvar *v0, *v1;
	__s32 maxbw = -1;
	v0 = NLMSG_DATA(nlh);
	v1 = kvar_list_val(v0, KAMX_OBJECT_PROPERTY_PARAMS);
	maxbw = kvar_int_val(kvar_map_find(v1, "MaxMulticastBandwidth"));

	DEBMSG("max bw = %d bps", maxbw);
	bwctrl_set_max_bw(maxbw);
	return 0;
}

/**
 * mcastdsn_amx_current_bw_changed() - Report current MC BW to US
 *
 * Send a MC netlink msg to US reporting the current MC BW.
 */
void mcastdsn_amx_current_bw_changed(void)
{
	__u32 bw = bwctrl_get_current_bw();
	mcastdsn_amx_send_status_value("CurrentMulticastBandwidth", bw);
}

/**
 * mcastdsn_amx_incr_bw_exceeded_cntr() - Increment bw exceeded cntr
 *
 * Increment counter counting IGMP/MLD reports which would have exceeded the
 * max MC bandwidth, and send a MC netlink msg to US with the new value.
 *
 * Context: Process context
 */
void mcastdsn_amx_incr_bw_exceeded_cntr(void)
{
	++mcastdsn_amx_bw_exceeded_cntr;
	mcastdsn_amx_send_status_value("BandwidthExceededCounter",
	                               mcastdsn_amx_bw_exceeded_cntr);
}

int mcastd_bwctrl_amx_init(void)
{
	mcastd_object_register("MCASTD", mcastdsn_amx_set_max_multicast_bandwidth);
	return 0;
}

void mcastd_bwctrl_amx_cleanup(void)
{
	mcastd_object_unregister("MCASTD", mcastdsn_amx_set_max_multicast_bandwidth);
}

#endif /* CONFIG_MCASTD_CORE_ACCESS_CONTROL */
