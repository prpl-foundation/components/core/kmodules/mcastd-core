/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL

#include <linux/kernel.h>
#include <linux/module.h>

#define IDENT "bwctrl"

#include <mcastd/kernel/debug.h>

#include "bwctrl.h"
#include "bwctrl_amx.h" /* mcastdsn_amx_current_bw_changed() */
#include "whitelist.h" /* mcastdwl_lookup_bw() */

static __s32 bwctrl_max_bw = -1; /* [bps] */
static __s32 bwctrl_current_bw = 0; /* [bps] */

void bwctrl_set_max_bw(__s32 bw)
{
	bwctrl_max_bw = bw;
}

__u32 bwctrl_get_current_bw(void)
{
	return bwctrl_current_bw;
}

/**
 * bwctrl_register_stream() - Register bw corresponding to MC address
 *
 * @param[in] addr : group address
 * @param[in,out] bw : bandwidth contributed by this group.
 *
 * The function asks the whitelist module if the group contributes to global
 * multicast (MC) bandwidth. If it does, the function adds this bandwidth to
 * the global MC bandwidth, and assigns the group bandwidth to @a bw.
 *
 * If the whitelist module doesn't have any info about the group address,
 * then this function is a no-op.
 */
void bwctrl_register_stream(__be32 addr, __u32 *bw)
{
	__u32 wl_bw = mcastdwl_lookup_bw(addr);
	if (wl_bw > 0) {
		DEBMSG("bwctrl_register_stream %pI4 bw:%d [%d -> %d]", &addr, wl_bw,
				bwctrl_current_bw, bwctrl_current_bw + wl_bw);
		bwctrl_current_bw += wl_bw;
		*bw = wl_bw;
		mcastdsn_amx_current_bw_changed();
	}
}

void bwctrl_unregister_stream(__be32 addr, __u32 bw)
{
	if (bw > 0) {
		DEBMSG("bwctrl_unregister_stream %pI4 bw:%d [%d -> %d]", &addr, bw,
				bwctrl_current_bw, bwctrl_current_bw - bw);
		bwctrl_current_bw -= bw;

		if (bwctrl_current_bw < 0)
			bwctrl_current_bw = 0;

		mcastdsn_amx_current_bw_changed();
	}
}

/**
 * bwctrl_check_bw_available() - Check if there's still MC BW left
 * @bw: requested extra MC BW in bps
 *
 * Context: non-blocking
 */
int bwctrl_check_bw_available(__u32 bw)
{
	DBGMSG("Check bw available (bps) max: %d cur: %d, new: %d",
			bwctrl_max_bw, bwctrl_current_bw, bw);

	if (bwctrl_max_bw == 0 || bwctrl_max_bw == -1)
		return 1;

	if ((bwctrl_current_bw + bw) <= bwctrl_max_bw)
		return 1;

	return 0;
}

#endif /* CONFIG_MCASTD_CORE_ACCESS_CONTROL */

