/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/string.h>
#include <linux/inetdevice.h>
#include <linux/ip.h>
#include <linux/igmp.h>
#include <linux/netfilter.h>
#include <linux/slab.h>
#include <net/protocol.h>
#include <net/ip.h>
#include <net/checksum.h>
#include <linux/netfilter_ipv4.h>
#include <linux/netfilter/x_tables.h>
#include <linux/version.h>
#include <linux/if_vlan.h>

#define IDENT "snooper"
#include <mcastd/kernel.h>
#include "snooper.h" /* mcastdsn_snoop() */

#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL
#include "access_control.h" /* MCASTD_ACCESS_OK */
#include "bwctrl_amx.h" /* mcastdsn_amx_incr_bw_exceeded_cntr() */
#include "whitelist.h" /* mcastdwl_whitelist_empty() */
#include "rate_limiting.h" /* mcastdsn_rate_limit_admit() */
#endif

#ifndef vlan_tx_tag_present
#define vlan_tx_tag_present skb_vlan_tag_present
#endif
#ifndef vlan_tx_tag_get
#define vlan_tx_tag_get skb_vlan_tag_get
#endif

#define UNUSED __attribute__ ((unused))

static struct socket *mcastdsn_igmp_sock = NULL;

static const uint32_t mcmask = htonl(0xf0000000);
static const uint32_t mcbits = htonl(0xe0000000);
static const uint32_t ssdp = htonl(0xeffffffa);
static const uint32_t mclocalmask = htonl(0xffffff00);


static int mcastdsn_igmp_is_mc_address(__be32 addr)
{
	return ((addr & mcmask) == mcbits) ? 1 : 0;
}

/**
 * mcastdsn_igmp_is_addr_for_snooper() - Check if address is relevant for snooper
 *
 * @addr: address to be checked
 *
 * The function first checks if 'addr' is an IPv4 MC address. If not, it
 * returns 0. Then it checks if 'addr' is of interest to the snooper. Addresses
 * which are not, are:
 * - the address used for SSDP (239.255.255.250)
 * - addresses in the range 224.0.0.0 to 224.0.0.255: they are not routed
 * If 'addr' is one of those, the function returns 0.
 */
static int mcastdsn_igmp_is_addr_for_snooper(__be32 addr)
{
	if (!mcastdsn_igmp_is_mc_address(addr))
		return 0;
	if (unlikely(addr == ssdp))
		return 0;
	if ((addr & mclocalmask) == mcbits)
		return 0;
	return 1;
}

/**
 * mcastdsn_igmp_run() - Process the IGMP report
 *
 * Context: Process context.
 */
static void mcastdsn_igmp_run(void *data)
{
	struct sk_buff *skb = data;
	struct iphdr *iphdr;
	struct igmphdr *igmphdr;
	struct igmpv3_report *report;
	struct igmpv3_grec *grec;
	int i, ngrecs, nsrcs, naux;
	iphdr = ip_hdr(skb);
	igmphdr = (struct igmphdr *)((__u32 *)iphdr + iphdr->ihl);
	switch (igmphdr->type) {
	case IGMPV3_HOST_MEMBERSHIP_REPORT:
		report = (struct igmpv3_report *)igmphdr;
		grec = &report->grec[0];
		ngrecs = ntohs(report->ngrec);
		for (i = 0; i < ngrecs; i++, grec = (struct igmpv3_grec *)
				((__u32 *)(grec + 1) + nsrcs + naux)) {
			nsrcs = ntohs(grec->grec_nsrcs);
			naux = grec->grec_auxwords;
			if (!mcastdsn_igmp_is_addr_for_snooper(grec->grec_mca)) {
				DEBMSG("grec_mca=0x%x: not in range", ntohl(grec->grec_mca));
				continue;
			}
			mcastdsn_snoop(skb->dev->ifindex, AF_INET,
			             (__u8 *)&grec->grec_mca, grec->grec_type,
			             nsrcs, (__u8 *)grec->grec_src,
			             (__u8 *)&iphdr->saddr, 0, skb);
		}
		break;
	case IGMPV2_HOST_MEMBERSHIP_REPORT:
		/* Next check was already done in mcastdsn_igmp_tg(..). It's not a problem
		   to do the check again. */
		if (!mcastdsn_igmp_is_addr_for_snooper(igmphdr->group)) {
			DEBMSG("group=0x%x not in range", ntohl(igmphdr->group));
			break;
		}
		mcastdsn_snoop(skb->dev->ifindex, AF_INET,
		               (__u8 *)&igmphdr->group, MCASTDSN_RECORD_IS_EX, 0,
		               NULL, (__u8 *)&iphdr->saddr, 1, skb);
		break;
	case IGMP_HOST_LEAVE_MESSAGE:
		/* Next check was already done in mcastdsn_igmp_tg(..). It's not a problem
		   to do the check again. */
		if (!mcastdsn_igmp_is_addr_for_snooper(igmphdr->group)) {
			DEBMSG("group=0x%x not in range", ntohl(igmphdr->group));
			break;
		}
		mcastdsn_snoop(skb->dev->ifindex, AF_INET,
		               (__u8 *)&igmphdr->group, MCASTDSN_RECORD_TO_IN, 0,
		               NULL, (__u8 *)&iphdr->saddr, 1, skb);
		break;
	default:
		break;
	}
	return;
}

static void mcastdsn_igmp_destroy(void *data) {
	struct sk_buff *skb = data;
	kfree_skb(skb);
}

#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL

/*check if the addr is already joined*/
static int mcastdsn_igmp_joined(struct mdb_iface *iface, __be32 addr)
{
	struct mdb_group *grp;

	list_for_each_entry(grp, &iface->records, head) {
		if (!grp) {
			/* We should never end up here. */
			ERRMSG("grp == NULL !!");
			break; /* bail out */
		}
		if (grp->refs & MDB_REFMASK_SNOOPER) {
			if (grp->address.data32[0] == addr)
				return 1;
		}
	}
	return 0;
}

/**
 * mcastdsn_igmp_admit() - Check if IGMP report can be admitted.
 * @skb: IGMP report
 *
 * The function decides whether the IGMP report can be admitted based on access
 * control criteria, such as a whitelist and a maximum allowed MC bandwidth.
 *
 * Context: non-blocking
 *
 * Return:
 * * MCASTD_ACCESS_OK if report is admitted
 * * MCASTD_ACCESS_OK_UNAUTHORIZED if report is admitted due to UnauthorizedJoinRequestBehaviour
 * * MCASTD_ACCESS_NOT_IN_WHITELIST if report is rejected because the MC address
 *   is not in the whitelist
 * * MCASTD_ACCESS_EXCEEDS_MAX_BW if report is rejected because it would exceed
 *   the max MC BW
 * * MCASTD_ACCESS_EXCEEDS_MAX_GROUPS if report is rejected because it would
 *   exceed the max nr of groups
 */
static int mcastdsn_igmp_admit(struct sk_buff *skb)
{
	struct iphdr *iphdr;
	struct igmphdr *igmphdr;
	struct igmpv3_report *report;
	struct igmpv3_grec *grec;
	int i, j, ngrecs, nsrcs, naux;
	unsigned short vlanid = 0;
	int ifindex;
	int ret = MCASTD_ACCESS_OK;
	int grp_cnt = 0;
	int max_groups;
	struct mdb_group *grp;
	struct mdb_iface *iface;
	struct mcastdsn_iface *iaux;
	unsigned long irqflags;
	int njoins = 0;

	iphdr = ip_hdr(skb);
	igmphdr = (struct igmphdr *)((__u32 *)iphdr + iphdr->ihl);

	ifindex = mcastdsn_get_ifindex_of_bridge(skb->dev);

	if (vlan_tx_tag_present(skb)) {
		vlanid = vlan_tx_tag_get(skb) & VLAN_VID_MASK;
	}

	/* Note: this lock protects against addition/deletion of records, but
	 * doesn't protect against the records changing. For that you need the
	 * mdb_lock, that we can't take in this context. We're in atomic
	 * context and can't schedule...
	 * The mdb records addresses are set at creation, and don't change
	 * over the lifetime. So it should be ok.
	 * */
	spin_lock_irqsave(&mdb_record_spinlock, irqflags);
	list_for_each_entry(iface, &mdb_ifaces, head) {
		if ((iface->family == AF_INET) && (iface->ifindex == ifindex))
			break;
	}

	if (iface == NULL)
		goto leave_unlock;

	iaux = mdb_iface_auxdata(iface, MDB_REF_SNOOPER_PRIV);
	if (iaux == NULL)
		goto leave_unlock;
	max_groups = iaux->max_groups;

	if (mcastdwl_whitelist_empty() && (max_groups == -1)) {
		/* admit all joins */
		goto leave_unlock;
	}

	list_for_each_entry(grp, &iface->records, head) {
		if (!grp) {
			/* We should never end up here. */
			ERRMSG("grp == NULL !! [whitelist_empty=%d, max_groups=%d]",
				mcastdwl_whitelist_empty(), max_groups);
			break; /* bail out */
		}
		if ((grp->refs & MDB_REFMASK_SNOOPER) && (grp->unauthorized == false)) {
			grp_cnt++;
		}
	}

	switch (igmphdr->type) {
	case IGMPV3_HOST_MEMBERSHIP_REPORT:
		report = (struct igmpv3_report *)igmphdr;
		grec = &report->grec[0];
		ngrecs = ntohs(report->ngrec);

		/* If one of the group records is not allowed, reject the report. */
		for (i = 0; i < ngrecs; i++, grec = (struct igmpv3_grec *)
				((__u32 *)(grec + 1) + nsrcs + naux)) {
			nsrcs = ntohs(grec->grec_nsrcs);
			naux = grec->grec_auxwords;

			if (mcastdsn_igmp_joined(iface, grec->grec_mca)) {
				/* Uncomment next statement for extra logging */
				//DBGMSG("IGMPv3: addr=%pI4: already joined: skip checks",
				//		&grec->grec_mca);
				continue;
			}

			if (((grec->grec_type == MCASTDSN_RECORD_IS_IN) ||
				 (grec->grec_type == MCASTDSN_RECORD_TO_IN)) && (nsrcs == 0)) {
				/* This group record leaves a group. No need to do checks. */
				/* Uncomment next statement for extra logging */
				//DBGMSG("IGMPv3: addr=%pI4: type=%s && nsrcs=0: skip checks",
				//		&grec->grec_mca, mcastdsn_record_string(grec->grec_type));
				continue;
			}

			if (nsrcs == 0) {
				ret = mcastdwl_whitelisted(vlanid, grec->grec_mca, INADDR_ANY);
				if (ret > MCASTD_ACCESS_OK_UNAUTHORIZED)
					goto leave_unlock;
			} else {
				for (j = 0; j < nsrcs; j++) {
					ret = mcastdwl_whitelisted(vlanid, grec->grec_mca,
								 grec->grec_src[j]);
					if (ret > MCASTD_ACCESS_OK_UNAUTHORIZED)
						goto leave_unlock;
				}
			}
                	
			if (ret == MCASTD_ACCESS_OK) {
				++njoins;
			}
		}

		if ((max_groups != -1) && (grp_cnt + njoins > max_groups)) {
			DBGMSG("IGMPv3: grp_cnt [%d] + njoins [%d] = %d > max_groups [%d]",
					grp_cnt, njoins, (grp_cnt + njoins), max_groups);
			ret = MCASTD_ACCESS_EXCEEDS_MAX_GROUPS;
			break;
		}
		break;
	case IGMPV2_HOST_MEMBERSHIP_REPORT:
		if (mcastdsn_igmp_joined(iface, igmphdr->group))
			goto leave_unlock;

		ret = mcastdwl_whitelisted(vlanid, igmphdr->group, INADDR_ANY);

		if ((max_groups != -1) && (ret == MCASTD_ACCESS_OK) && (grp_cnt + 1 > max_groups)) {
			DBGMSG("IGMPv2: grp_cnt [%d] + 1 = %d > max_groups [%d]",
				grp_cnt, (grp_cnt + 1), max_groups);
			ret = MCASTD_ACCESS_EXCEEDS_MAX_GROUPS;
			break;
		}

		break;
	case IGMP_HOST_LEAVE_MESSAGE:
		/**
		 * Don't do any checks: a leave message does not increase grp_cnt
		 * or the MC bandwidth. And it also doesn't make sense to check if
		 * the MC address occurs in the whitelist.
		 */
		break;
	}

leave_unlock:
	spin_unlock_irqrestore(&mdb_record_spinlock, irqflags);
	DBGMSG("admit: ret: %s", (ret > MCASTD_ACCESS_OK_UNAUTHORIZED) ? "REJECT" : "ACCEPT");
	return ret;
}

/**
 * mcastdsn_igmp_incr_bw_exceeded_cntr() - Increment bw exceeded cntr
 *
 * Context: Process context.
 */
static void mcastdsn_igmp_incr_bw_exceeded_cntr(void *data UNUSED)
{
	mcastdsn_amx_incr_bw_exceeded_cntr();
}

#endif /* CONFIG_MCASTD_CORE_ACCESS_CONTROL */

/**
 * mcastdsn_igmp_tg() - Handle possible IGMP report
 *
 * Check if it's an IGMP report. If not, return NF_ACCEPT.
 *
 * If it is an IGMP report, do checks which are possible in a non-blocking
 * context. Immediately return NF_DROP if the report is not valid, e.g. if
 * its checksum is not ok.
 *
 * If the report is admitted, clone the report and schedule a job to continue
 * handling the report in process context (the mcastd kernel thread will do
 * that).
 *
 * Context: non-blocking
 *
 * Return: NF_ACCEPT or NF_DROP
 */
#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,24))
unsigned int mcastdsn_igmp_tg(struct sk_buff **pskb,
                              const struct net_device *in, const struct net_device *out,
                              unsigned int hooknum, const struct xt_target *target,
                              const void *targinfo)
{
	struct sk_buff *skb = *pskb;
#elif (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,28))
unsigned int mcastdsn_imgp_tg(struct sk_buff *skb,
                              const struct net_device *in, const struct net_device *out,
                              unsigned int hooknum, const struct xt_target *target,
                              const void *targinfo)
{
#elif (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,35))
unsigned int mcastdsn_igmp_tg(struct sk_buff *skb,
                              const struct xt_target_param *param)
{
#else
unsigned int mcastdsn_igmp_tg(struct sk_buff *skb,
                              const struct xt_action_param *param)
{
#endif
	struct iphdr *iphdr;
	struct igmphdr *igmphdr;
	struct igmpv3_report *report;
	struct igmpv3_grec *grec;
	struct mcastd_work work = {
		.run = mcastdsn_igmp_run,
		.destroy = mcastdsn_igmp_destroy,
	};
	int igmplen, pos, i, ngrecs, nsrcs, naux;
	__sum16 csum1, csum2;
	unsigned char *option;
	int optlen = 0;
	int ipopt_ra_present = 0;
	int keep_handling_options = 1;
	int at_least_1_grpaddr_in_mcrange = 0;
	int at_least_1_grpaddr_for_snooper = 0;
#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL
	int rc;
#endif

	iphdr = ip_hdr(skb);
	if (!iphdr)
		return NF_ACCEPT;
	if (iphdr->protocol != IPPROTO_IGMP)
		return NF_ACCEPT;
	if (iphdr->ttl != 1)
		return NF_DROP;
	if (iphdr->ihl * 4 > ntohs(iphdr->tot_len))
		return NF_DROP;
	if (ntohs(iphdr->tot_len) >
			skb_tail_pointer(skb) - skb_network_header(skb))
		return NF_DROP;
	for (option = (unsigned char *)iphdr + sizeof(struct iphdr);
			(option - (unsigned char *)iphdr < iphdr->ihl * 4) && keep_handling_options;
			option += optlen) {
		switch (option[0]) {
			case IPOPT_END:
				keep_handling_options = 0;
				continue;
			case IPOPT_NOOP:
				optlen = 1;
				continue;
			case IPOPT_RA:
				ipopt_ra_present = 1;
				break;
			default:
				break;
		}
		optlen = option[1];
		if (optlen == 0)
			return NF_DROP;
	}
	/* IGMPv1 packets possibly do not have the router alert option because
	 * the IGMPv1 RFC (RFC 1112) does not ask for it. Drop any IGMP packet
	 * without that option, including such IGMPv1 packets.
	 */
	if (!ipopt_ra_present)
		return NF_DROP;
	igmplen = ntohs(iphdr->tot_len) / 4 - iphdr->ihl;
	if (igmplen * 4 < sizeof(struct igmphdr))
		return NF_DROP;
	igmphdr = (struct igmphdr *)((__u32 *)iphdr + iphdr->ihl);
	if (!skb->dev)
		return NF_DROP;
	csum1 = igmphdr->csum;
	igmphdr->csum = 0;
	csum2 = ip_compute_csum(igmphdr, igmplen * 4);
	igmphdr->csum = csum1;
	if (csum1 != csum2)
		return NF_DROP;
	switch (igmphdr->type) {
	case IGMPV3_HOST_MEMBERSHIP_REPORT:
		report = (struct igmpv3_report *)igmphdr;
		grec = &report->grec[0];
		ngrecs = ntohs(report->ngrec);
		for (i = 0; i < ngrecs; i++, grec = (struct igmpv3_grec *)
				((__u32 *)(grec + 1) + nsrcs + naux)) {
			pos = (__u32 *)grec - (__u32 *)report;
			if (pos + sizeof(struct igmpv3_grec)/4 > igmplen)
				break;
			nsrcs = ntohs(grec->grec_nsrcs);
			naux = grec->grec_auxwords;
			if (pos + sizeof(*grec) / 4 + nsrcs + naux > igmplen)
				break;
			if (mcastdsn_igmp_is_mc_address(grec->grec_mca)) {
				at_least_1_grpaddr_in_mcrange = 1;
			}
			if (mcastdsn_igmp_is_addr_for_snooper(grec->grec_mca)) {
				at_least_1_grpaddr_for_snooper = 1;
				/*
				 * An IGMPv3 report can have many group records. Only call
				 * DBGMSG() for the 1st report to avoid flooding the log.
				 */
				if (i == 0) {
					DBGMSG("IGMPv3: record 0: type=%s, gaddr=%pI4, nsrcs=%d; ngrecs=%d",
					       mcastdsn_record_string(grec->grec_type), &grec->grec_mca,
					       nsrcs, ngrecs);
				}
			}
		}
		if (i < ngrecs)
			return NF_DROP;
		if (!at_least_1_grpaddr_in_mcrange) {
			DBGMSG("%d grp address(es): none is in mcrange", ngrecs);
			return NF_DROP;
		}
		if (!at_least_1_grpaddr_for_snooper) {
			DBGMSG("%d grp address(es): none for snooper", ngrecs);
			return NF_ACCEPT;
		}
		break;
	case IGMPV2_HOST_MEMBERSHIP_REPORT: /* no break */
	case IGMP_HOST_LEAVE_MESSAGE:
		if (!mcastdsn_igmp_is_mc_address(igmphdr->group)) {
			DBGMSG("group=%pI4 is not a MC address", &igmphdr->group);
			return NF_DROP;
		}
		if (!mcastdsn_igmp_is_addr_for_snooper(igmphdr->group)) {
			DBGMSG("group=%pI4 is not for snooper", &igmphdr->group);
			return NF_ACCEPT;
		}
		DBGMSG("IGMPv2 %s: graddr=%pI4",
				(igmphdr->type == IGMPV2_HOST_MEMBERSHIP_REPORT) ?
				"Membership Report" : "Leave Group", &igmphdr->group);
		break;
	case IGMP_HOST_MEMBERSHIP_REPORT:
		/* Required for GPON certification to drop IGMPv1 packets. We don't handle them
		 * anyway. Because the IGMPv1 RFC does require IGMPv1 packets to have a router
		 * alert option, they probably don't have that option. Such packets are already
		 * dropped because of the check on that option higher above.
		 */
		return NF_DROP;
	default:
		/* Not interested in any other type of IGMP packet.
		 * When running in bridge mode, L2Snooping is usually set to true in
		 * mcastd-defaults.odl. Then mcastd uses ebtables to snoop. And the ebtables
		 * rule to snoop is not for a particular interface (iptables rules to
		 * snoop normally specify 'bridge' as interface). Hence the snooper also
		 * snoops IGMP packets coming from the WAN interface, such as IGMP queries.
		 * It should of course not drop them.
		 */
		DBGMSG("IGMP msg type=%d: no interest", igmphdr->type);
		return NF_ACCEPT;
	}

#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL
	rc = mcastdsn_igmp_admit(skb);
	if (rc > MCASTD_ACCESS_OK_UNAUTHORIZED) {
		if (MCASTD_ACCESS_EXCEEDS_MAX_BW == rc) {
			/* Schedule task to increment BW exceeded cntr and to notify US */
			work.run = mcastdsn_igmp_incr_bw_exceeded_cntr;
			work.destroy = NULL;
			mcastd_work_schedule(&work, GFP_ATOMIC);
		}
		return NF_DROP;
	}

	if (!mcastdsn_rate_limit_admit(skb, AF_INET))
		return NF_DROP;
#endif

	work.data = skb_clone(skb, GFP_ATOMIC);
	if (!work.data)
		return NF_DROP;
	mcastd_work_schedule(&work, GFP_ATOMIC);
	return NF_ACCEPT;
}

static struct mcastdsn_igmp_packet {
	int mtu;
	__be32 saddr;
	__be32 daddr;
	union {
		void *raw;
		struct igmphdr *hdr;
		struct igmpv3_query *v3query;
	} pkt;
	int pktsize;
	int pktlen;
	__u8 family;
} mcastdsn_igmp_packet;

static void mcastdsn_igmp_make(struct mdb_iface *iface)
{
	mcastdsn_igmp_packet.mtu = iface->mtu;
	mcastdsn_igmp_packet.saddr = iface->address.data32[0];
	if (mcastdsn_igmp_packet.pktsize < mcastdsn_igmp_packet.mtu) {
		ERRMSG("mcastdsn_igmp_packet.pktsize[%d] < mcastdsn_igmp_packet.mtu[%d]",
		       mcastdsn_igmp_packet.pktsize,
		       mcastdsn_igmp_packet.mtu);
		mcastdsn_igmp_packet.mtu = mcastdsn_igmp_packet.pktsize;
	}
	mcastdsn_igmp_packet.pktlen = 0;
}

static void mcastdsn_igmp_send(void)
{
	struct sockaddr_in addr;
	struct msghdr msg;
	struct kvec vec;
	struct igmpv3_query *q;

	if (!mcastdsn_igmp_packet.saddr || !mcastdsn_igmp_packet.pktlen)
		return;

	q = mcastdsn_igmp_packet.pkt.v3query;
	q->csum = 0;
	q->csum = ip_compute_csum(q, mcastdsn_igmp_packet.pktlen);

	memset(&addr, 0, sizeof(struct sockaddr_in));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = mcastdsn_igmp_packet.saddr;
	if (kernel_bind(mcastdsn_igmp_sock, (struct sockaddr *)&addr,
			sizeof(addr))) {
		ERRMSG("kernel_bind() failed");
		return;
	}
	addr.sin_addr.s_addr = mcastdsn_igmp_packet.daddr;
	memset(&msg, 0, sizeof(msg));
	msg.msg_name = &addr;
	msg.msg_namelen = sizeof(addr);
	vec.iov_base = mcastdsn_igmp_packet.pkt.raw;
	vec.iov_len = mcastdsn_igmp_packet.pktlen;
	if (kernel_sendmsg(mcastdsn_igmp_sock, &msg, &vec, 1, vec.iov_len) < 0) {
		ERRMSG("kernel_sendmsg() failed");
		return;
	}
}

static void mcastdsn_igmp_query_fill(struct mdb_iface *iface, __be32 group,
                                     int sflag, unsigned long max_resp_time)
{
	struct igmpv3_query *q;
	struct mcastdsn_iface *iaux = mdb_iface_auxdata(iface, MDB_REF_SNOOPER_PRIV);

	mcastdsn_igmp_make(iface);
	q = mcastdsn_igmp_packet.pkt.v3query;
	q->type = IGMP_HOST_MEMBERSHIP_QUERY;
	q->group = group;

	switch (iaux->querier_version) {
	case 1:
		mcastdsn_igmp_packet.pktlen = sizeof(struct igmphdr);
		q->code = 0;
		break;
	case 2:
		mcastdsn_igmp_packet.pktlen = sizeof(struct igmphdr);
		q->code = (max_resp_time / 100 > 0xff) ? 0xff : max_resp_time / 100;
		break;
	case 3:
	default:
		mcastdsn_igmp_packet.pktlen = sizeof(struct igmpv3_query);
		q->code = IGMPV3_MRC(max_resp_time / 100);
		q->resv = 0;
		q->suppress = sflag;
		q->qrv = mcastdsn_RV;
		q->qqic = IGMPV3_QQIC(max_resp_time / 1000);
		q->nsrcs = 0;
		break;
	}
}

static void mcastdsn_igmp_iface_query(struct mdb_iface *iface)
{
	if (iface->address.family != AF_INET)
		return;

	if (mcastdsn_igmp_packet.pktsize < sizeof(struct igmpv3_query)) {
		ERRMSG("Not enough room to send IGMP query");
		return;
	}

	mcastdsn_igmp_query_fill(iface, 0, 0, mcastdsn_QRI);
	mcastdsn_igmp_packet.daddr = htonl(0xe0000001);
	mcastdsn_igmp_send();
}

static void mcastdsn_igmp_group_query(struct mdb_iface *iface,
                                      struct mdb_address *group, int sflag)
{
	if (iface->address.family != AF_INET || group->family != AF_INET)
		return;

	if (mcastdsn_igmp_packet.pktsize < sizeof(struct igmpv3_query)) {
		ERRMSG("Not enough room to send IGMP query");
		return;
	}

	mcastdsn_igmp_query_fill(iface, group->data32[0], sflag, mcastdsn_LMQI);
	mcastdsn_igmp_packet.daddr = group->data32[0];
	mcastdsn_igmp_send();
}

static void mcastdsn_igmp_source_query_init(struct mdb_iface *iface,
                                            struct mdb_address *group, int sflag)
{
	mcastdsn_igmp_packet.family = group->family;
	if (iface->address.family != AF_INET || group->family != AF_INET)
		return;

	if (mcastdsn_igmp_packet.pktsize < sizeof(struct igmpv3_query) + 4) {
	                                     /* at least 1 src needs to fit in*/
		ERRMSG("Not enough room to send IGMP query");
		return;
	}
	mcastdsn_igmp_query_fill(iface, group->data32[0], sflag, mcastdsn_LMQI);
	mcastdsn_igmp_packet.daddr = group->data32[0];
}

static void mcastdsn_igmp_source_query_done(void)
{
	struct igmpv3_query *q;
	if (mcastdsn_igmp_packet.family != AF_INET)
		return;
	if (mcastdsn_igmp_packet.pktsize < sizeof(struct igmpv3_query))
		return;
	q = mcastdsn_igmp_packet.pkt.v3query;
	if (!q->nsrcs)
		return;
	q->nsrcs = htons(q->nsrcs);
	mcastdsn_igmp_send();
	mcastdsn_igmp_packet.pktlen = sizeof(struct igmpv3_query);
	q->nsrcs = 0;
	q->csum = 0;
}

static void mcastdsn_igmp_source_query_push(struct mdb_address *source)
{
	struct igmpv3_query *q;
	if (source->family != AF_INET || mcastdsn_igmp_packet.family != AF_INET)
		return;
	if (mcastdsn_igmp_packet.pktsize < mcastdsn_igmp_packet.mtu)
		return;
	q = mcastdsn_igmp_packet.pkt.v3query;
	if (mcastdsn_igmp_packet.pktlen + 4 > mcastdsn_igmp_packet.mtu)
		mcastdsn_igmp_source_query_done();
	mcastdsn_igmp_packet.pktlen += 4;
	q->srcs[q->nsrcs++] = source->data32[0];
}

static struct xt_target mcastdsn_igmp_tg_reg __read_mostly = {
		.name		= "SNOOP",
		.family		= AF_INET,
		.target		= mcastdsn_igmp_tg,
		.targetsize	= 0,
		.table		= "mangle",
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,25))
		.hooks		= 1 << NF_INET_PRE_ROUTING,
#else
		.hooks		= 1 << NF_IP_PRE_ROUTING,
#endif
		.checkentry	= NULL,
		.me		= THIS_MODULE,
};

static struct mcastdsn_querier mcastdsn_igmp_querier = {
	.iface_query = mcastdsn_igmp_iface_query,
	.group_query = mcastdsn_igmp_group_query,
	.source_query_init = mcastdsn_igmp_source_query_init,
	.source_query_push = mcastdsn_igmp_source_query_push,
	.source_query_done = mcastdsn_igmp_source_query_done,
};

int mcastd_snooper_igmp_init(void)
{
	int ret;
	int zero = 0;
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(5,8,0))
	int ra = 1;
#else
	char ipopt[4] = { IPOPT_RA, 4, 0, 0 };
#endif
	int tos = IPTOS_PREC_INTERNETCONTROL;
	memset(&mcastdsn_igmp_packet, 0, sizeof(struct mcastdsn_igmp_packet));
	mcastdsn_igmp_packet.pkt.raw = kmalloc(ETH_DATA_LEN, GFP_KERNEL);
	mcastdsn_igmp_packet.pktsize = ETH_DATA_LEN;
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(4,2,0))
	ret = sock_create_kern(&init_net, AF_INET, SOCK_RAW, IPPROTO_IGMP,
	                       &mcastdsn_igmp_sock);
#else
	ret = sock_create_kern(AF_INET, SOCK_RAW, IPPROTO_IGMP,
	                       &mcastdsn_igmp_sock);
#endif

	if (ret)
		return ret;
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(5,8,0))
	sock_set_rcvbuf(mcastdsn_igmp_sock->sk, zero);
#else
	kernel_setsockopt(mcastdsn_igmp_sock, SOL_IP, SO_RCVBUF,
	                  (char *)&zero, sizeof(int));
#endif
	/* RFC 3376 - IGMPv3: "Every IGMP message described in this document is sent
	 * with an IP Time-to-Live of 1, IP Precedence of Internetwork Control
	 * (e.g., Type of Service 0xc0), and carries an IP Router Alert option
	 * [RFC-2113] in its IP header." The kernel already sets the TTL to 1 for
	 * multicast packets (see mc_ttl in the kernel). mcastd enforces the 2 other
	 * requirements in the next 2 statements. */
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(5,8,0))
	ip_setsockopt(mcastdsn_igmp_sock->sk, SOL_IP, IP_ROUTER_ALERT, KERNEL_SOCKPTR(&ra), sizeof(ra));
	ip_setsockopt(mcastdsn_igmp_sock->sk, SOL_IP, IP_TOS, KERNEL_SOCKPTR(&tos), sizeof(tos));
#else
	kernel_setsockopt(mcastdsn_igmp_sock, SOL_IP, IP_OPTIONS, ipopt, 4);
	kernel_setsockopt(mcastdsn_igmp_sock, SOL_IP, IP_TOS, (char *) &tos, sizeof(tos));
#endif
	mcastdsn_register_querier(MCASTDSN_PROTO_IGMP, &mcastdsn_igmp_querier);
	return xt_register_target(&mcastdsn_igmp_tg_reg);
}

void mcastd_snooper_igmp_cleanup(void)
{
	xt_unregister_target(&mcastdsn_igmp_tg_reg);
	mcastdsn_unregister_querier(MCASTDSN_PROTO_IGMP);

	if (mcastdsn_igmp_packet.pkt.raw)
		kfree(mcastdsn_igmp_packet.pkt.raw);
	sock_release(mcastdsn_igmp_sock);
}

