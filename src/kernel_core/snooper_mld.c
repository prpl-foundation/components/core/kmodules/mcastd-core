/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/string.h>
#include <linux/in6.h>
#include <linux/ipv6.h>
#include <linux/icmpv6.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv6.h>
#include <linux/netfilter/x_tables.h>
#include <linux/version.h>
#include <net/ipv6.h>
#include <net/ip6_checksum.h>

#define IDENT "snooper"
#include <mcastd/kernel.h>
#include <mcastd/kernel/mld.h>
#include "snooper.h" /* mcastdsn_snoop() */

static const struct in6_addr mcastdsn_mld_addr_any = IN6ADDR_ANY_INIT;
static const struct in6_addr mcastdsn_mld_addr_allnodes =
		IN6ADDR_LINKLOCAL_ALLNODES_INIT;

static struct socket *mcastdsn_mld_sock = NULL;

static inline int mcastdsn_mld_mcrange(struct in6_addr *addr) {
	if (addr->s6_addr[0] != 0xff)
		return 0;
	if (addr->s6_addr[1] <= 0x02)
		return 0;
	return 1;
}

static void mcastdsn_mld_run(void *data) {
	struct sk_buff *skb = data;
	struct ipv6hdr *ipv6hdr;
	struct mld_msg *msg;
	struct mld2_report *report;
	struct mld2_grec *grec;
	struct ipv6_opt_hdr *opt;
	int i, ngrecs, nsrcs, naux;
	__u8 nexthdr;

	ipv6hdr = ipv6_hdr(skb);

	nexthdr = ipv6hdr->nexthdr;
	opt = (struct ipv6_opt_hdr *)(ipv6hdr + 1);
	while (nexthdr == NEXTHDR_HOP) {
		nexthdr = opt->nexthdr;
		opt = (struct ipv6_opt_hdr *)((__u8 *)opt + 8 + opt->hdrlen);
	}

	msg = (struct mld_msg *)opt;

	switch (msg->mld_type) {
	case ICMPV6_MLD2_REPORT:
		report = (struct mld2_report *)msg;
		grec = &report->mld2r_grec[0];
		ngrecs = ntohs(report->mld2r_ngrec);
		for (i = 0; i < ngrecs; i++, grec = (struct mld2_grec *)
				((__u8 *)(grec + 1) + nsrcs * 16 + naux * 4)) {
			nsrcs = ntohs(grec->grec_nsrcs);
			naux = grec->grec_auxwords;
			if (!mcastdsn_mld_mcrange(&grec->grec_mca))
				continue;
			mcastdsn_snoop(skb->dev->ifindex, AF_INET6,
			               (__u8 *)&grec->grec_mca, grec->grec_type,
			               nsrcs, (__u8 *)grec->grec_src,
			               (__u8 *)&ipv6hdr->saddr, 0, skb);
		}
		break;
	case ICMPV6_MGM_REPORT:
		/* Next check was already done in mcastdsn_mld_tg(..). It's not a problem
		   to do the check again. */
		if (!mcastdsn_mld_mcrange(&msg->mld_mca))
			break;
		mcastdsn_snoop(skb->dev->ifindex, AF_INET6,
		               (__u8 *)&msg->mld_mca, MCASTDSN_RECORD_IS_EX, 0,
		               NULL, (__u8 *)&ipv6hdr->saddr, 1, skb);
		break;
	case ICMPV6_MGM_REDUCTION:
		/* Next check was already done in mcastdsn_mld_tg(..). It's not a problem
		   to do the check again. */
		if (!mcastdsn_mld_mcrange(&msg->mld_mca))
			break;
		mcastdsn_snoop(skb->dev->ifindex, AF_INET6,
		               (__u8 *)&msg->mld_mca, MCASTDSN_RECORD_TO_IN, 0,
		               NULL, (__u8 *)&ipv6hdr->saddr, 1, skb);
		break;
	default:
		break;
	}
	return;
}

static void mcastdsn_mld_destroy(void *data) {
	struct sk_buff *skb = data;
	kfree_skb(skb);
}

#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,24))
unsigned int mcastdsn_mld_tg(struct sk_buff **pskb,
                             const struct net_device *in, const struct net_device *out,
                             unsigned int hooknum, const struct xt_target *target,
                             const void *targinfo)
{
	struct sk_buff *skb = *pskb;
#elif (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,28))
unsigned int mcastdsn_mld_tg(struct sk_buff *skb,
                             const struct net_device *in, const struct net_device *out,
                             unsigned int hooknum, const struct xt_target *target,
                             const void *targinfo)
{
#elif (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,35))
unsigned int mcastdsn_mld_tg(struct sk_buff *skb,
                             const struct xt_target_param *param)
{
#else
unsigned int mcastdsn_mld_tg(struct sk_buff *skb,
                             const struct xt_action_param *param)
{
#endif
	struct ipv6hdr *ipv6hdr;
	struct mld_msg *msg;
	struct mld2_report *report;
	struct mld2_grec *grec;
	struct ipv6_opt_hdr *opt;
	__u8 nexthdr, *tail;
	struct mcastd_work work = {
		.run = mcastdsn_mld_run,
		.destroy = mcastdsn_mld_destroy,
	};
	int len, i, ngrecs, nsrcs, naux, ra;
	__sum16 csum1, csum2;
	__u16 offset = sizeof(struct ipv6hdr);
	int at_least_1_grpaddr_in_mcrange = 0;

	if (!skb->dev)
		return NF_ACCEPT;
	ipv6hdr = ipv6_hdr(skb);
	if (!ipv6hdr)
		return NF_ACCEPT;
	if (ipv6hdr->hop_limit != 1)
		return NF_ACCEPT;
	tail = (__u8 *)(ipv6hdr + 1) + ntohs(ipv6hdr->payload_len);
	if (tail > skb_tail_pointer(skb))
		return NF_ACCEPT;
	nexthdr = ipv6hdr->nexthdr;
	opt = (struct ipv6_opt_hdr *)(ipv6hdr + 1);
	ra = 0;

	while (nexthdr == NEXTHDR_HOP && (__u8 *)opt + 8 < tail) {
		if (ipv6_find_tlv(skb, offset , IPV6_TLV_ROUTERALERT) >= 0)
			ra = 1;
		offset += ipv6_optlen(opt);
		nexthdr = opt->nexthdr;
		opt = (struct ipv6_opt_hdr *)(skb_network_header(skb) + offset);
	}
	if (!ra || (__u8 *)opt > tail)
		return NF_ACCEPT;
	if (nexthdr != NEXTHDR_ICMP)
		return NF_ACCEPT;
	len = tail - (__u8 *)opt;
	if (len < (int)sizeof(struct mld_msg))
		return NF_ACCEPT;
	msg = (struct mld_msg *)opt;
	switch (msg->mld_type) {
	case ICMPV6_MLD2_REPORT:
		report = (struct mld2_report *)msg;
		grec = &report->mld2r_grec[0];
		ngrecs = ntohs(report->mld2r_ngrec);
		for (i = 0; i < ngrecs; i++, grec = (struct mld2_grec *)
				((__u8 *)(grec + 1) + nsrcs * 16 + naux * 4)) {
			if ((__u8 *)(grec + 1) > tail)
				break;
			nsrcs = ntohs(grec->grec_nsrcs);
			naux = grec->grec_auxwords;
			if ((__u8 *)(grec + 1) + nsrcs * 16 + naux * 4 > tail)
				break;
			if (mcastdsn_mld_mcrange(&grec->grec_mca))
				at_least_1_grpaddr_in_mcrange = 1;
		}
		if (i < ngrecs)
			return NF_DROP;
		if (!at_least_1_grpaddr_in_mcrange) {
			DBGMSG("%d grp address(es): none is in mcrange", ngrecs);
			return NF_DROP;
		}
		break;
	case ICMPV6_MGM_REPORT: /* no break */
	case ICMPV6_MGM_REDUCTION:
		if (!mcastdsn_mld_mcrange(&msg->mld_mca))
			return NF_DROP;
		break;
	default:
		// Not interested in any other type of ICMPv6 packet
		return NF_ACCEPT;
	}
	csum1 = msg->mld_cksum;
	msg->mld_cksum = 0;
	csum2 = csum_ipv6_magic(&ipv6hdr->saddr, &ipv6hdr->daddr, len,
			IPPROTO_ICMPV6, csum_partial(msg, len, 0));
	msg->mld_cksum = csum1;
	if (csum1 != csum2)
		return NF_DROP;
	work.data = skb_clone(skb, GFP_ATOMIC);
	if (!work.data)
		return NF_DROP;
	mcastd_work_schedule(&work, GFP_ATOMIC);
	return NF_ACCEPT;
}

static struct mcastdsn_mld_packet {
	int mtu;
	int ifindex;
	struct in6_addr saddr;
	struct in6_addr daddr;
	union {
		void *raw;
		struct mld2_query *query;
	} pkt;
	int pktsize;
	int pktlen;
	__u8 family;
} mcastdsn_mld_packet;

static void mcastdsn_mld_make(struct mdb_iface *iface)
{
	mcastdsn_mld_packet.ifindex = iface->ifindex;
	mcastdsn_mld_packet.mtu = iface->mtu;
	memcpy(&mcastdsn_mld_packet.saddr, iface->address.data, 16);
	if (mcastdsn_mld_packet.pktsize < mcastdsn_mld_packet.mtu) {
		ERRMSG("mcastdsn_mld_packet.pktsize[%d] < mcastdsn_mld_packet.mtu[%d]",
				mcastdsn_mld_packet.pktsize,
				mcastdsn_mld_packet.mtu);
		mcastdsn_mld_packet.mtu = mcastdsn_mld_packet.pktsize;
	}
	mcastdsn_mld_packet.pktlen = 0;
}

static void mcastdsn_mld_send(void)
{
	struct msghdr msg;
	struct kvec vec;
	struct sockaddr_in6 addr;
	struct mcastdsn_mld_packet *p = &mcastdsn_mld_packet;
	int ret;
	if (!p->pktlen)
		return;
	p->pkt.query->mld2q_cksum = 0;
	p->pkt.query->mld2q_cksum = csum_ipv6_magic(&p->saddr, &p->daddr,
			p->pktlen, IPPROTO_ICMPV6,
			csum_partial(p->pkt.raw, p->pktlen, 0));
	memset(&addr, 0, sizeof(addr));
	addr.sin6_family = AF_INET6;
	memcpy(&addr.sin6_addr, &p->saddr, 16);
	addr.sin6_scope_id = p->ifindex;
	if (kernel_bind(mcastdsn_mld_sock, (struct sockaddr *)&addr,
			sizeof(addr))) {
		ERRMSG("kernel_bind() failed");
		return;
	}
	memcpy(&addr.sin6_addr, &p->daddr, 16);
	memset(&msg, 0, sizeof(msg));
	msg.msg_name = &addr;
	msg.msg_namelen = sizeof(addr);
	vec.iov_base = p->pkt.raw;
	vec.iov_len = p->pktlen;
	ret = kernel_sendmsg(mcastdsn_mld_sock, &msg, &vec, 1, vec.iov_len);
	if (ret < 0) {
		ERRMSG("kernel_sendmsg() failed [ret=%d]", ret);
		return;
	}
}

static void mcastdsn_mld_query_fill(struct mdb_iface *iface, const struct in6_addr
                                    *group, int sflag, unsigned long max_resp_time)
{
	struct mld2_query *q;
	struct mcastdsn_iface *iaux = mdb_iface_auxdata(iface, MDB_REF_SNOOPER_PRIV);

	mcastdsn_mld_make(iface);
	q = mcastdsn_mld_packet.pkt.query;
	q->mld2q_type = ICMPV6_MGM_QUERY;
	q->mld2q_mca = *group;

	switch (iaux->querier_version) {
	case 1:
		mcastdsn_mld_packet.pktlen = sizeof(struct mld_msg);
		q->mld2q_mrc = (max_resp_time / 100) > 0xffff ? 0xffff :
			max_resp_time / 100;
		break;
	case 2:
	default:
		mcastdsn_mld_packet.pktlen = sizeof(struct mld2_query);
		q->mld2q_code = 0;
		q->mld2q_mrc = MLDV2_MRC(max_resp_time / 100);
		q->mld2q_resv1 = 0;
		q->mld2q_resv2 = 0;
		q->mld2q_suppress = sflag;
		q->mld2q_qrv = mcastdsn_RV;
		q->mld2q_qqic = MLDV2_MRC(max_resp_time / 1000);
		q->mld2q_nsrcs = 0;
		break;
	}
}

static void mcastdsn_mld_iface_query(struct mdb_iface *iface)
{
	if (iface->address.family != AF_INET6)
		return;
	if (mcastdsn_mld_packet.pktsize < sizeof(struct mld2_query)) {
		ERRMSG("Not enough room to send MLD query");
		return;
	}
	mcastdsn_mld_query_fill(iface, &mcastdsn_mld_addr_any, 0, mcastdsn_QI);
	mcastdsn_mld_packet.daddr = mcastdsn_mld_addr_allnodes;
	mcastdsn_mld_send();
}

static void mcastdsn_mld_group_query(struct mdb_iface *iface,
                                     struct mdb_address *group, int sflag)
{
	if (iface->address.family != AF_INET6 || group->family != AF_INET6)
		return;
	if (mcastdsn_mld_packet.pktsize < sizeof(struct mld2_query)) {
		ERRMSG("Not enough room to send MLD query");
		return;
	}

	mcastdsn_mld_query_fill(iface, (struct in6_addr *)group->data, sflag,
			mcastdsn_LMQI);
	mcastdsn_mld_packet.daddr = *((struct in6_addr *)group->data);
	mcastdsn_mld_send();
}

static void mcastdsn_mld_source_query_init(struct mdb_iface *iface,
                                           struct mdb_address *group, int sflag)
{
	if (iface->address.family != AF_INET6 || group->family != AF_INET6)
		return;
	if (mcastdsn_mld_packet.pktsize < sizeof(struct mld2_query) + 16) {
	                                     /* at least 1 src needs to fit in*/
		ERRMSG("Not enough room to send MLD query");
		return;
	}
	mcastdsn_mld_packet.family = AF_INET6;
	mcastdsn_mld_query_fill(iface, (struct in6_addr *)group->data, sflag,
	                        mcastdsn_LMQI);
	mcastdsn_mld_packet.daddr = *((struct in6_addr *)group->data);
}

static void mcastdsn_mld_source_query_done(void)
{
	struct mld2_query *q;
	if (mcastdsn_mld_packet.family != AF_INET6)
		return;
	if (mcastdsn_mld_packet.pktsize < sizeof(struct mld2_query))
		return;
	q = mcastdsn_mld_packet.pkt.query;
	if (!q->mld2q_nsrcs)
		return;
	q->mld2q_nsrcs = htons(q->mld2q_nsrcs);
	mcastdsn_mld_send();
	mcastdsn_mld_packet.pktlen = sizeof(struct mld2_query);
	q->mld2q_nsrcs = 0;
}

static void mcastdsn_mld_source_query_push(struct mdb_address *source)
{
	struct mld2_query *q;
	if (source->family != AF_INET6 || mcastdsn_mld_packet.family != AF_INET6)
		return;
	if (mcastdsn_mld_packet.pktsize < mcastdsn_mld_packet.mtu)
		return;
	q = mcastdsn_mld_packet.pkt.query;
	if (mcastdsn_mld_packet.pktlen + 16 > mcastdsn_mld_packet.mtu)
		mcastdsn_mld_source_query_done();
	mcastdsn_mld_packet.pktlen += 16;
	memcpy(&q->mld2q_srcs[q->mld2q_nsrcs++], source->data, 16);
}

static struct xt_target mcastdsn_mld_tg_reg = {
	.name        = "SNOOP",
	.family	     = AF_INET6,
	.target	     = mcastdsn_mld_tg,
	.targetsize  = 0,
	.table       = "mangle",
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,25))
	.hooks       = 1 << NF_INET_PRE_ROUTING,
#else
	.hooks       = 1 << NF_IP6_PRE_ROUTING,
#endif
	.checkentry  = NULL,
	.me          = THIS_MODULE,
};

static struct mcastdsn_querier mcastdsn_mld_querier = {
	.iface_query = mcastdsn_mld_iface_query,
	.group_query = mcastdsn_mld_group_query,
	.source_query_init = mcastdsn_mld_source_query_init,
	.source_query_push = mcastdsn_mld_source_query_push,
	.source_query_done = mcastdsn_mld_source_query_done,
};

int mcastd_snooper_mld_init(void)
{
	struct {
		struct ipv6_opt_hdr hdr;
		__u8 ra[4];
		__u8 pad[2];
	} opt = {
		.hdr = { .nexthdr = IPPROTO_ICMPV6, .hdrlen = 0 },
		.ra = { 5, 2, 0, 0 },
		.pad = { 0, 0 }
	};
	int ret;
	int zero = 0;
	memset(&mcastdsn_mld_packet, 0, sizeof(struct mcastdsn_mld_packet));
	mcastdsn_mld_packet.pkt.raw = kmalloc(ETH_DATA_LEN, GFP_KERNEL);
	mcastdsn_mld_packet.pktsize = ETH_DATA_LEN;
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(4,2,0))
	ret = sock_create_kern(&init_net, AF_INET6, SOCK_RAW, IPPROTO_ICMPV6,
	                       &mcastdsn_mld_sock);
#else
	ret = sock_create_kern(AF_INET6, SOCK_RAW, IPPROTO_ICMPV6,
	                       &mcastdsn_mld_sock);
#endif
	if (ret)
		return ret;
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(5,8,0))
	ipv6_setsockopt(mcastdsn_mld_sock->sk, IPPROTO_IPV6, IPV6_HOPOPTS,
	                KERNEL_SOCKPTR(&opt), sizeof(opt));
	sock_set_rcvbuf(mcastdsn_mld_sock->sk, zero);
#else
	kernel_setsockopt(mcastdsn_mld_sock, IPPROTO_IPV6, IPV6_HOPOPTS,
	                  (char *)&opt, sizeof(opt));
	kernel_setsockopt(mcastdsn_mld_sock, SOL_IP, SO_RCVBUF,
	                  (char *)&zero, sizeof(int));
#endif
	mcastdsn_register_querier(MCASTDSN_PROTO_MLD, &mcastdsn_mld_querier);
	return xt_register_target(&mcastdsn_mld_tg_reg);
}

void mcastd_snooper_mld_cleanup(void)
{
	xt_unregister_target(&mcastdsn_mld_tg_reg);
	mcastdsn_unregister_querier(MCASTDSN_PROTO_MLD);
	if (mcastdsn_mld_packet.pkt.raw)
		kfree(mcastdsn_mld_packet.pkt.raw);
	sock_release(mcastdsn_mld_sock);
}

