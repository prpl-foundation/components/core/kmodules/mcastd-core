# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.1.5 - 2024-05-27(10:37:06 +0000)

### Fixes

- [Random] feed_mcastd/mcastd-core/compile failed to build

### Other

- enable opensourcing

## Release v0.1.4 - 2024-05-22(21:09:55 +0000)

### Fixes

- Duplicated exported symbols error when recompiling kernel module

## Release v0.1.3 - 2024-05-06(07:10:53 +0000)

### Other

- Data model parameters needed for IGMPs in Routing Page

## Release v0.1.2 - 2024-04-23(14:26:00 +0000)

### Other

- Port multicast proxy plugin (KMCD) to amx

## Release v0.1.1 - 2023-12-05(15:51:46 +0000)

### Other

- Use MCASTD_DEBUG option to enable debug section

## Release v0.1.0 - 2023-12-05(11:15:43 +0000)

### New

- port multicast proxy plugin to amx

### Other

- Opensource component

