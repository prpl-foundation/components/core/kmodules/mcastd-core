/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __MCASTD_KERNEL_DEBUG_H__
#define __MCASTD_KERNEL_DEBUG_H__

#include <linux/version.h>

#ifndef IDENT
#define IDENT "anonymous"
#endif

/**
 * Copy-paste from sound/pci/asihpi/hpidebug.h
 */
#ifndef compile_time_assert
#define compile_time_assert(cond, msg) \
    typedef char msg[(cond) ? 1 : -1]
#endif

#undef CONFIG_CROSS_COMPILE

#ifdef CONFIG_MCASTD_CORE_DEBUG
/**
 * Log debug message if mcastd_debug_enable is set and trace zone IDENT is enabled.
 *
 * DEBMSG(..) MUST NOT be called in code where it's not allowed to sleep, such
 * as in atomic context (e.g. interrupt handler), within a spinlock-protected
 * critical section, etc. Reason: DEBMSG(..) calls mutex_lock(..), which tries
 * to sleep if can't lock the mutex. If if tries to sleep while it's not
 * allowed, following kernel oops occurs:
 * "BUG: scheduling while atomic".
 * The mutex DEBMSG(..) locks protects the zone information. Use DBGMSG(..) if
 * you want to print a debug message in code where it's not allowed to sleep.
 */
#define DEBMSG(fmt, ...) do {                                                  \
		if (likely(!atomic_read(&mcastd_debug_enable)))                  \
			break;                                                 \
		mcastd_debug_msg(IDENT, KERN_DEBUG "mcastd_" IDENT ": " fmt       \
		      " @ %s():%d\n", ##__VA_ARGS__ , __FUNCTION__, __LINE__); \
	} while (0)
#else
#define DEBMSG(fmt, ...)
#endif

#ifdef CONFIG_MCASTD_CORE_DEBUG
/**
 * Log debug message if mcastd_debug_enable is set.
 *
 * Do not check any trace zone.
 */
#define DBGMSG(fmt, ...) do {                                                  \
		if (likely(!atomic_read(&mcastd_debug_enable)))                  \
			break;                                                 \
		printk(KERN_DEBUG "mcastd_" IDENT ": " fmt                      \
		      " @ %s():%d\n", ##__VA_ARGS__ , __FUNCTION__, __LINE__); \
	} while (0)
#else
#define DBGMSG(fmt, ...)
#endif

#define ERRMSG(fmt, ...) printk(KERN_ERR "mcastd_" IDENT ": " fmt \
             " @ %s():%d\n", ##__VA_ARGS__ , __FUNCTION__, __LINE__)

#ifdef CONFIG_MCASTD_CORE_DEBUG
#define MCASTD_ASSERT(condition) do {                       \
		if (likely(condition))                    \
			break;                            \
		ERRMSG("ASSERT(" #condition ") failed."); \
	} while (0)
#else
#define MCASTD_ASSERT(condition)
#endif

extern atomic_t mcastd_debug_enable;

void mcastd_debug_msg(const char *ident, const char *fmt, ...);

#if LINUX_VERSION_CODE < KERNEL_VERSION(4,17,0)
const char *netdev_cmd_to_name(unsigned long cmd);
#endif

#endif
