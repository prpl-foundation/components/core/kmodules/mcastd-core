/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/list.h>
#include <linux/mutex.h>
#include <linux/slab.h>
#include <linux/string.h>
#include <linux/types.h>
#include <linux/socket.h>
#include <linux/fs.h>
#include <linux/dcache.h>

#include <mcastd/kernel/mdb.h>

#define IDENT "mdb"
#include <mcastd/kernel/debug.h>

#include "bwctrl.h"

struct mdb_record;

/* Generic mdb record
 *
 * All mdb records (interface records, group records, source records and host
 * records) start with following fields.
 *
 * - head: to be part of list of children of parent (parent->children).
 *         For iface records it's used to be part of the list mdb_ifaces.
 * - children: children of this record. E.g. an interface record has group
 *             records as children. Host records don't have children.
 * - parent: not applicable for an iface record
 * - auxlen: length (in bytes) of auxiliary data
 */
struct mdb_record {
	struct list_head head;
	struct list_head children;
	struct mdb_record *parent;
	__u32 refs;
	__u32 change;
	size_t auxlen;
	struct mdb_address address; // not valid for iface record!
};

struct mdb_subscriber {
	struct list_head head;
	mdb_event_cb cb;
};

struct mdb_aux {
	const char *name;
	size_t size;
	int (*data2var)(struct kvarbuf *buf, struct kvar *var, void *data);
	size_t offset;
};

LIST_HEAD(mdb_ifaces);

/*
 * List of group records. A group can be the child of more than 1 interface.
 */
LIST_HEAD(mdb_groups);
static struct list_head mdb_subscribers[MDB_PRIORITY_COUNT];
static struct mdb_aux mdb_auxtable[MDB_LEVEL_COUNT][MDB_REF_COUNT];
/*
 * Array indicating how many bytes of auxiliary data are registered per level.
 */
static size_t mdb_auxlen[MDB_LEVEL_COUNT];
DEFINE_MUTEX(mdb_mutex);

#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL
/*spinlock to protects against addition/deletion of mdb_records.
 * This was added because sometimes we must access the mdb from an atomic
 * context, and we can't take the mdb_lock() because that's a mutex.*/
DEFINE_SPINLOCK(mdb_record_spinlock);
#endif

const char *mdb_address_to_string(struct mdb_address *addr)
{
	static char buf[4][64];
	static int bufi = 0;
	bufi = (bufi + 1) % 4;
	switch (addr->family) {
	case AF_INET:
		sprintf(buf[bufi], "%u.%u.%u.%u", addr->data[0], addr->data[1],
				addr->data[2], addr->data[3]);
		break;
	case AF_INET6: {
		char *s = buf[bufi];
		int i = 0, j = 0, k = -1, l = 0;
		for (i = 0; i < 8; i++) {
			if (addr->data16[i]) {
				j = i + 1;
			} else if (i - j > k - l) {
				k = i;
				l = j;
			}
		}
		for (i = 0; i < 8; i++) {
			if (i < l || i > k)
				s += sprintf(s, "%x", ntohs(addr->data16[i]));
			if (i <= l || (i > k && i != 7))
				*s++ = ':';
			if (i == l && i == 0)
				*s++ = ':';
		}
		*s++ = '\0';
		break;
	}
	case AF_UNIX: {
		struct socket *sock;
		memcpy(&sock, addr->data, sizeof(struct socket *));
		if (sock && sock->file)
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,25))
			return d_path(&sock->file->f_path,
#else
			return d_path(sock->file->f_path.dentry, 
					sock->file->f_path.mnt,
#endif
					buf[bufi], ARRAY_SIZE(buf[bufi]));
		else
			sprintf(buf[bufi], "%p", sock);
		break;
	}
	default:
		sprintf(buf[bufi], "<invalid>");
		break;
	}
	return buf[bufi];
}

const char *family_to_string(sa_family_t family)
{
	switch (family)
	{
	case AF_UNSPEC: return "AF_UNSPEC";
	case AF_INET  : return "AF_INET";
	case AF_INET6 : return "AF_INET6";
	case AF_UNIX  : return "AF_UNIX";
	default: break;
	}
	return "<unknown>";
}

void mdb_lock()
{
	mutex_lock(&mdb_mutex);
}

void mdb_unlock()
{
	mutex_unlock(&mdb_mutex);
}

/*
 * Create mdb record of size @a size. Add it to list of children of @a parent.
 *
 * @param[in] tail : if true, add record to tail of children of parent, else
 *                   add it to the head.
 */
struct mdb_record *mdb_record_create(size_t size, struct mdb_record *parent,
                                     size_t auxlen,
                                     const struct mdb_address *address,
                                     int tail)
{
	struct mdb_record *record = kzalloc(size, GFP_KERNEL);
#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL
	unsigned long irqflags;
#endif

	if (!record) {
		ERRMSG("Out of memory");
		return NULL;
	}

#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL
	spin_lock_irqsave(&mdb_record_spinlock, irqflags);
#endif
	if (parent) {
		if (tail)
			list_add_tail(&record->head, &parent->children);
		else
			list_add(&record->head, &parent->children);

	}
	record->parent = parent;
	INIT_LIST_HEAD(&record->children);
	record->auxlen = auxlen;
	if (address)
		memcpy(&record->address, address, sizeof(struct mdb_address));
#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL
	spin_unlock_irqrestore(&mdb_record_spinlock, irqflags);
#endif
	return record;
}

/*
 * Create mdb_iface and add it to tail of @a mdb_ifaces.
 */
struct mdb_iface *mdb_iface_create(const char *name)
{
	size_t auxlen = mdb_auxlen[MDB_LEVEL_IFACE];
	size_t size = sizeof(struct mdb_iface) + auxlen;
	struct mdb_iface *iface = (struct mdb_iface *)mdb_record_create(size,
			NULL, auxlen, NULL, 1);
#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL
	unsigned long irqflags;
#endif

	if (!iface)
		return NULL;
#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL
	spin_lock_irqsave(&mdb_record_spinlock, irqflags);
#endif
	list_add_tail(&iface->head, &mdb_ifaces);
#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL
	spin_unlock_irqrestore(&mdb_record_spinlock, irqflags);
#endif
	strlcpy(iface->name, name, MDB_IFACE_NAMESIZE);
	return iface;
}

/*
 * Create mdb_group and add it to tail of children of @a iface.
 *
 * Find mdb_group record with same address in @a mdb_groups. Then
 * - if it exists, add this new record to list mdups of that record
 * - else add this new record to @a mdb_groups and let mlead of this
 *     new record point to this new record (so let it point to itself).
 */
struct mdb_group *mdb_group_create(struct mdb_iface *iface,
                                   const struct mdb_address *address)
{
	size_t auxlen = mdb_auxlen[MDB_LEVEL_GROUP];
	size_t size = sizeof(struct mdb_group) + auxlen;
	struct mdb_group *group = (struct mdb_group *)mdb_record_create(size,
			(struct mdb_record *)iface, auxlen, address, 1);
	if (!group)
		return NULL;
	group->mlead = mdb_group_mfind(address);
	if (group->mlead) {
		list_add_tail(&group->mdups, &group->mlead->mdups);
	} else {
		list_add_tail(&group->mhead, &mdb_groups);
		group->mlead = group;
		INIT_LIST_HEAD(&group->mdups);
		INIT_LIST_HEAD(&group->msources);
	}
#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL
	if (iface->family == AF_INET) {
		bwctrl_register_stream(address->data32[0], &group->bw);
	}
#endif
	return group;
}

/*
 * Create mdb_source and add it to children of @a group.
 *
 * Add it to tail of children of @a group if @a tail is 1, else add it to the
 * front.
 *
 * Find mdb_source record with same address in @a group->mlead->msources. In
 * other words, find mdb_source with same source address belonging to a group
 * with the same group address. Then
 * - if it exists, add this new record to list mdups of that record
 * - else add this new record to @a group->mlead->msources and let mlead of this
 *     new record point to this new record (so let it point to itself).
 */
static
struct mdb_source *mdb_source_create_priv(struct mdb_group *group,
                                          const struct mdb_address *address,
                                          int tail)
{
	size_t auxlen = mdb_auxlen[MDB_LEVEL_SOURCE];
	size_t size = sizeof(struct mdb_source) + auxlen;
	struct mdb_source *source = (struct mdb_source *)mdb_record_create(size,
			(struct mdb_record *)group, auxlen, address, tail);
	if (!source)
		return NULL;
	source->mlead = mdb_source_mfind(group, address);
	if (source->mlead) {
		list_add_tail(&source->mdups, &source->mlead->mdups);
	} else {
		list_add_tail(&source->mhead, &group->mlead->msources);
		source->mlead = source;
		INIT_LIST_HEAD(&source->mdups);
	}
	return source;
}

/*
 * Create mdb_source and add it to tail of children of @a group.
 *
 * Set the address of the new record to @a address.
 */
struct mdb_source *mdb_source_create(struct mdb_group *group,
                                     const struct mdb_address *address)
{
	return mdb_source_create_priv(group, address, 1);
}

/*
 * Create mdb_host and add it to tail of children of @a source.
 */
struct mdb_host *mdb_host_create(struct mdb_source *source,
                                 const struct mdb_address *address)
{
	size_t auxlen = mdb_auxlen[MDB_LEVEL_HOST];
	size_t size = sizeof(struct mdb_host) + auxlen;
	return (struct mdb_host *)mdb_record_create(size,
			(struct mdb_record *)source, auxlen, address, 1);
}

/*
 * Create mdb_source and add it to head of children of @a group.
 *
 * Set the address of the new record to the any address (all zeroes).
 */
struct mdb_source *mdb_group_create_any_source(struct mdb_group *group)
{
	struct mdb_address address;
	address.family = group->address.family;
	memset(address.data, 0, mdb_address_size(&address));
	return mdb_source_create_priv(group, &address, 0);
}

/*
 * Find the mdb_iface with @a name in the list mdb_ifaces.
 */
struct mdb_iface *mdb_iface_find(const char *name)
{
	struct mdb_iface *iface;
	list_for_each_entry(iface, &mdb_ifaces, head) {
		if (!strcmp(iface->name, name))
			return iface;
	}
	return NULL;
}

/*
 * Find record with certain address in children of @a parent.
 */
static struct mdb_record *mdb_record_find(struct mdb_record *parent,
                                          const struct mdb_address *address)
{
	struct mdb_record *record;
	if (!parent)
		return NULL;
	list_for_each_entry(record, &parent->children, head) {
		if (record->address.family != address->family)
			continue;
		if (!memcmp(record->address.data, address->data,
				mdb_address_size(address)))
			return record;
	}
	return NULL;
}

/*
 * Find group record with @a address in children of @a iface.
 */
struct mdb_group *mdb_group_find(struct mdb_iface *iface,
                                 const struct mdb_address *address)
{
	return (struct mdb_group *)mdb_record_find(
			(struct mdb_record *)iface, address);
}

/*
 * Find source record with @a address in children of @a group.
 */
struct mdb_source *mdb_source_find(struct mdb_group *group,
                                   const struct mdb_address *address)
{
	return (struct mdb_source *)mdb_record_find(
			(struct mdb_record *)group, address);

}

/*
 * Find host record with @a address in children of @a source.
 */
struct mdb_host *mdb_host_find(struct mdb_source *source,
                               const struct mdb_address *address)
{
	return (struct mdb_host *)mdb_record_find(
			(struct mdb_record *)source, address);
}

/*
 * Find mdb_group with certain address in mdb_groups.
 */
struct mdb_group *mdb_group_mfind(const struct mdb_address *address)
{
	struct mdb_group *group;
	list_for_each_entry(group, &mdb_groups, mhead) {
		if (group->address.family != address->family)
			continue;
		if (!memcmp(group->address.data, address->data,
				mdb_address_size(address)))
			return group;
	}
	return NULL;
}

/*
 * Find mdb_source with certain address in @a group->mlead->msources.
 */
struct mdb_source *mdb_source_mfind(struct mdb_group *group,
                                    const struct mdb_address *address)
{
	struct mdb_source *source;
	list_for_each_entry(source, &group->mlead->msources, mhead) {
		if (source->address.family != address->family)
			continue;
		if (!memcmp(source->address.data, address->data,
				mdb_address_size(address)))
			return source;
	}
	return NULL;

}

/*
 * Release mdb_iface instance if no longer in use.
 *
 * Remove it from the list mdb_ifaces and delete it if:
 * - it doesn't have any groups/children and
 * - it doesn't have any refs
 *
 * NOTE - During normal operation, AMX has a ref on each interface (cfr
 *        MDB_REF_AMX), and hence mcastd normally does not release mdb_iface
 *        instances.
 */
void mdb_iface_release(struct mdb_iface *iface)
{
	if (list_empty(&iface->records) && !iface->refs) {
		list_del(&iface->head);
		kfree(iface);
	}
}

static void mdb_group_mrelease(struct mdb_group *group)
{
	struct mdb_group *next, *dup;
	if (group->mlead == group) {
		next = list_first_entry(&group->mdups, struct mdb_group, mdups);
		if (next != group) {
			list_replace(&group->mhead, &next->mhead);
			list_replace(&group->msources, &next->msources);
			list_for_each_entry(dup, &group->mdups, mdups)
				dup->mlead = next;
		} else {
			list_del(&group->mhead);
		}
	}
	list_del(&group->mdups);
}

static void mdb_source_mrelease(struct mdb_source *source)
{
	struct mdb_source *next, *dup;
	if (source->mlead == source) {
		next = list_first_entry(&source->mdups, struct mdb_source,
		                        mdups);
		if (next != source) {
			list_replace(&source->mhead, &next->mhead);
			list_for_each_entry(dup, &source->mdups, mdups)
				dup->mlead = next;
		} else {
			list_del(&source->mhead);
		}
	}
	list_del(&source->mdups);
}

/*
 * Find out if there are records changed.
 *
 * The function iterates over @a record and all its children. For each such
 * record, it ORs its change field with event->change[record_level] where
 * record_level is the level the record belongs to. So if the function returns,
 * event->change[level], event->change[level + 1], etc indicate whether
 * there are changes on those levels.
 */
static void mdb_precommit(struct mdb_event *event, struct mdb_record *record,
                          int level)
{
	struct mdb_record *child;
	if (level < MDB_LEVEL_HOST) {
		list_for_each_entry(child, &record->children, head)
			mdb_precommit(event, child, level + 1);
	}
	event->change[level] |= record->change;
}

/*
 * Function this mdb module calls when it has finished committing an mdb_record.
 *
 * Delete all records which are no longer in use. A record is no longer in use
 * when its field refs is 0 and the field refs of all its (direct and indirect)
 * children is also 0.
 *
 * Return: OR of the 'refs' field of @a record and the 'refs' field of all its
 *         (direct and indirect) children
 */
static __u32 mdb_postcommit(struct mdb_record *record, int level)
{
	struct mdb_record *child, *n;
	__u32 ret = 0;
#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL
	unsigned long irqflags;
#endif

	record->change = 0;
	if (level < MDB_LEVEL_HOST) {
		list_for_each_entry_safe(child, n, &record->children, head)
			ret |= mdb_postcommit(child, level + 1);
	}
	ret |= record->refs;
	if (!ret) {
		DBGMSG("Release record {level=%u address=%s}",
				level, mdb_address_to_string(&record->address));
		switch (level) {
		case MDB_LEVEL_GROUP:
			mdb_group_mrelease((struct mdb_group *)record);
			break;
		case MDB_LEVEL_SOURCE:
			mdb_source_mrelease((struct mdb_source *)record);
			break;
		default:
			break;
		}
#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL
		if (level == MDB_LEVEL_GROUP) {
			struct mdb_group *group = (struct mdb_group *)record;
			if ((group->address.family == AF_INET) && group->bw) {
				bwctrl_unregister_stream(group->address.data32[0],
						group->bw);
			}
		}
		spin_lock_irqsave(&mdb_record_spinlock, irqflags);
#endif
		list_del(&record->head);
		kfree(record);
#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL
		spin_unlock_irqrestore(&mdb_record_spinlock, irqflags);
#endif
	}
	return ret;
}

/*
 * Commit the mdb_group @a group.
 *
 * The function first calls mdb_precommit(..) to find out if there are any
 * changes in the mdb_group or any of its (direct and indirect) children. If
 * there is a change, then the function calls the callback function of all
 * subscribers, and it calls them according to priority.
 *
 * Then the function calls:
 * - mdb_postcommit(..) and
 * - mdb_iface_release(group->iface).
 */
void mdb_commit(struct mdb_group *group, struct sk_buff *skb)
{
	struct mdb_subscriber *subscriber;
	struct mdb_iface *iface = group->iface;
	struct mdb_event event = {{0, 0, 0, 0}, group, skb};
	int i;

	mdb_precommit(&event, (struct mdb_record *)group, MDB_LEVEL_GROUP);

	DBGMSG("Before commit %s change=[0x%x 0x%x 0x%x]",
			mdb_address_to_string(&group->address),
			event.change[MDB_LEVEL_GROUP],
			event.change[MDB_LEVEL_SOURCE],
			event.change[MDB_LEVEL_HOST]);

	for (i = 0; i < MDB_LEVEL_COUNT; i++)
		if (event.change[i])
			break;
	// Only call callback function if at least something has changed
	if (i < MDB_LEVEL_COUNT) for (i = 0; i < MDB_PRIORITY_COUNT; i++)
		list_for_each_entry(subscriber, &mdb_subscribers[i], head)
			subscriber->cb(&event);

	DBGMSG("After commit %s", mdb_address_to_string(&group->address));

	mdb_postcommit((struct mdb_record *)group, MDB_LEVEL_GROUP);

	mdb_iface_release(iface);
}

void mdb_subscribe(enum mdb_priority priority, mdb_event_cb cb)
{
	struct mdb_subscriber *subscriber =
			kmalloc(sizeof(struct mdb_subscriber), GFP_KERNEL);
	if (!subscriber) {
		ERRMSG("Out of memory");
		return;
	}
	list_add_tail(&subscriber->head, &mdb_subscribers[priority]);
	subscriber->cb = cb;
}

void mdb_unsubscribe(enum mdb_priority priority, mdb_event_cb cb)
{
	struct mdb_subscriber *subscriber;
	list_for_each_entry(subscriber, &mdb_subscribers[priority], head) {
		if (subscriber->cb != cb)
			continue;
		list_del(&subscriber->head);
		kfree(subscriber);
		break;
	}
}

/*
 * Module info->ref registers info->size auxiliary bytes for level info->level.
 *
 * E.g if a module registers 5 bytes auxiliary data at level MDB_LEVEL_IFACE,
 * then mdb_iface_create(..) will allocate 5 bytes extra when allocating memory
 * for an mdb_source instance.
 *
 * Modules can register auxiliary data in a random order.
 *
 * Assume SNOOPER_PRIV registers a few auxiliary bytes for level
 * MDB_LEVEL_IFACE, and then SNOOPER_IN, then mdb.c will allocate following
 * bytes for an mdb_iface:
 *
 *   mdb_iface
 *   +----------+  -------------
 *   |head      |    /|\     /|\
 *   |----------|     |       |
 *   |records   |     |       |
 *   |----------|     | [1]   | [2]
 *   |          |     |       |
 *   |          |     |       |
 *   |----------|     |       |
 *   |mtu       |    \|/      |
 *   +----------+  -----------|-----
 *   |          |             |  /|\
 *   |          |             |   |
 *   |          |            \|/  | mdb_auxlen[MDB_LEVEL_IFACE]
 *   +----------+  -------------  |
 *   |          |                 |
 *   |          |                \|/
 *   +----------+  -----------------
 *
 * [1] mdb_auxtable[MDB_LEVEL_IFACE][MDB_REF_SNOOPER_PRIV].offset
 * [2] mdb_auxtable[MDB_LEVEL_IFACE][MDB_REF_SNOOPER_IN].offset
 */
void mdb_register_aux(struct mdb_auxinfo *info)
{
	struct mdb_aux *aux = &mdb_auxtable[info->level][info->ref];
	aux->name = info->name;
	aux->data2var = info->data2var;
	/* Next line protects against a module attempting to call this function
	 * more than once for the same level. But why not write following line at
	 * the start of this function ?
	 * if (aux->name != NULL) return;
	 */
	if (info->size == aux->size)
		return;
	aux->size = info->size;
	aux->offset = mdb_auxlen[info->level];
	mdb_auxlen[info->level] += NLMSG_ALIGN(aux->size);
}

/*
 * Module info->ref unregisters any auxiliary data at level info->level.
 */
void mdb_unregister_aux(struct mdb_auxinfo *info)
{
	struct mdb_aux *aux = &mdb_auxtable[info->level][info->ref];
	aux->name = NULL;
	aux->data2var = NULL;
}

/*
 * Return offset of auxiliary data for module @a ref at level @a level.
 *
 * The offset is relative to the start of the record used for that level, e.g.
 * the start of an mdb_iface record when level is MDB_LEVEL_IFACE.
 */
size_t mdb_aux_offset(enum mdb_level level, enum mdb_ref ref)
{
	static size_t mdb_record_size[MDB_LEVEL_COUNT] = {
		sizeof(struct mdb_iface),
		sizeof(struct mdb_group),
		sizeof(struct mdb_source),
		sizeof(struct mdb_host)
	};
	return mdb_record_size[level] + mdb_auxtable[level][ref].offset;
}

/*
 * Put the auxiliary data of a module @a ref for a certain level in @a buf.
 *
 * This function is typically called because AMX queries those data.
 *
 * @param[in,out] buf : buffer in which to put the data
 * @param[in,out] var :
 * @param[in] level
 * @param[in] ref
 * @param[in] data : pointer to auxiliary data of module @a ref at
 *                   level @a level
 *
 * For example, if @a level is MDB_LEVEL_IFACE and @a ref is
 * MDB_REF_SNOOPER_PRIV, then @a data is pointer to a mcastdsn_iface.
 */
int mdb_aux2var(struct kvarbuf *buf, struct kvar *var,
                enum mdb_level level, enum mdb_ref ref, void *data)
{
	struct mdb_aux *aux = &mdb_auxtable[level][ref];
	if (!aux->name || !aux->data2var || !data)
		return -EINVAL;
	return aux->data2var(buf, var, data);
}

const char *mdb_aux_name(enum mdb_level level, enum mdb_ref ref)
{
	return mdb_auxtable[level][ref].name;
}

static void mdb_record_destroy(struct mdb_record *record, int level)
{
#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL
	unsigned long irqflags;
#endif

	if (level < MDB_LEVEL_HOST) {
		while (!list_empty(&record->children))
			mdb_record_destroy(list_first_entry(&record->children,
					struct mdb_record, head), level + 1);
	}
	if (record->refs) {
		ERRMSG("Destroying record of level %d with refs=0x%x",
		       level, record->refs);
	}
	switch (level) {
	case MDB_LEVEL_GROUP:
		mdb_group_mrelease((struct mdb_group *)record);
		break;
	case MDB_LEVEL_SOURCE:
		mdb_source_mrelease((struct mdb_source *)record);
		break;
	default:
		break;
	}
#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL
	spin_lock_irqsave(&mdb_record_spinlock, irqflags);
#endif
	list_del(&record->head);
	kfree(record);
#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL
	spin_unlock_irqrestore(&mdb_record_spinlock, irqflags);
#endif
}

int mcastd_mdb_init(void)
{
	int i;
	for (i = 0; i < MDB_PRIORITY_COUNT; i++) {
		INIT_LIST_HEAD(&mdb_subscribers[i]);
	}
	memset(mdb_auxtable, 0, sizeof(mdb_auxtable));
	memset(mdb_auxlen, 0, sizeof(mdb_auxlen));
	return 0;
}

void mcastd_mdb_cleanup(void)
{
	int i;
	struct mdb_subscriber *subscriber;
	struct mdb_iface *iface;
	while (!list_empty(&mdb_ifaces)) {
		iface = list_first_entry(&mdb_ifaces, struct mdb_iface, head);
		mdb_record_destroy((struct mdb_record *)iface, MDB_LEVEL_IFACE);
	}
	for (i = 0; i < MDB_PRIORITY_COUNT; i++) {
		while (!list_empty(&mdb_subscribers[i])) {
			subscriber = list_first_entry(&mdb_subscribers[i],
					struct mdb_subscriber, head);
			DBGMSG("Late unsubscribe of %p", subscriber->cb);
			list_del(&subscriber->head);
			kfree(subscriber);
		}
	}
}

EXPORT_SYMBOL(mdb_ifaces);
EXPORT_SYMBOL(mdb_groups);
EXPORT_SYMBOL(mdb_mutex);
EXPORT_SYMBOL(mdb_address_to_string);
EXPORT_SYMBOL(mdb_lock);
EXPORT_SYMBOL(mdb_unlock);
EXPORT_SYMBOL(mdb_iface_create);
EXPORT_SYMBOL(mdb_group_create);
EXPORT_SYMBOL(mdb_source_create);
EXPORT_SYMBOL(mdb_host_create);
EXPORT_SYMBOL(mdb_group_create_any_source);
EXPORT_SYMBOL(mdb_iface_find);
EXPORT_SYMBOL(mdb_group_find);
EXPORT_SYMBOL(mdb_group_mfind);
EXPORT_SYMBOL(mdb_source_find);
EXPORT_SYMBOL(mdb_source_mfind);
EXPORT_SYMBOL(mdb_host_find);
EXPORT_SYMBOL(mdb_iface_release);
EXPORT_SYMBOL(mdb_commit);
EXPORT_SYMBOL(mdb_subscribe);
EXPORT_SYMBOL(mdb_unsubscribe);
EXPORT_SYMBOL(mdb_register_aux);
EXPORT_SYMBOL(mdb_unregister_aux);
EXPORT_SYMBOL(mdb_aux_offset);
EXPORT_SYMBOL(mdb_aux_name);
EXPORT_SYMBOL(mdb_aux2var);

