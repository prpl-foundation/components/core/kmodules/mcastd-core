/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <linux/kernel.h>
#include <linux/module.h>

#define IDENT "core"
#include <mcastd/kernel/debug.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Dries De Winter <dries.dewinter@softathome.com>");
MODULE_DESCRIPTION("MultiCAST Daemon - Core functionality");
MODULE_VERSION("v1.0");

extern int  mcastd_daemon_init(void);
extern void mcastd_daemon_cleanup(void);
extern int  mcastd_netlink_init(void);
extern void mcastd_netlink_cleanup(void);
extern int  mcastd_kamx_init(void);
extern void mcastd_kamx_cleanup(void);
extern int  mcastd_debug_init(void);
extern void mcastd_debug_cleanup(void);
extern int  mcastd_mdb_init(void);
extern void mcastd_mdb_cleanup(void);
extern int  mcastd_mdb_amx_init(void);
extern void mcastd_mdb_amx_cleanup(void);
extern int  mcastd_snooper_init(void);
extern void mcastd_snooper_cleanup(void);
extern int  mcastd_snooper_igmp_init(void);
extern void mcastd_snooper_igmp_cleanup(void);
extern int  mcastd_snooper_mld_init(void);
extern void mcastd_snooper_mld_cleanup(void);
extern int  mcastd_snooper_ebt_init(void);
extern void mcastd_snooper_ebt_cleanup(void);
extern int  mcastd_snooper_amx_init(void);
extern void mcastd_snooper_amx_cleanup(void);
#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL
extern int  mcastd_bwctrl_amx_init(void);
extern void mcastd_bwctrl_amx_cleanup(void);
extern int  mcastd_whitelist_init(void);
extern void mcastd_whitelist_cleanup(void);
extern int  mcastd_whitelist_amx_init(void);
extern void mcastd_whitelist_amx_cleanup(void);
#endif

static struct mcastd_part {
	int (*init_fn)(void);
	void (*cleanup_fn)(void);
	int up;
} mcastd_parts[] = {
	{ mcastd_netlink_init,      mcastd_netlink_cleanup,      0},
	{ mcastd_kamx_init,         mcastd_kamx_cleanup,         0},
	{ mcastd_debug_init,        mcastd_debug_cleanup,        0},
	{ mcastd_mdb_init,          mcastd_mdb_cleanup,          0},
	{ mcastd_mdb_amx_init,      mcastd_mdb_amx_cleanup,      0},
	{ mcastd_daemon_init,       mcastd_daemon_cleanup,       0},
	{ mcastd_snooper_init,      mcastd_snooper_cleanup,      0},
	{ mcastd_snooper_igmp_init, mcastd_snooper_igmp_cleanup, 0},
	{ mcastd_snooper_mld_init,  mcastd_snooper_mld_cleanup,  0},
	{ mcastd_snooper_ebt_init,  mcastd_snooper_ebt_cleanup,  0},
	{ mcastd_snooper_amx_init,  mcastd_snooper_amx_cleanup,  0},
#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL
	{ mcastd_bwctrl_amx_init,   mcastd_bwctrl_amx_cleanup,   0},
	{ mcastd_whitelist_init,    mcastd_whitelist_cleanup,    0},
	{ mcastd_whitelist_amx_init,mcastd_whitelist_amx_cleanup,    0},
#endif
};

static void mcastd_cleanup(void)
{
	int i;

	DBGMSG("Unloading mcastd core ...");

	for (i = ARRAY_SIZE(mcastd_parts) - 1; i >= 0; i--) {
		if (mcastd_parts[i].up)
			mcastd_parts[i].cleanup_fn();
	}

	DBGMSG("Unloaded mcastd core");
}

static int mcastd_init(void)
{
	int i;

	DBGMSG("Loading mcastd core ...");

	for (i = 0; i < ARRAY_SIZE(mcastd_parts); i++) {
		if (mcastd_parts[i].init_fn())
			break;
		mcastd_parts[i].up++;
	}

	if (i == ARRAY_SIZE(mcastd_parts)) {
		DEBMSG("Loaded mcastd core successfully");
		return 0;
	}

	ERRMSG("Failed to load mcastd core.");
	mcastd_cleanup();
	return -1;
}

module_init(mcastd_init);
module_exit(mcastd_cleanup);

