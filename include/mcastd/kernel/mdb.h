/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __MCASTD_KERNEL_MDB_H__
#define __MCASTD_KERNEL_MDB_H__

#include <linux/types.h>
#include <linux/string.h>
#include <linux/socket.h>
#include <linux/in.h>
#include <linux/in6.h>

#include <mcastd/kernel/compat.h>
#include <mcastd/kernel/kvariant.h>

#define MDB_IFACE_NAMESIZE 32

/**
 * enum mdb_ref - Users of an MDB record.
 *
 * Each MDB record has a field 'refs'. It's a refmask. A refmask gives the IDs
 * of the users who a have a ref on the record (a ref counter doesn't give that
 * info).
 *
 * If the snooper is using a record, it sets bit MDB_REF_SNOOPER_PRIV in the
 * refmask. The snooper typically sets it if it has timers running for that
 * record.
 *
 * mcastd deletes a record if its refmask becomes 0.
 */
enum mdb_ref {
    MDB_REF_AMX,
    MDB_REF_SNOOPER_IN,
    MDB_REF_SNOOPER_EX,
    MDB_REF_SNOOPER_PRIV,
    MDB_REF_PORT,
    MDB_REF_PROXY_CLIENT,
    MDB_REF_MRT,
    MDB_REF_USER,
    MDB_REF_VENDOR_ACCEL,
    MDB_REF_EXTRA_MIN,
    MDB_REF_EXTRA_MAX = 31,
    MDB_REF_COUNT
};

#define MDB_REFMASK_PCB           (1 << MDB_REF_AMX)
#define MDB_REFMASK_SNOOPER_IN    (1 << MDB_REF_SNOOPER_IN)
#define MDB_REFMASK_SNOOPER_EX    (1 << MDB_REF_SNOOPER_EX)
#define MDB_REFMASK_SNOOPER_PRIV  (1 << MDB_REF_SNOOPER_PRIV)
#define MDB_REFMASK_PORT          (1 << MDB_REF_PORT)
#define MDB_REFMASK_PROXY_CLIENT  (1 << MDB_REF_PROXY_CLIENT)
#define MDB_REFMASK_MRT           (1 << MDB_REF_MRT)
#define MDB_REFMASK_USER          (1 << MDB_REF_USER)
#define MDB_REFMASK_VENDOR_ACCEL  (1 << MDB_REF_VENDOR_ACCEL)
#define MDB_REFMASK_EXTRA_MIN     (1 << MDB_REF_EXTRA_MIN)

#define MDB_REFMASK_SNOOPER \
    (MDB_REFMASK_SNOOPER_IN | MDB_REFMASK_SNOOPER_EX)

enum mdb_level {
    MDB_LEVEL_IFACE,
    MDB_LEVEL_GROUP,
    MDB_LEVEL_SOURCE,
    MDB_LEVEL_HOST,
    MDB_LEVEL_COUNT
};

struct mdb_address {
    __u8 family;
    union {
        __u8 data[16];
        __be16 data16[8];
        __be32 data32[4];
    };
};

/*
 * Interface record.
 *
 * - head: to be part mdb_ifaces, i.e. the list of mdb_iface records.
 * - records: children of this record; it's a list of mdb_group records.
 * - parent: not applicable: an mdb_iface does not have a parent.
 * - auxlen: length of auxdata
 */
struct mdb_iface {
    struct list_head head;
    struct list_head records;
    void* parent;
    __u32 refs;
    __u32 change;
    size_t auxlen;
    struct mdb_address address;
    char name[MDB_IFACE_NAMESIZE];
    __u8 family;
    int ifindex;
    int mtu;
    char auxdata[0];
};

/*
 * Group record
 *
 * - head: this mdb_group is part of list of mdb_group records in @a iface,
 *         namely iface->records.
 * - sources: children of this record; it's a list of mdb_source records.
 * - iface: parent
 * - mhead: if this group record is the 1st group record for a certain value of
 *          address, then it's added to the list mdb_groups.
 * - mlead: mdb_group with same value for address. If this mdb_group is
 *          the 1st one for that address, this pointer points to this
 *          mdb_group.
 * - mdups: list of mdb_group records with same value for address.
 * - msources: list of mdb_source records with unique values for address.
 *             The mdb_group with mlead pointing to itself has this list.
 * - bw: bandwidth taken by this group in bit/s according to whitelist
 * - unauthorized: true if subscription is denied because the MC
 *     address is not in the whitelist, but UnauthorizedJoinRequestBehaviour
 *     is true, indicating the HGW should forward the join upstream.
 *
 * The following fields are only applicable if CONFIG_MCASTD_CORE_ACCESS_CONTROL is defined:
 * - bw
 * - unauthorized
 * In the past there was an #ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL / #endif guard around them.
 * But this causes problems when mcastd-core defines CONFIG_MCASTD_CORE_ACCESS_CONTROL and another
 * component which uses 'struct mdb_group' does not (or vice versa).
 */
struct mdb_group {
    struct list_head head;
    struct list_head sources;
    struct mdb_iface* iface;
    __u32 refs;
    __u32 change;
    size_t auxlen;
    struct mdb_address address;
    struct list_head mhead;
    struct mdb_group* mlead;
    struct list_head mdups;
    struct list_head msources;
    __u32 bw;          /* only applicable if CONFIG_MCASTD_CORE_ACCESS_CONTROL is defined */
    bool unauthorized; /* only applicable if CONFIG_MCASTD_CORE_ACCESS_CONTROL is defined */
    char auxdata[0];
};

/*
 * Source record
 *
 * - head: this mdb_source is part of a list of mdb_source records in @a group,
 *         namely group->sources.
 * - hosts: children of this record; it's a list of mdb_host records.
 * - group: parent
 * - mhead: if this source record is the 1st source record for a certain value
 *          of address for mdb_group @a group, then it's added to list
 *          @a group->mlead->msources.
 * - mlead: mdb_source with same value for address and belonging to same group.
 *          If this mdb_source is the 1st one (for this combination of source
 *          address and group address), then this pointer points to this
 *          mdb_source.
 * - mdups : list of mdb_source records with same (source) address belonging to
 *           same group.
 */
struct mdb_source {
    struct list_head head;
    struct list_head hosts;
    struct mdb_group* group;
    __u32 refs;
    __u32 change;
    size_t auxlen;
    struct mdb_address address;
    struct list_head mhead;
    struct mdb_source* mlead;
    struct list_head mdups;
    char auxdata[0];
};

/*
 * Host record
 *
 * - head: this mdb_host is part of a list of mdb_host records in @a source,
 *         namely source->hosts.
 * - source: parent
 * - port : ifindex of the interface to which the host is connected. E.g. if
 *          the IGMP/MLD reports from a host arrive on eth0, this field is
 *          set to the ifindex of eth0. (This number is visible in the output
 *          of 'ip link'.)
 */
struct mdb_host {
    struct list_head head;
    struct list_head padding1;
    struct mdb_source* source;
    __u32 refs;
    __u32 change;
    size_t auxlen;
    struct mdb_address address;
    int port;
    char auxdata[0];
};

struct mdb_event {
    __u32 change[MDB_LEVEL_COUNT];
    struct mdb_group* group;
    struct sk_buff* skb;
};

/*
 * Structure used when a module registers auxiliary data with mdb.
 *
 * See the function mdb_register_aux(..).
 *
 * - level: level at which module registers auxiliary data: interface,
 *          group, source or host
 * - ref  : identifies module registering the auxiliary data
 * - name : human-readable name for @a ref
 * - size : nr of bytes the module @a ref registers at @a level
 * - data2var: function to put auxiliary data @a data in buffer @a buf.
 */
struct mdb_auxinfo {
    enum mdb_level level;
    enum mdb_ref ref;
    const char* name;
    size_t size;
    int (* data2var)(struct kvarbuf* buf, struct kvar* var, void* data);
};

enum mdb_priority {
    MDB_PRIORITY_CRIT, /* subscribers on the critical path */
    MDB_PRIORITY_HIGH, /* for subscribers that modify data inside
                          handler on which other subscribers depend */
    MDB_PRIORITY_MED,  /* subscribers for which performance is nice to
                          have */
    MDB_PRIORITY_LOW,  /* don't care subscribers, e.g. queueing to
                          userspace */
    MDB_PRIORITY_LAST, /* debugging and subscribers that want to be
                          the last in the row intentionally */
    MDB_PRIORITY_COUNT
};

typedef void (* mdb_event_cb)(struct mdb_event* e);

extern struct list_head mdb_ifaces;
extern struct list_head mdb_groups;

extern struct mutex mdb_mutex;
extern spinlock_t mdb_record_spinlock;

static inline void mdb_setref(__u32* refs, __u32* change, enum mdb_ref ref) {
    if(*refs & 1 << ref) {
        return;
    }
    *refs |= 1 << ref;
    *change ^= 1 << ref;
}

static inline void mdb_clrref(__u32* refs, __u32* change, enum mdb_ref ref) {
    if(!(*refs & 1 << ref)) {
        return;
    }
    *refs &= ~(1 << ref);
    *change ^= 1 << ref;
}

static inline void mdb_iface_setref(struct mdb_iface* iface, enum mdb_ref ref) {
    mdb_setref(&iface->refs, &iface->change, ref);
}

static inline void mdb_iface_clrref(struct mdb_iface* iface, enum mdb_ref ref) {
    mdb_clrref(&iface->refs, &iface->change, ref);
}

static inline void mdb_group_setref(struct mdb_group* group, enum mdb_ref ref) {
    mdb_setref(&group->refs, &group->change, ref);
}

static inline void mdb_group_clrref(struct mdb_group* group, enum mdb_ref ref) {
    mdb_clrref(&group->refs, &group->change, ref);
}

static inline void mdb_source_setref(struct mdb_source* source, enum mdb_ref ref) {
    mdb_setref(&source->refs, &source->change, ref);
}

static inline void mdb_source_clrref(struct mdb_source* source, enum mdb_ref ref) {
    mdb_clrref(&source->refs, &source->change, ref);
}

static inline void mdb_host_setref(struct mdb_host* host, enum mdb_ref ref) {
    mdb_setref(&host->refs, &host->change, ref);
}

static inline void mdb_host_clrref(struct mdb_host* host, enum mdb_ref ref) {
    mdb_clrref(&host->refs, &host->change, ref);
}

static inline size_t mdb_address_size(const struct mdb_address* address) {
    switch(address->family) {
    case AF_INET:  return 4;
    case AF_INET6: return 16;
    case AF_UNIX:  return sizeof(struct socket*);
    default:
        return 0;
    }
}

static inline bool mdb_address_equals(const struct mdb_address* address1,
                                      const struct mdb_address* address2) {
    const __be32* a1 = address1->data32, * a2 = address2->data32;
    if(address1->family != address2->family) {
        return false;
    }
    switch(address1->family) {
    case AF_UNSPEC: return true;
    case AF_INET:   return a1[0] == a2[0];
    case AF_INET6:  return a1[0] == a2[0] && a1[1] == a2[1] &&
               a1[2] == a2[2] && a1[3] == a2[3];
    case AF_UNIX: {
        struct socket* s1, * s2;
        memcpy(&s1, a1, sizeof(struct socket*));
        memcpy(&s2, a2, sizeof(struct socket*));
        return s1 == s2;
    }
    default:
        return false;
    }
}

static inline bool mdb_address_any(struct mdb_address* ma) {
    __be32* a = ma->data32;
    switch(ma->family) {
    case AF_UNSPEC: return true;
    case AF_INET:   return a[0] ? false : true;
    case AF_INET6:  return (a[0] | a[1] | a[2] | a[3]) ? false : true;
    case AF_UNIX:
    default:
        return false;
    }
}

static inline int mdb_address_to_sockaddr(struct mdb_address* ma,
                                          struct sockaddr_storage* ss) {
    if(!ss || !ma) {
        return -EINVAL;
    }

    ss->ss_family = ma->family;

    switch(ss->ss_family) {
    case AF_INET: {
        struct sockaddr_in* in_addr = (struct sockaddr_in*) ss;
        in_addr->sin_addr.s_addr = *((unsigned long*) ma->data);
        break;
    }
    case AF_INET6: {
        struct sockaddr_in6* in6_addr = (struct sockaddr_in6*) ss;
        __u32* a = in6_addr->sin6_addr.s6_addr32;
        a[0] = ma->data32[0];
        a[1] = ma->data32[1];
        a[2] = ma->data32[2];
        a[3] = ma->data32[3];
        break;
    }
    case AF_UNIX:
    default:
        return -EINVAL;
    }

    return 0;
}

const char* mdb_address_to_string(struct mdb_address* addr);
const char* family_to_string(sa_family_t family);

void mdb_lock(void);
void mdb_unlock(void);

struct mdb_iface* mdb_iface_create(const char* name);
struct mdb_group* mdb_group_create(struct mdb_iface* iface,
                                   const struct mdb_address* address);
struct mdb_source* mdb_source_create(struct mdb_group* group,
                                     const struct mdb_address* address);
struct mdb_host* mdb_host_create(struct mdb_source* source,
                                 const struct mdb_address* address);
struct mdb_source* mdb_group_create_any_source(struct mdb_group* group);

struct mdb_iface* mdb_iface_find(const char* name);
struct mdb_group* mdb_group_find(struct mdb_iface* iface,
                                 const struct mdb_address* address);
struct mdb_source* mdb_source_find(struct mdb_group* group,
                                   const struct mdb_address* address);
struct mdb_host* mdb_host_find(struct mdb_source* source,
                               const struct mdb_address* address);
struct mdb_group* mdb_group_mfind(const struct mdb_address* address);
struct mdb_source* mdb_source_mfind(struct mdb_group* group,
                                    const struct mdb_address* address);

static inline
struct mdb_source* mdb_group_get_any_source(struct mdb_group* group) {
    struct mdb_source* source;
    source = list_first_entry(&group->sources, struct mdb_source, head);
    if(&source->head == &group->sources) {
        return NULL;
    }
    if(mdb_address_any(&source->address)) {
        return source;
    }
    return NULL;
}

void mdb_iface_release(struct mdb_iface* iface);

void mdb_commit(struct mdb_group* group, struct sk_buff* skb);

void mdb_subscribe(enum mdb_priority, mdb_event_cb cb);
void mdb_unsubscribe(enum mdb_priority, mdb_event_cb cb);

void mdb_register_aux(struct mdb_auxinfo* info);
void mdb_unregister_aux(struct mdb_auxinfo* info);

size_t mdb_aux_offset(enum mdb_level level, enum mdb_ref ref);
int mdb_aux2var(struct kvarbuf* buf, struct kvar* var,
                enum mdb_level level, enum mdb_ref ref, void* data);
const char* mdb_aux_name(enum mdb_level level, enum mdb_ref ref);

/*
 * Return pointer to (start of) auxiliary data of module @a ref in @a iface
 * record.
 */
static inline
void* mdb_iface_auxdata(struct mdb_iface* iface, enum mdb_ref ref) {
    size_t offset = mdb_aux_offset(MDB_LEVEL_IFACE, ref);
    if(!offset || (offset >= sizeof(struct mdb_iface) + iface->auxlen)) {
        return NULL;
    }
    return (void*) iface + offset;
}

/*
 * Return pointer to (start of) auxiliary data of module @a ref in @a group
 * record.
 */
static inline
void* mdb_group_auxdata(struct mdb_group* group, enum mdb_ref ref) {
    size_t offset = mdb_aux_offset(MDB_LEVEL_GROUP, ref);
    if(!offset || (offset >= sizeof(struct mdb_group) + group->auxlen)) {
        return NULL;
    }
    return (void*) group + offset;
}

/*
 * Return pointer to (start of) auxiliary data of module @a ref in @a source
 * record.
 */
static inline
void* mdb_source_auxdata(struct mdb_source* source, enum mdb_ref ref) {
    size_t offset = mdb_aux_offset(MDB_LEVEL_SOURCE, ref);
    if(!offset || (offset >= sizeof(struct mdb_source) + source->auxlen)) {
        return NULL;
    }
    return (void*) source + offset;
}

/*
 * Return pointer to (start of) auxiliary data of module @a ref in @a host
 * record.
 */
static inline
void* mdb_host_auxdata(struct mdb_host* host, enum mdb_ref ref) {
    size_t offset = mdb_aux_offset(MDB_LEVEL_HOST, ref);
    if(!offset || (offset >= sizeof(struct mdb_host) + host->auxlen)) {
        return NULL;
    }
    return (void*) host + offset;
}

#endif

