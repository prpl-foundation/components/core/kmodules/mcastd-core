/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <net/netlink.h>
#include <linux/module.h>
#include <linux/netlink.h>
#include <linux/mutex.h>
#include <linux/version.h>
#include <linux/socket.h>
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,24))
#include <net/net_namespace.h>
#endif
#include <net/sock.h>

#include <mcastd/kernel/netlink.h>

#define IDENT "core"
#include <mcastd/kernel/debug.h>

static DEFINE_MUTEX(mcastdnl_mutex);

static struct sock *mcastdnl_sock = NULL;

static struct {
	mcastdnl_input_cb input;
} mcastdnl_table[MCASTDNL_TYPE_COUNT - NLMSG_MIN_TYPE];

void mcastdnl_lock(void)
{
	mutex_lock(&mcastdnl_mutex);
}

void mcastdnl_unlock(void)
{
	mutex_unlock(&mcastdnl_mutex);
}

/**
 * mcastdnl_make() - create skb and put netlink message in it
 * @type: netlink message type. E.g. MCASTDNL_TYPE_NEWOBJ.
 * @pid: netlink port ID. 0 for a MC msg. When called to create a response
 *       for a netlink msg from US, use the pid from that latter msg.
 * @seq: sequence number. 0 for a MC msg. When called to create a response
 *       for a netlink msg from US, use the seqnr from that latter msg.
 * @payload: length of message payload
 * @nlh: netlink message upon success
 * @skb: socket buffer to store message in upon success
 *
 * Return:
 * * 0 on success
 * * negative value upon error
 */
int mcastdnl_make(__u16 type, __u32 pid, __u32 seq, size_t payload,
                  struct nlmsghdr **nlh, struct sk_buff **skb)
{
	*nlh = NULL;
	*skb = nlmsg_new(payload, GFP_KERNEL);
	if (*skb == NULL) {
		ERRMSG("Out of memory");
		return -ENOMEM;
	}
	*nlh = nlmsg_put(*skb, pid, seq, type, payload, 0);
	if (*nlh == NULL) {
		ERRMSG("Out of memory");
		nlmsg_free(*skb);
		*skb = NULL;
		return -ENOBUFS;
	}
	memset(NLMSG_DATA(*nlh), 0xbb, payload);
	nlmsg_end(*skb, *nlh);
	return 0;
}

#if (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,24))
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(4,12,0))
static int mcastdnl_rcv(struct sk_buff *skb, struct nlmsghdr *nlh, struct netlink_ext_ack *extack)
{
	(void)extack;
#else
static int mcastdnl_rcv(struct sk_buff *skb, struct nlmsghdr *nlh)
{
#endif
	(void)skb;
	if (unlikely(nlh->nlmsg_type < NLMSG_MIN_TYPE ||
			nlh->nlmsg_type >= MCASTDNL_TYPE_COUNT))
		return -ENOTSUPP;
	if (unlikely(!mcastdnl_table[nlh->nlmsg_type - NLMSG_MIN_TYPE].input))
		return -ENOTSUPP;
	return mcastdnl_table[nlh->nlmsg_type - NLMSG_MIN_TYPE].input(nlh);
}
#else
static int mcastdnl_rcv(struct sk_buff *skb, struct nlmsghdr *nlh, int *errp)
{
	int ret;
	(void)skb;

	if (unlikely(nlh->nlmsg_type < NLMSG_MIN_TYPE ||
			nlh->nlmsg_type >= MCASTDNL_TYPE_COUNT)) {
		*errp = -ENOTSUPP;
		return -1;
	}
	if (unlikely(!mcastdnl_table[nlh->nlmsg_type - NLMSG_MIN_TYPE].input)) {
		*errp = -ENOTSUPP;
		return -1;
	}
	if ((ret = mcastdnl_table[nlh->nlmsg_type - NLMSG_MIN_TYPE].input(nlh))
			!= 0) {
		*errp = ret;
		return -1;
	}
	return 0;
}
#endif

#if (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,24))
static void mcastdnl_input(struct sk_buff *skb)
{
	mcastdnl_lock();
	netlink_rcv_skb(skb, mcastdnl_rcv);
	mcastdnl_unlock();
}
#else
static void mcastdnl_input(struct sock *sk, int len)
{
	unsigned int qlen = 0;
	do {
		mcastdnl_lock();
		netlink_run_queue(sk, &qlen, &mcastdnl_rcv);
		mcastdnl_unlock();
	} while (qlen);
}
#endif

int mcastdnl_register(int msgtype, mcastdnl_input_cb input)
{
	if (msgtype < NLMSG_MIN_TYPE || msgtype >= MCASTDNL_TYPE_COUNT)
		return -EINVAL;
	mcastdnl_lock();
	mcastdnl_table[msgtype - NLMSG_MIN_TYPE].input = input;
	mcastdnl_unlock();
	return 0;
}

void mcastdnl_unregister(int msgtype)
{
	if (msgtype < NLMSG_MIN_TYPE || msgtype >= MCASTDNL_TYPE_COUNT)
		return;
	mcastdnl_lock();
	mcastdnl_table[msgtype - NLMSG_MIN_TYPE].input = NULL;
	mcastdnl_unlock();
}

int mcastdnl_unicast(struct sk_buff *skb, struct nlmsghdr *nlh, u32 payload,
                     u32 pid)
{
	int err = -EINVAL;
	if (!skb || !nlh)
		return err;
	nlh->nlmsg_len = NLMSG_LENGTH(payload);
	err = nlmsg_unicast(mcastdnl_sock, skb, pid);
	if (err)
		ERRMSG("nlmsg_unicast(pid=%u type=%u) returned %d.",
		        pid, nlh->nlmsg_type, err);
	return err;
}

int mcastdnl_multicast(struct sk_buff *skb, struct nlmsghdr *nlh, u32 payload,
                       u32 pid, unsigned int group, gfp_t flags)
{
	int err = -EINVAL;
	if (!skb || !nlh)
		return err;
	nlh->nlmsg_len = NLMSG_LENGTH(payload);
	err = nlmsg_multicast(mcastdnl_sock, skb, pid, group, flags);
	if (err && err != -ESRCH)
		ERRMSG("nlmsg_multicast(pid=%u group=%u type=%u) returned %d.",
		       pid, group, nlh->nlmsg_type, err);
	return err;
}

void mcastd_netlink_cleanup(void)
{
	
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,25))
	netlink_kernel_release(mcastdnl_sock);
#else
	sock_release(mcastdnl_sock->sk_socket);
#endif
}

int mcastd_netlink_init(void)
{
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(3,7,1))
	struct netlink_kernel_cfg cfg;
	memset(&cfg, 0, sizeof(cfg));
	cfg.groups = MCASTDNL_GROUP_COUNT;
	cfg.input = mcastdnl_input;
	mcastdnl_sock = netlink_kernel_create(&init_net, NETLINK_MCASTD, &cfg);
#elif (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,24))
	mcastdnl_sock = netlink_kernel_create(&init_net,
	                                      NETLINK_MCASTD, MCASTDNL_GROUP_COUNT,
	                                      mcastdnl_input, NULL, THIS_MODULE);
#else
	mcastdnl_sock = netlink_kernel_create(NETLINK_MCASTD, MCASTDNL_GROUP_COUNT,
	                                      mcastdnl_input, THIS_MODULE);
#endif

	if (!mcastdnl_sock) {
		ERRMSG("netlink_kernel_create(unit=%d, groups=%d) failed.",
		       NETLINK_MCASTD, MCASTDNL_GROUP_COUNT);
		return -1;
	}
	return 0;
}

EXPORT_SYMBOL(mcastdnl_lock);
EXPORT_SYMBOL(mcastdnl_unlock);
EXPORT_SYMBOL(mcastdnl_make);
EXPORT_SYMBOL(mcastdnl_register);
EXPORT_SYMBOL(mcastdnl_unregister);
EXPORT_SYMBOL(mcastdnl_unicast);
EXPORT_SYMBOL(mcastdnl_multicast);

