/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <linux/module.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/socket.h>
#include <linux/netlink.h>
#include <linux/string.h>

#include <mcastd/kernel/kvariant.h>

#define IDENT "core"
#include <mcastd/kernel/debug.h>

int kvar_validate(struct kvar *var)
{
	int i, j, key, val;
	struct kvar *v;
	switch (var->hdr.type) {
	case KVARTYPE_BOOL:
		if (unlikely(var->hdr.len < sizeof(int)))
			goto NOK;
		break;
	case KVARTYPE_INT:
		if (unlikely(var->hdr.len < sizeof(__s32)))
			goto NOK;
		break;
	case KVARTYPE_UINT:
		if (unlikely(var->hdr.len < sizeof(__u32)))
			goto NOK;
		break;
	case KVARTYPE_STRING:
		if (unlikely(var->hdr.len < var->hdr.cnt + 1))
			goto NOK;
		if (unlikely(var->val.s[var->hdr.cnt] != '\0'))
			goto NOK;
		break;
	case KVARTYPE_LIST:
		if (unlikely(var->hdr.len <
				var->hdr.cnt * sizeof(struct kvarlist)))
			goto NOK;
		for (i = 0; i < var->hdr.cnt; i++) {
			if (unlikely(!var->val.l[i].val))
				continue;
			val = var->val.l[i].val;
			if (unlikely(val + sizeof(struct kvarhdr) >
					var->hdr.len))
				goto NOK;
			v = (struct kvar *)((void *)&var->val + val);
			if (unlikely(val + sizeof(struct kvarhdr) + v->hdr.len >
					var->hdr.len))
				goto NOK;
			if (unlikely(kvar_validate(v)))
				return -EINVAL;
		}
		break;
	case KVARTYPE_MAP:
		if (unlikely(var->hdr.len <
				var->hdr.cnt * sizeof(struct kvarmap)))
			goto NOK;
		for (i = 0; i < var->hdr.cnt; i++) {
			if (unlikely(!var->val.m[i].key && !var->val.m[i].val))
				continue;
			if (unlikely(!var->val.m[i].key || !var->val.m[i].val))
				goto NOK;
			key = var->val.m[i].key;
			val = var->val.m[i].val;
			for (j = key; j < val; j++)
				if (!*((char *)&var->val + j))
					break;
			if (unlikely(j == val))
				goto NOK;
			if (unlikely(val + sizeof(struct kvarhdr) >
					var->hdr.len))
				goto NOK;
			v = (struct kvar *)((void *)&var->val + val);
			if (unlikely(val + sizeof(struct kvarhdr) + v->hdr.len >
					var->hdr.len))
				goto NOK;
			if (unlikely(kvar_validate(v)))
				return -EINVAL;
		}
		break;
	case KVARTYPE_ADDR:
		if (unlikely(var->hdr.len < var->hdr.cnt + 1))
			goto NOK;
		switch (var->val.a.fam) {
		case AF_INET:
			if (unlikely(var->hdr.cnt != 4))
				goto NOK;
			break;
		case AF_INET6:
			if (unlikely(var->hdr.cnt != 16))
				goto NOK;
			break;
		default:
			goto NOK;
		}
		break;
	default:
		goto NOK;
	}
	return 0;
NOK:
	DEBMSG("kvar_validate() fail");
	return -EINVAL;
}

int kvar_bool_val(struct kvar *var)
{
	if (unlikely(!var || var->hdr.type != KVARTYPE_BOOL))
		return 0;
	return var->val.b;
}

__s32 kvar_int_val(struct kvar *var)
{
	if (unlikely(!var))
		return 0;
	/* Accidentally sending an uint instead of int is a likely error during
	 * development. Therefore log an error if the type is not as expected.
	 */
	if (unlikely(var->hdr.type != KVARTYPE_INT)) {
		ERRMSG("Value in kvar is of type %d instead of int", var->hdr.type);
		return 0;
	}
	return var->val.i;
}

__u32 kvar_uint_val(struct kvar *var)
{
	if (unlikely(!var))
	   return 0;
	/* Accidentally sending an int instead of uint is a likely error during
	 * development. Therefore log an error if the type is not as expected.
	 */
	if (unlikely(var->hdr.type != KVARTYPE_UINT)) {
		ERRMSG("Value in kvar is of type %d instead of uint", var->hdr.type);
		return 0;
	}
	return var->val.u;
}

char *kvar_string_val(struct kvar *var)
{
	if (unlikely(!var ||var->hdr.type != KVARTYPE_STRING))
		return NULL;
	return var->val.s;
}

int kvar_list_count(struct kvar *var) {
	if (unlikely(!var || var->hdr.type != KVARTYPE_LIST))
		return 0;
	return var->hdr.cnt;
}

struct kvar *kvar_list_val(struct kvar *var, __u16 index)
{
	if (unlikely(!var || var->hdr.type != KVARTYPE_LIST))
		return NULL;
	if (unlikely(index >= var->hdr.cnt || !var->val.l[index].val))
		return NULL;
	return (struct kvar *)((void *)&var->val + var->val.l[index].val);
}

char *kvar_map_key(struct kvar *var, __u16 index)
{
	if (unlikely(!var || var->hdr.type != KVARTYPE_MAP))
		return NULL;
	if (unlikely(index >= var->hdr.cnt || !var->val.m[index].key))
		return NULL;
	return (char *)&var->val + var->val.m[index].key;
}

int kvar_map_count(struct kvar *var) {
	if (unlikely(!var || var->hdr.type != KVARTYPE_MAP))
		return 0;
	return var->hdr.cnt;
}

struct kvar *kvar_map_val(struct kvar *var, __u16 index)
{
	if (unlikely(!var || var->hdr.type != KVARTYPE_MAP))
		return NULL;
	if (unlikely(index >= var->hdr.cnt || !var->val.m[index].val))
		return NULL;
	return (struct kvar *)((void *)&var->val + var->val.m[index].val);
}

struct kvar *kvar_map_find(struct kvar *var, const char *key)
{
	__u16 i;
	if (unlikely(!var || var->hdr.type != KVARTYPE_MAP))
		return NULL;
	for (i = 0; i < var->hdr.cnt; i++) {
		if (!var->val.m[i].key)
			continue;
		if (!strcmp((char *)&var->val + var->val.m[i].key, key))
			break;
	}
	if (i == var->hdr.cnt || !var->val.m[i].val)
		return NULL;
	return (struct kvar *)((void *)&var->val + var->val.m[i].val);
}

__u8 kvar_addr_fam(struct kvar *var)
{
	if (unlikely(!var || var->hdr.type != KVARTYPE_ADDR))
		return AF_UNSPEC;
	return var->val.a.fam;
}

__u8 *kvar_addr_val(struct kvar *var)
{
	if (unlikely(!var || var->hdr.type != KVARTYPE_ADDR))
		return NULL;
	return var->val.a.val;
}

int kvar_void_set(struct kvarbuf *buf, struct kvar *var)
{
	(void)buf;
	var->hdr.type = KVARTYPE_VOID;
	var->hdr.len = 0;
	var->hdr.cnt = 0;
	return 0;
}

int kvar_bool_set(struct kvarbuf *buf, struct kvar *var, int b)
{
	__u16 pos, len;
	if (unlikely(!var))
		return -EINVAL;
	pos = (unsigned char *)&var->val - buf->data.raw;
	len = sizeof(int);
	if (unlikely(pos + len + 1 > buf->size))
		return -ENOBUFS;
	var->hdr.type = KVARTYPE_BOOL;
	var->hdr.len = len;
	var->val.b = b;
	return 0;
}

int kvar_int_set(struct kvarbuf *buf, struct kvar *var, __s32 i)
{
	__u16 pos, len;
	if (unlikely(!var))
		return -EINVAL;
	pos = (unsigned char *)&var->val - buf->data.raw;
	len = sizeof(__s32);
	if (unlikely(pos + len + 1 > buf->size))
		return -ENOBUFS;
	var->hdr.type = KVARTYPE_INT;
	var->hdr.len = len;
	var->val.i = i;
	return 0;
}

int kvar_uint_set(struct kvarbuf *buf, struct kvar *var, __u32 u)
{
	__u16 pos, len;
	if (unlikely(!var))
		return -EINVAL;
	pos = (unsigned char *)&var->val - buf->data.raw;
	len = sizeof(__u32);
	if (unlikely(pos + len + 1 > buf->size))
		return -ENOBUFS;
	var->hdr.type = KVARTYPE_UINT;
	var->hdr.len = len;
	var->val.u = u;
	return 0;
}

int kvar_string_set(struct kvarbuf *buf, struct kvar *var, const char *s)
{
	__u16 pos, len;
	if (unlikely(!var))
		return -EINVAL;
	pos = (unsigned char *)&var->val - buf->data.raw;
	len = strlen(s?s:"") + 1;
	if (unlikely(pos + len > buf->size))
		return -ENOBUFS;
	var->hdr.type = KVARTYPE_STRING;
	var->hdr.len = len;
	var->hdr.cnt = len - 1;
	strcpy(var->val.s, s?s:"");
	return 0;
}

int kvar_list_init(struct kvarbuf *buf, struct kvar *var, __u16 cnt)
{
	__u16 pos, len;
	if (unlikely(!var))
		return -EINVAL;
	pos = (unsigned char *)&var->val - buf->data.raw;
	len = cnt * sizeof(struct kvarlist);
	if (unlikely(pos + len > buf->size))
		return -ENOBUFS;
	var->hdr.type = KVARTYPE_LIST;
	var->hdr.len = len;
	var->hdr.cnt = cnt;
	memset(&var->val.l, 0, len);
	return 0;
}

int kvar_list_edit(struct kvarbuf *buf, struct kvar *var, __u16 index,
                   struct kvar **val)
{
	__u16 ent, pos, len;
	if (val)
		*val = NULL;
	if (unlikely(!var))
		return -EINVAL;
	ent = NLMSG_ALIGN(var->hdr.len);
	pos = (unsigned char *)&var->val - buf->data.raw;
	len = sizeof(struct kvarhdr);
	if (unlikely(pos + ent + len > buf->size))
		return -ENOBUFS;
	var->val.l[index].val = ent;
	memset((void *)&var->val + ent, 0, sizeof(struct kvarhdr));
	var->hdr.len = NLMSG_ALIGN(var->hdr.len);
	var->hdr.len += NLMSG_ALIGN(sizeof(struct kvarhdr));
	if (val)
		*val = (struct kvar *)((void *)&var->val + ent);
	return 0;
}

int kvar_list_done(struct kvarbuf *buf, struct kvar *var, __u16 index)
{
	struct kvar *val;
	(void)buf;
	if (unlikely(!var))
		return -EINVAL;
	val = kvar_list_val(var, index);
	if (unlikely(!val))
		return -EINVAL;
	var->hdr.len += val->hdr.len;
	return 0;
}

int kvar_map_init(struct kvarbuf *buf, struct kvar *var, __u16 cnt)
{
	__u16 pos, len;
	if (unlikely(!var))
		return -EINVAL;
	pos = (unsigned char *)&var->val - buf->data.raw;
	len = cnt * sizeof(struct kvarmap);
	if (pos + len > buf->size)
		return -ENOBUFS;
	var->hdr.type = KVARTYPE_MAP;
	var->hdr.len = len;
	var->hdr.cnt = cnt;
	memset(&var->val.m, 0, len);
	return 0;
}

int kvar_map_edit(struct kvarbuf *buf, struct kvar *var, __u16 index,
                  const char *key, struct kvar **val)
{
	__u16 ent, pos, keylen, len;
	if (val)
		*val = NULL;
	if (unlikely(!var))
		return -EINVAL;
	ent = NLMSG_ALIGN(var->hdr.len);
	pos = (unsigned char *)&var->val - buf->data.raw;
	keylen = NLMSG_ALIGN(strlen(key) + 1);
	len = keylen + sizeof(struct kvarhdr);
	if (unlikely(pos + ent + len > buf->size))
		return -ENOBUFS;
	var->val.m[index].key = ent;
	strcpy((char *)&var->val + ent, key);
	var->val.m[index].val = ent + keylen;
	memset((void *)&var->val + ent + keylen, 0, sizeof(struct kvarhdr));
	var->hdr.len = NLMSG_ALIGN(var->hdr.len);
	var->hdr.len += keylen + NLMSG_ALIGN(sizeof(struct kvarhdr));
	if (val)
		*val = (struct kvar *)((void *)&var->val + ent + keylen);
	return 0;
}

int kvar_map_done(struct kvarbuf *buf, struct kvar *var, __u16 index)
{
	struct kvar *val;
	(void)buf;
	if (unlikely(!var))
		return -EINVAL;
	val = kvar_map_val(var, index);
	if (unlikely(!val))
		return -EINVAL;
	var->hdr.len += val->hdr.len;
	return 0;
}

int kvar_addr_set(struct kvarbuf *buf, struct kvar *var, __u8 fam, __u8 *val)
{
	__u16 pos, len;
	if (unlikely(!var || !val))
		return -EINVAL;
	pos = (unsigned char *)&var->val - buf->data.raw;
	switch (fam) {
	case AF_INET:  len = 5; break;
	case AF_INET6: len = 17; break;
	default:
		return -EINVAL;
	}
	if (unlikely(pos + len > buf->size))
		return -ENOBUFS;
	var->hdr.type = KVARTYPE_ADDR;
	var->hdr.len = len;
	var->hdr.cnt = len - 1;
	var->val.a.fam = fam;
	memcpy(var->val.a.val, val, len - 1);
	return 0;
}

EXPORT_SYMBOL(kvar_bool_val);
EXPORT_SYMBOL(kvar_int_val);
EXPORT_SYMBOL(kvar_uint_val);
EXPORT_SYMBOL(kvar_string_val);
EXPORT_SYMBOL(kvar_list_count);
EXPORT_SYMBOL(kvar_list_val);
EXPORT_SYMBOL(kvar_map_count);
EXPORT_SYMBOL(kvar_map_key);
EXPORT_SYMBOL(kvar_map_val);
EXPORT_SYMBOL(kvar_map_find);
EXPORT_SYMBOL(kvar_addr_fam);
EXPORT_SYMBOL(kvar_addr_val);
EXPORT_SYMBOL(kvar_void_set);
EXPORT_SYMBOL(kvar_bool_set);
EXPORT_SYMBOL(kvar_int_set);
EXPORT_SYMBOL(kvar_uint_set);
EXPORT_SYMBOL(kvar_string_set);
EXPORT_SYMBOL(kvar_list_init);
EXPORT_SYMBOL(kvar_list_edit);
EXPORT_SYMBOL(kvar_list_done);
EXPORT_SYMBOL(kvar_map_init);
EXPORT_SYMBOL(kvar_map_edit);
EXPORT_SYMBOL(kvar_map_done);
EXPORT_SYMBOL(kvar_addr_set);

