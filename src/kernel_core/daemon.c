/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <linux/module.h>
#include <linux/list.h>
#include <linux/kthread.h>
#include <linux/slab.h>
#include <linux/jiffies.h>

#include <mcastd/kernel/daemon.h>
#include <mcastd/kernel/compat.h>

#define IDENT "core"
#include <mcastd/kernel/debug.h>

struct mcastd_worklist {
	struct list_head head;
	struct mcastd_work work;
};

static LIST_HEAD(mcastd_workqueue);
static DEFINE_SPINLOCK(mcastd_worklock);
static struct task_struct *mcastd_thread = NULL;
static atomic_t mcastd_dead = ATOMIC_INIT(0);
static LIST_HEAD(mcastd_timers);
static DEFINE_SPINLOCK(mcastd_timerlock);

void mcastd_work_schedule(struct mcastd_work *work, gfp_t gfp)
{
	struct mcastd_worklist *list;
	unsigned long irqflags;
	if (atomic_read(&mcastd_dead)) {
		ERRMSG("Cannot schedule task (mcastd dead).");
		if (work->destroy)
			work->destroy(work->data);
		return;
	}
	list = kmalloc(sizeof(struct mcastd_worklist), gfp);
	if (!list) {
		ERRMSG("Cannot schedule task (Out of memory).");
		if (work->destroy)
			work->destroy(work->data);
		return;
	}
	list->work.run = work->run;
	list->work.destroy = work->destroy;
	list->work.data = work->data;
	spin_lock_irqsave(&mcastd_worklock, irqflags);
	list_add_tail(&list->head, &mcastd_workqueue);
	spin_unlock_irqrestore(&mcastd_worklock, irqflags);
	wake_up_process(mcastd_thread);
}

static int mcastd_work_pull(struct mcastd_worklist **work)
{
	int ret = 0;
	unsigned long irqflags;
	spin_lock_irqsave(&mcastd_worklock, irqflags);
	if (!list_empty(&mcastd_workqueue)) {
		*work = list_first_entry(&mcastd_workqueue, struct mcastd_worklist,
		                         head);
		list_del(&(*work)->head);
		ret = 1;
	}
	spin_unlock_irqrestore(&mcastd_worklock, irqflags);
	return ret;
}

static inline signed long mcastd_expcmp(unsigned long exp1, unsigned long exp2)
{
	return exp1 - exp2;
}

/**
 * mcastd_timer_pull() - Pull timer from mcastd_timers if it's expired or canceled
 * @timer: function assigns timer pulled from mcastd_timers to this param if
 *         the 1st time from mcastd_timers is expired ore canceled.
 * @now: the current time. Function uses it to check if the 1st timer from
 *       mcastd_timers is expired or not.
 * @force: if 1, the function pulls the 1st timer from mcastd_timers even if
 *         it's not expired, and declares the timer canceled. KCMD sets this
 *         param to 1 to clean up all timers upon stopping.
 *
 * Return:
 * * 1 if the function pulls a timer from mcastd_timers and assigns it to 'timer'.
 *   'timer' is in state EXPIRED or CANCELED.
 * * 0 if the function didn't pull a timer from mcastd_timers
 */
static int mcastd_timer_pull(struct mcastd_timer **timer, unsigned long now,
                             int force)
{
	struct mcastd_timer *t = NULL;
	int ret = 0;
	unsigned long irqflags;
	spin_lock_irqsave(&mcastd_timerlock, irqflags);
	if (!list_empty(&mcastd_timers)) {
		t = list_first_entry(&mcastd_timers, struct mcastd_timer, head);
		if (force)
			t->state = MCASTD_TIMER_CANCELED;
		if (t->state == MCASTD_TIMER_RUNNING &&
				mcastd_expcmp(t->expires, now) <= 0)
			t->state = MCASTD_TIMER_EXPIRED;
		if (t->state == MCASTD_TIMER_CANCELED ||
				t->state == MCASTD_TIMER_EXPIRED) {
			__list_del(t->head.prev, t->head.next);
			t->head.next = t->head.prev = NULL;
			*timer = t;
			ret = 1;
		}
	}
	spin_unlock_irqrestore(&mcastd_timerlock, irqflags);
	return ret;
}

/**
 * mcastd_timeout() - Return period until next mcastd timer times out.
 *
 * Return:
 * * period in jiffies until next mcastd timer times out if there is such a timer
 * * MAX_SCHEDULE_TIMEOUT if no mcastd_timers are present
 */
static signed long mcastd_timeout(void)
{
	struct mcastd_timer *timer = NULL;
	signed long ret = MAX_SCHEDULE_TIMEOUT;
	unsigned long irqflags;
	spin_lock_irqsave(&mcastd_timerlock, irqflags);
	if (!list_empty(&mcastd_timers)) {
		timer = list_first_entry(&mcastd_timers, struct mcastd_timer, head);
		ret = timer->expires - jiffies;
	}
	spin_unlock_irqrestore(&mcastd_timerlock, irqflags);
	return ret;
}

static void mcastd_timer_idle(struct mcastd_timer *timer)
{
	unsigned long irqflags;
	spin_lock_irqsave(&mcastd_timerlock, irqflags);
	timer->state = MCASTD_TIMER_IDLE;
	spin_unlock_irqrestore(&mcastd_timerlock, irqflags);
}

/**
 * mcastd_run() - Handle jobs and timers
 *
 * The function:
 * - pulls jobs (from mcastd_workqueue) as long as there are jobs. It calls run()
 *   and destroy() for each of them.
 * - pulls timers (from mcastd_timers) as long as there are expired or canceled
 *   timers. It calls the callback function of the timer if it's expired. That
 *   callback function might reschedule the timer, which sets the timer to
 *   RUNNING. mcastd_run() then calls the destroy function of the timer if the
 *   timer is expired or canceled.
 *
 * If there are no expired mcastd_timers anymore, it puts the mcastd_thread to sleep until
 * the 1st timer in mcastd_timers times out, or it puts the mcastd_thread in an endless
 * sleep (until another function wakes it up) if there is no such timer.
 *
 * If mcastd is stopped, it removes all jobs and mcastd_timers, and it calls their
 * destroy function.
 */
static int mcastd_run(void *data)
{
	struct mcastd_worklist *work;
	struct mcastd_timer *timer;
	unsigned long now;
	enum mcastd_timer_state timer_state;
	struct mutex *timer_lock;
	while (!kthread_should_stop()) {
		if (mcastd_work_pull(&work)) {
			__set_current_state(TASK_RUNNING);
			if (work->work.run)
				work->work.run(work->work.data);
			if (work->work.destroy)
				work->work.destroy(work->work.data);
			kfree(work);
			continue;
		}
		now = jiffies;
		if (mcastd_timer_pull(&timer, now, 0)) {
			__set_current_state(TASK_RUNNING);
			timer_lock = timer->lock;
			if (timer_lock)
				mutex_lock(timer_lock);
			timer_state = mcastd_timer_state(timer);
			if (timer_state == MCASTD_TIMER_EXPIRED && timer->cb)
				timer->cb(timer->data);
			timer_state = mcastd_timer_state(timer);
			if (timer_state == MCASTD_TIMER_EXPIRED ||
					timer_state == MCASTD_TIMER_CANCELED) {
				mcastd_timer_idle(timer);
				if (timer->destroy)
					timer->destroy(timer->data);
			}
			if (timer_lock)
				mutex_unlock(timer_lock);
			continue;
		}
		schedule_timeout_interruptible(mcastd_timeout());
	}
	__set_current_state(TASK_RUNNING);
	while (mcastd_work_pull(&work)) {
		if (work->work.destroy)
			work->work.destroy(work->work.data);
		kfree(work);
	}
	while (mcastd_timer_pull(&timer, 0, 1)) {
		timer_lock = timer->lock;
		if (timer_lock)
			mutex_lock(timer_lock);
		if (timer->destroy)
			timer->destroy(timer->data);
		if (timer_lock)
			mutex_unlock(timer_lock);
	}
	return 0;
}

/*
 * (Re-)schedule timer such that it expires @a expires ms from now.
 *
 * The function:
 * - removes the timer from a list if it's in a list.
 * - inserts the timer in list mcastd_timers such that any timers expiring earlier
 *   are in front of it, and any timers expiring later are behind it.
 * - sets the timer to RUNNING.
 * - wakes up mcastd_thread.
 */
void mcastd_timer_schedule(struct mcastd_timer *timer, unsigned long expires)
{
	struct mcastd_timer *t;
	unsigned long irqflags;
	spin_lock_irqsave(&mcastd_timerlock, irqflags);
	if (timer->head.next)
		__list_del(timer->head.prev, timer->head.next);
	// unit of 'expires' changes from ms to jiffies in next stmt
	expires = jiffies + expires * HZ / 1000;
	list_for_each_entry(t, &mcastd_timers, head) {
		if (t->state != MCASTD_TIMER_RUNNING)
			continue;
		if (mcastd_expcmp(t->expires, expires) > 0)
			break; // timer t expires later than new timeout period
	}
	list_add_tail(&timer->head, &t->head); // insert 'timer' before 't'
	timer->expires = expires;
	timer->state = MCASTD_TIMER_RUNNING;
	spin_unlock_irqrestore(&mcastd_timerlock, irqflags);
	wake_up_process(mcastd_thread);
}

/*
 * Cancel timer.
 *
 * If the timer is in a list, remove it from the list, set state to
 * MCASTD_TIMER_CANCELED, add it to the front of mcastd_timers and wake up
 * mcastd_thread.
 */
void mcastd_timer_cancel(struct mcastd_timer *timer)
{
	unsigned long irqflags;
	spin_lock_irqsave(&mcastd_timerlock, irqflags);
	if (timer->head.next) {
		__list_del(timer->head.prev, timer->head.next);
		timer->state = MCASTD_TIMER_CANCELED;
		list_add(&timer->head, &mcastd_timers);
	}
	spin_unlock_irqrestore(&mcastd_timerlock, irqflags);
	wake_up_process(mcastd_thread);
}

/*
 * Return in how many milliseconds the timer expires.
 *
 * @return 0 if timer is not running.
 * @return 1 if timer is running and expired.
 * @return > 1 if timer is not yet expired.
 */
unsigned long mcastd_timer_expires(struct mcastd_timer *timer)
{
	enum mcastd_timer_state state;
	unsigned long ret, now = jiffies;
	unsigned long irqflags;
	spin_lock_irqsave(&mcastd_timerlock, irqflags);
	state = timer->state;
	ret = timer->expires;
	spin_unlock_irqrestore(&mcastd_timerlock, irqflags);
	if (state != MCASTD_TIMER_RUNNING)
		return 0;
	if (mcastd_expcmp(ret, now) * 1000 / HZ > 0)
		return (ret - now) * 1000 / HZ;
	return 1;
}

enum mcastd_timer_state mcastd_timer_state(struct mcastd_timer *timer)
{
	enum mcastd_timer_state ret;
	unsigned long irqflags;
	spin_lock_irqsave(&mcastd_timerlock, irqflags);
	ret = timer->state;
	spin_unlock_irqrestore(&mcastd_timerlock, irqflags);
	return ret;
}

int mcastd_daemon_init(void)
{
	mcastd_thread = kthread_run(mcastd_run, NULL, "mcastd");
	if (!mcastd_thread) {
		ERRMSG("kthread_create() failed.");
		return -1;
	}
#ifndef CONFIG_MCASTD_CORE_NICE
#define CONFIG_MCASTD_CORE_NICE -18
#endif
	set_user_nice(mcastd_thread, CONFIG_MCASTD_CORE_NICE);
	return 0;
}

void mcastd_daemon_cleanup(void)
{
	atomic_set(&mcastd_dead, 1);
	kthread_stop(mcastd_thread);
}

EXPORT_SYMBOL(mcastd_work_schedule);
EXPORT_SYMBOL(mcastd_timer_schedule);
EXPORT_SYMBOL(mcastd_timer_cancel);
EXPORT_SYMBOL(mcastd_timer_expires);
EXPORT_SYMBOL(mcastd_timer_state);
