obj-m := $(KMOD_NAME).o
ccflags-y += -I$(COMPONENT_TOPDIR)/include -I$(COMPONENT_TOPDIR)/include/mcastd -I$(COMPONENT_TOPDIR)/include/mcastd/common -I$(COMPONENT_TOPDIR)/include/mcastd/kernel
$(KMOD_NAME)-y := kmod_$(KMOD_NAME).o proxyc_amx.o

ifeq (,$(findstring mcastd-core.symvers,$(KBUILD_EXTRA_SYMBOLS)))
# Manually add kernel_core symvers because OPENWRT symbol populate happens only after the whole package installation
override KBUILD_EXTRA_SYMBOLS += $(COMPONENT_TOPDIR)/src/kernel_core/Module.symvers
else
# Replace kernel_core symvers because OPENWRT symbol populate happens only after the whole package installation
override KBUILD_EXTRA_SYMBOLS := $(KBUILD_EXTRA_SYMBOLS:%/mcastd-core.symvers=$(COMPONENT_TOPDIR)/src/kernel_core/Module.symvers)
endif

ifeq ($(CONFIG_MCASTD_CORE_DEBUG), y)
ccflags-y += -DCONFIG_MCASTD_CORE_DEBUG
endif