/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __MCASTD_KERNEL_PROXYC_PRIV_H__
#define __MCASTD_KERNEL_PROXYC_PRIV_H__

#include <linux/net.h>

#include <mcastd/kernel/mdb.h>

/**
 * struct mcastdpc_iface - Private data of proxy/client at interface level
 * @client_enable: true if proxy/client is enabled for this interface, else
 *                 false
 * @addr_set: true if interface has IP address, else false
 * @is_up: true if the interface whose status is monitored by mcastd_core-plugin
 *         is up, else false
 *
 * 'client_enable' and 'addr_set' are properties of a mcastd WAN interface, such
 * as MCASTD.Intf.iptv. 'is_up' is usually a property of the same mcastd WAN
 * interface. But some setups have a bridge with mcastd WAN interface, e.g.
 * bridge_viptv. The bridge typically has 2 interfaces:
 * - one interface with NeMo flag vod-iptv for video on demand (VOD), which is
 *   unicast
 * - one interface with NeMo flag mcast-iptv for IPTV.
 * Then 'is_up' indicates whether the interface with flag mcast-iptv is up or
 * not. It's possible that bridge_viptv has an IP address (and hence is up),
 * while the interface with flag mcast-iptv is still down. The proxy/client has
 * to wait for 'is_up' to become true for that latter interface. As long as
 * is_up is false, it does not make sense to send an IGMP/MLD report upstream.
 */
struct mcastdpc_iface {
	bool client_enable;
	bool addr_set;
	bool is_up;
	struct socket *sock;
};

void mcastdpc_set_intf(struct mdb_iface *iface, struct mcastdpc_iface *config);

int mcastdpc_amx_init(void);
void mcastdpc_amx_cleanup(void);
#endif
