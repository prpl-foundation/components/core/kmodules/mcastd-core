-include $(CONFIGDIR)/components.config

export INSTALL ?= install
export PKG_CONFIG_LIBDIR ?= /usr/lib/pkgconfig
export BINDIR ?= /usr/bin
export LIBDIR ?= /usr/lib
export SLIBDIR ?= /usr/lib
export LUALIBDIR ?= /usr/lib/lua
export INCLUDEDIR ?= /usr/include
export INITDIR ?= /etc/init.d
export ACLDIR ?= /etc/acl
export DOCDIR ?= $(D)/usr/share/doc/mcastd-core
export PROCMONDIR ?= /usr/lib/processmonitor/scripts
export RESETDIR ?= /etc/reset
export MACHINE ?= $(shell $(CC) -dumpmachine)

export COMPONENT = mcastd-core
export COMPONENT_TOPDIR = $(CURDIR)

compile:
	$(MAKE) -C $(LINUX_DIR) modules M=$(BUILD_DIR)/src/kernel_core
	$(MAKE) -C $(LINUX_DIR) modules M=$(BUILD_DIR)/src/kernel_proxy

clean:
	$(MAKE) -C src/kernel_core clean
	$(MAKE) -C src/kernel_proxy clean

install:
	$(INSTALL) -d -m 0755 $(D)/$(INCLUDEDIR)/mcastd/common
	$(INSTALL) -D -p -m 0644 include/mcastd/common/*.h $(D)$(INCLUDEDIR)/mcastd/common/
	$(INSTALL) -d -m 0755 $(D)/$(INCLUDEDIR)/mcastd/kernel
	$(INSTALL) -D -p -m 0644 include/mcastd/kernel/*.h $(D)$(INCLUDEDIR)/mcastd/kernel/
	$(INSTALL) -D -p -m 0644 include/mcastd/kernel.h $(D)$(INCLUDEDIR)/mcastd/kernel.h

.PHONY: compile clean install
