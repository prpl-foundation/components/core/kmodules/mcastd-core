/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <linux/ip.h>
#include <linux/netdevice.h>
#include <linux/socket.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/fs.h>
#include <linux/inetdevice.h>
#include <linux/cdev.h>
#include <linux/version.h>
#include <linux/bitops.h>
#include <net/ip.h>
#include <net/ipv6.h>

#define IDENT "proxyc"
#include <mcastd/kernel.h>
#include <mcastd/kernel/proxyc_priv.h>

static int mcastdpc_iface2var(struct kvarbuf *buf, struct kvar *var,
                              void *data)
{
	struct kvar *v0 = var, *v1;
	struct mcastdpc_iface *iaux = data;
	struct mdb_address skaddr = { .family = AF_UNIX };
	memcpy(skaddr.data, &iaux->sock, sizeof(struct socket *));
	kvar_map_init(buf, v0, 4);
	kvar_map_edit(buf, v0, 0, "client_enable", &v1);
	kvar_uint_set(buf, v1, iaux->client_enable);
	kvar_map_done(buf, v0, 0);
	kvar_map_edit(buf, v0, 1, "addr_set", &v1);
	kvar_bool_set(buf, v1, iaux->addr_set);
	kvar_map_done(buf, v0, 1);
	kvar_map_edit(buf, v0, 2, "is_up", &v1);
	kvar_bool_set(buf, v1, iaux->is_up);
	kvar_map_done(buf, v0, 2);
	kvar_map_edit(buf, v0, 3, "sock", &v1);
	kvar_string_set(buf, v1, mdb_address_to_string(&skaddr));
	kvar_map_done(buf, v0, 3);
	return 0;
}

static struct mdb_auxinfo mcastdpc_ifaceinfo = {
	.level = MDB_LEVEL_IFACE,
	.ref = MDB_REF_PROXY_CLIENT,
	.name = "proxyc",
	.size = sizeof(struct mcastdpc_iface),
	.data2var = mcastdpc_iface2var
};

/**
 * iface_set_group_filter() - Subscribe/unsubscribe to/from MC stream.
 *
 * Send one or more (default two) IGMP/MLD reports to all IGMP/MLD routers to
 * subscribe or unsubscribe.
 *
 * When subscribing to a MC stream, it also ensures the HGW sends an IGMP/MLD
 * report in response to an IGMP/MLD query checking if the HGW is still
 * interested in the MC stream.
 *
 * NOTE - The number of IMGP/MLD reports sent is determined by the robustness
 * variable. The default value is 2. See IGMP_QUERY_ROBUSTNESS_VARIABLE and
 * MLD_QRV_DEFAULT in the Linux kernel. They can be tuned via:
 * - /proc/sys/net/ipv4/igmp_qrv for IGMP, and
 * - /proc/sys/net/ipv6/mld_qrv for MLD.
 *
 * Does it also configure the box to pass/block MC streams ?
 */
static inline void iface_set_group_filter(struct group_filter *g_filter,
		struct mdb_iface *iface, struct mdb_group *grec) {

	int ret;
	int size = GROUP_FILTER_SIZE(g_filter->gf_numsrc);
	int level;
	struct mcastdpc_iface *iaux;
	struct group_req greq;
	
	iaux = mdb_iface_auxdata(iface,	MDB_REF_PROXY_CLIENT);
		
	DEBMSG("Set group filter iface ifindex=%d iaux=%p sock=%p",
	       iface->ifindex, iaux, iaux->sock);

	if (!iaux->sock) {
		ERRMSG("No socket created for this interface");
		return;
	}

	switch (iface->family)
	{
	case AF_INET:  level = SOL_IP;   break;
	case AF_INET6: level = SOL_IPV6; break;
	default:
		ERRMSG("Crappy address family: %u", g_filter->gf_group.ss_family);
		return;
	}

	greq.gr_interface = iface->ifindex;
	mdb_address_to_sockaddr(&grec->address, &greq.gr_group);

	if (g_filter->gf_fmode == MCAST_INCLUDE && g_filter->gf_numsrc == 0) {
		/* special case - (INCLUDE, empty) == LEAVE_GROUP */
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(5,8,0))
		if(level == SOL_IP) {
			ret = ip_setsockopt(iaux->sock->sk, level, MCAST_LEAVE_GROUP,
			                    KERNEL_SOCKPTR(&greq),
			                    sizeof(struct group_req));
		} else {
			ret = ipv6_setsockopt(iaux->sock->sk, level, MCAST_LEAVE_GROUP,
			                      KERNEL_SOCKPTR(&greq),
			                      sizeof(struct group_req));
		}
#else
		ret = kernel_setsockopt(iaux->sock, level, MCAST_LEAVE_GROUP,
		                        (char*)(&greq),
		                        sizeof(struct group_req));
#endif
		if (ret < 0) {
			ERRMSG("Error [%d] in leaving MC group", ret);
			return;
		}
		return;
	}

	/* First join the group. Otherwise the function call x_setsockopt(..)
	 * below with MCAST_MSFILTER as value for optname returns EINVAL. It calls
	 * ip_mc_msfilter(..)/ip6_mc_msfilter(..), and that function returns EINVAL
	 * if there is no prior join.
	 */
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(5,8,0))
	if(level == SOL_IP) {
		ret = ip_setsockopt(iaux->sock->sk, level, MCAST_JOIN_GROUP, KERNEL_SOCKPTR(&greq),
		                    sizeof(struct group_req));
	} else {
		ret = ipv6_setsockopt(iaux->sock->sk, level, MCAST_JOIN_GROUP, KERNEL_SOCKPTR(&greq),
		                        sizeof(struct group_req));
	}
#else
	ret = kernel_setsockopt(iaux->sock, level, MCAST_JOIN_GROUP, (char*)(&greq),
	                        sizeof(struct group_req));
#endif
	if (ret < 0) {
		ERRMSG("Error [%d] in joining MC group", ret);
		return;
	}

	g_filter->gf_interface = iface->ifindex;

#if (LINUX_VERSION_CODE >= KERNEL_VERSION(5,8,0))
	if(level == SOL_IP) {
		ret = ip_setsockopt(iaux->sock->sk, level, MCAST_MSFILTER, KERNEL_SOCKPTR(g_filter),
		                    size);
	} else {
		ret = ipv6_setsockopt(iaux->sock->sk, level, MCAST_MSFILTER, KERNEL_SOCKPTR(g_filter),
		                      size);
	}
#else
	ret = kernel_setsockopt(iaux->sock, level, MCAST_MSFILTER, (char*)g_filter,
	                        size);
#endif
	if (ret < 0)
		ERRMSG("Error[%d] in setting multicast filter", ret);
}

static __u32 fill_gfslist(struct mdb_iface *irec, struct mdb_group *grec,
                          struct group_filter *gf, __u32 reftest, __u32 refmask)
{
	struct sockaddr_storage *ss = gf ? gf->gf_slist : NULL;
	struct mdb_source *srec;
	__u32 numsrc = 0;
	__u32 srefs = 0;
	list_for_each_entry(srec, &grec->msources, mhead) {
		if (mdb_address_any(&srec->address))
			continue; // ignore any-address
		srefs = 0;
		do {
			/* ignore memberships on this proxyc intf */
			if (srec->group->iface == irec)
				goto next_srec;
			srefs |= srec->refs;
next_srec:
			srec = list_entry(srec->mdups.next, struct mdb_source,
			                  mdups);
		} while (srec != srec->mlead);
		if ((srefs & refmask) != reftest)
			continue;
		numsrc++;
		if (!gf)
			continue;
		mdb_address_to_sockaddr(&srec->address, ss++);
	}
	return numsrc;
}

static inline void print_sockaddr(struct sockaddr_storage *ss)
{
	if (ss->ss_family == AF_INET)
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,36))
		DEBMSG("%pI4", &((struct sockaddr_in*)ss)->sin_addr.s_addr);
#else
		DEBMSG(NIPQUAD_FMT, NIPQUAD(((struct sockaddr_in*)ss)->sin_addr.s_addr));
#endif
	else
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,30))
		DEBMSG("%pI6", &((struct sockaddr_in6*)ss)->sin6_addr.s6_addr);
#else
		DEBMSG(NIP6_FMT, NIP6(((struct sockaddr_in6*)ss)->sin6_addr));
#endif
}

static void print_group_filter(struct group_filter *g_filter) 
{
	int i;
	struct sockaddr_storage *ss = g_filter->gf_slist;

	DEBMSG("==== groupfilter start ====");
	DEBMSG("interface[%d], mode[%s] numsrc[%d]", g_filter->gf_interface,
			(g_filter->gf_fmode == MCAST_INCLUDE ? "IN" : "EX"),
			g_filter->gf_numsrc);
	DEBMSG("group:");
	print_sockaddr(&g_filter->gf_group);
	if (g_filter->gf_numsrc) {
		DEBMSG("sources:");
		for (i = 0;  i < g_filter->gf_numsrc; i++) {
			print_sockaddr(ss++);
		}
	}
	DEBMSG("==== groupfilter end ====");

}

static struct group_filter *build_group_filter(struct mdb_iface *irec,
                                               struct mdb_group *grec)
{
	__u32 fmode = MCAST_INCLUDE;
	__u32 numsrc = 0;
	__u32 refmask;
	__u32 reftest;
	struct group_filter *gf = NULL;

	/* determine filter mode */
	grec = grec->mlead;
	do {
		/* ignore memberships on this proxyc intf */
		if (grec->iface == irec)
			goto next_grec;
		if (grec->refs & MDB_REFMASK_SNOOPER_EX)
			fmode = MCAST_EXCLUDE;
next_grec:
		grec = list_entry(grec->mdups.next, struct mdb_group, mdups);
	} while (grec != grec->mlead);

	if (fmode == MCAST_EXCLUDE) {
		/* Put sources in list that have at least one EX record
		 * and zero IN records. */
		reftest = MDB_REFMASK_SNOOPER_EX;
		refmask = MDB_REFMASK_SNOOPER;
	} else {
		/* Put sources in list that have at least one IN record.
		 * Don't care about EX records. */
		reftest = MDB_REFMASK_SNOOPER_IN;
		refmask = MDB_REFMASK_SNOOPER_IN;
	}

	/* sflist - dry run to find the number of sources */
	numsrc = fill_gfslist(irec, grec, NULL, reftest, refmask);

	/* initialize group filter */
	gf = kzalloc(GROUP_FILTER_SIZE(numsrc), GFP_KERNEL);
	if (!gf) {
		ERRMSG("kzalloc(%u) failed", (unsigned int)GROUP_FILTER_SIZE(numsrc));
		return NULL;
	}
	gf->gf_fmode = fmode;
	mdb_address_to_sockaddr(&grec->address, &gf->gf_group);
	gf->gf_interface = irec->ifindex;

	/* sflist - for real now */
	gf->gf_numsrc = fill_gfslist(irec, grec, gf, reftest, refmask);
			
	print_group_filter(gf);
	return gf;
}

static void mdb_event_handler(struct mdb_event *e)
{
	struct mdb_iface *irec;
	struct mcastdpc_iface *iaux;
	struct group_filter *gf;

	if (!(e->change[MDB_LEVEL_GROUP] & MDB_REFMASK_SNOOPER) &&
			!(e->change[MDB_LEVEL_SOURCE] & MDB_REFMASK_SNOOPER))
		return;
	
	DEBMSG("Group %p %s change[0x%x] refs[0x%x]",
			e->group,
			mdb_address_to_string(&e->group->address),
			e->group->change,
			e->group->refs);

	list_for_each_entry(irec, &mdb_ifaces, head) {
		iaux = mdb_iface_auxdata(irec, MDB_REF_PROXY_CLIENT);
		if (!iaux || !iaux->sock)
			continue;
		if (irec->address.family != e->group->address.family)
			continue;
		gf = build_group_filter(irec, e->group);
		if (!gf)
			continue;
		iface_set_group_filter(gf, irec, e->group);
		kfree(gf);
	}
}

static inline void iface_changed(struct mdb_iface *irec)
{
	struct mcastdpc_iface *iaux = mdb_iface_auxdata(irec, MDB_REF_PROXY_CLIENT);
	struct mdb_group *grec;
	struct group_filter *gf;

	DEBMSG("client_enable=%d addr_set=%d is_up=%d",
			iaux->client_enable, iaux->addr_set, iaux->is_up);

	if (iaux->client_enable && iaux->addr_set && iaux->is_up) {
		if(sock_create(irec->address.family, SOCK_DGRAM, 0, &iaux->sock) < 0) {
			ERRMSG("socket creation failed");
			return;
		}
		/* TODO Is next line needed ? It sets a socket flag. But it's nowhere
		 * documented. */
		//set_bit(SOCK_KMCD_PROXYC, &iaux->sock->flags);

		list_for_each_entry(grec, &mdb_groups, mhead) {
			gf = build_group_filter(irec, grec);
			if (!gf)
				continue;
			iface_set_group_filter(gf, irec, grec);
			kfree(gf);
		}
	} else {
		if (iaux->sock) {
			sock_release(iaux->sock);
			iaux->sock = NULL;
		}
	}
}

/**
 * mcastdpc_set_intf() - Update the status of the interface
 * @iface: info about the interface from mcastd-core
 * @config: info specific for this client/proxy part (coming from user space)
 */
void mcastdpc_set_intf(struct mdb_iface *iface, struct mcastdpc_iface *config)
{
	struct mcastdpc_iface *iaux = mdb_iface_auxdata(iface, MDB_REF_PROXY_CLIENT);
	if ((iaux->client_enable != config->client_enable) ||
	    (iaux->addr_set != config->addr_set) ||
	    (iaux->is_up != config->is_up)) {
		iaux->client_enable = config->client_enable;
		iaux->addr_set = config->addr_set;
		iaux->is_up = config->is_up;
		iface_changed(iface);
	}
}

static int mcastdpc_init(void)
{
	mdb_lock();
	mdb_register_aux(&mcastdpc_ifaceinfo);
	mdb_subscribe(MDB_PRIORITY_CRIT, mdb_event_handler);
	mdb_unlock();
	mcastdpc_amx_init();
	DEBMSG("mcastdpc successfully loaded");
	return 0;
}

static void mcastdpc_exit(void)
{
	mcastdpc_amx_cleanup();
	mdb_lock();
	mdb_unsubscribe(MDB_PRIORITY_CRIT, mdb_event_handler);
	mdb_unregister_aux(&mcastdpc_ifaceinfo);
	mdb_unlock();
	DEBMSG("mcastdpc unloaded");
}

module_init(mcastdpc_init);
module_exit(mcastdpc_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Cedric Jehasse <cedric.jehasse@softathome.com>");
MODULE_DESCRIPTION("IGMP Proxy client");
MODULE_VERSION("2.6-v1.0");
