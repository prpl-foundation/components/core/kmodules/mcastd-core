obj-m := $(KMOD_NAME).o
ccflags-y += -I$(COMPONENT_TOPDIR)/include -I$(COMPONENT_TOPDIR)/include/mcastd -I$(COMPONENT_TOPDIR)/include/mcastd/common -I$(COMPONENT_TOPDIR)/include/mcastd/kernel
$(KMOD_NAME)-y := kmod_$(KMOD_NAME).o kvariant.o netlink.o kamx.o debug.o daemon.o mdb.o mdb_amx.o \
                snooper.o snooper_amx.o snooper_igmp.o snooper_mld.o snooper_ebt.o \
                bwctrl.o bwctrl_amx.o whitelist.o whitelist_amx.o rate_limiting.o

ifdef $(CONFIG_MCASTD_CORE_NICE)
ccflags-y += -DCONFIG_MCASTD_CORE_NICE=$(CONFIG_MCASTD_CORE_NICE)
endif
ifeq ($(CONFIG_MCASTD_CORE_DEBUG), y)
ccflags-y += -DCONFIG_MCASTD_CORE_DEBUG
endif
ifeq ($(CONFIG_MCASTD_CORE_ACCESS_CONTROL), y)
ccflags-y += -DCONFIG_MCASTD_CORE_ACCESS_CONTROL
endif