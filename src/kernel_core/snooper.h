/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __MCASTD_KERNEL_SNOOPER_H__
#define __MCASTD_KERNEL_SNOOPER_H__

#include <linux/types.h>
#include <linux/socket.h>

#include <mcastd/kernel/daemon.h> /* mcastd_timer */
#include <mcastd/kernel/mdb.h> /* mdb_iface */

enum mcastdsn_record {
	MCASTDSN_RECORD_IS_IN = 1,
	MCASTDSN_RECORD_IS_EX,
	MCASTDSN_RECORD_TO_IN,
	MCASTDSN_RECORD_TO_EX,
	MCASTDSN_RECORD_ALLOW,
	MCASTDSN_RECORD_BLOCK,
	MCASTDSN_RECORD_MIN = MCASTDSN_RECORD_IS_IN,
	MCASTDSN_RECORD_MAX = MCASTDSN_RECORD_BLOCK,
	MCASTDSN_RECORD_COUNT = MCASTDSN_RECORD_MAX - MCASTDSN_RECORD_MIN + 1
};

enum mcastdsn_proto {
	MCASTDSN_PROTO_IGMP,
	MCASTDSN_PROTO_MLD,
	MCASTDSN_PROTO_AMX,
	MCASTDSN_PROTO_COUNT
};

struct mcastdsn_querier {
	void (*iface_query)(struct mdb_iface *iface);
	void (*group_query)(struct mdb_iface *iface,
	                    struct mdb_address *group, int sflag);
	void (*source_query_init)(struct mdb_iface *iface,
	                          struct mdb_address *group, int sflag);
	void (*source_query_push)(struct mdb_address *source);
	void (*source_query_done)(void);
};

/*
 * Auxiliary data registered by snooper at interface level.
 *
 * - timer: used for sending general queries.
 * - sqc: startup query count. When starting up, a querier sends a few queries
 *        at a higher rate. This field indicates the number of such queries
 *        left.
 * - upstream_rate: upstream IGMP/MLD rate in messages/s. The default value
 *                  of 0 imposes no rate limit. Only applicable if
 *                  snooping_enabled is 1.
 * - mc_subscriptions_present: 1 if IGMP/MLD subscriptions are present, else 0.
 *     Only applicable if snooping_enabled is 1.
 *
 * The fields rate_bucket, rate_last_packet, rate_rem_time, diff_total and
 * pkts_in_second are all used for limiting the rate of upstream IGMP/MLD
 * reports. They are only used if upstream_rate != 0.
 */
struct mcastdsn_iface {
	int snooping_enabled;
	int querier_enabled;
	unsigned long querier_version;
	int fast_leave_enabled;
	int force_compat;
	struct mcastd_timer timer;
	int sqc;
	int mc_subscriptions_present;
#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL
	int max_groups;
	u32     upstream_rate;
	u32     rate_bucket;
	ktime_t rate_last_packet;
	u32     rate_rem_time;
	u64     diff_total;
	u32     pkts_in_second;
#endif
};

extern unsigned long mcastdsn_RV;
extern unsigned long mcastdsn_QI;  // [ms]
extern unsigned long mcastdsn_QRI; // [ms]
extern unsigned long mcastdsn_GMI; // [ms]
extern unsigned long mcastdsn_SQI; // [ms]
extern unsigned long mcastdsn_SQC;
extern unsigned long mcastdsn_LMQI; // [ms]
extern unsigned long mcastdsn_LMQC;
extern unsigned long mcastdsn_LMQT; // [ms]
extern unsigned long mcastdsn_OHPI; // [ms]

static inline const char *mcastdsn_record_string(__u8 record) {
	switch (record) {
	case MCASTDSN_RECORD_IS_IN: return "IS_IN";
	case MCASTDSN_RECORD_IS_EX: return "IS_EX";
	case MCASTDSN_RECORD_TO_IN: return "TO_IN";
	case MCASTDSN_RECORD_TO_EX: return "TO_EX";
	case MCASTDSN_RECORD_ALLOW: return "ALLOW";
	case MCASTDSN_RECORD_BLOCK: return "BLOCK";
	default:
		return "INVAL";
	}
}

void mcastdsn_snoop(int ifindex, __u8 family, __u8 *group, __u8 type, __u16 nsrcs,
                    __u8 *srcs, __u8 *host, int compat, struct sk_buff *skb);

void mcastdsn_register_querier(enum mcastdsn_proto proto,
                               struct mcastdsn_querier *querier);
void mcastdsn_unregister_querier(enum mcastdsn_proto proto);

void mcastdsn_set_intf(struct mdb_iface *iface, struct mcastdsn_iface *config);

int mcastdsn_get_ifindex_of_bridge(struct net_device * dev);

#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL
/**
 * struct mcastdsn_whlist_entry - Convenience type to pass whitelist info
 * @ifindex: ifindex of the IF to which the entry belongs. This is typically
 *           the ifindex of the LAN bridge.
 * @start_addr: start address of allowed MC range
 * @end_addr: end address of allowed MC range
 */
struct mcastdsn_whlist_entry {
	int ifindex;
	struct mdb_address start_addr;
	struct mdb_address end_addr;
};

void mcastdsn_free_whlist_apply(const struct mcastdsn_whlist_entry *const wl_entry);
#endif

#endif
