/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/string.h>
#include <net/netlink.h>

#define IDENT "snooper"
#include <mcastd/kernel.h>
#include "snooper.h" /* MCASTDSN_RECORD_IS_IN */

static struct kvarbuf mcastdsn_amx_query_buf = { .size = MCASTD_MAXNLMSGSIZE };
static struct kvar *mcastdsn_amx_query_params = NULL;
static struct kvar *mcastdsn_amx_query_srcs = NULL;
static int mcastdsn_amx_query_srclen = 0;
static struct sk_buff *mcastdsn_amx_query_skb = NULL;
static struct mdb_iface *mcastdsn_amx_query_iface = NULL;
static struct mdb_address *mcastdsn_amx_query_group = NULL;
static int mcastdsn_amx_query_sflag = 0;

static int mcastdsn_amx_snoop(struct nlmsghdr *nlh)
{
	struct kvarbuf buf = { .size = MCASTD_MAXNLMSGSIZE };
	struct kvar *v0, *v1, *v2;
	struct sk_buff *skb;
	int ifindex = 1;
	__u8 family = AF_INET;
	__u8 *group;
	__u8 type;
	__u16 nsrcs = 0;
	__u8 *srcs = NULL;
	__u8 *host;
	int compat = 0;
	int ret = -EINVAL;

	v0 = NLMSG_DATA(nlh);
	v1 = kvar_list_val(v0, KAMX_FCALL_PROPERTY_ARGS);
	v2 = kvar_map_find(v1, "ifindex");
	if (kvar_int_val(v2))
		ifindex = kvar_int_val(v2);
	v2 = kvar_map_find(v1, "group");
	if (!kvar_addr_val(v2))
		goto leave;
	family = kvar_addr_fam(v2);
	group = kvar_addr_val(v2);
	v2 = kvar_map_find(v1, "type");
	if (!kvar_string_val(v2))
		goto leave;
	if (!strcmp(kvar_string_val(v2), "IS_IN"))
		type = MCASTDSN_RECORD_IS_IN;
	else if (!strcmp(kvar_string_val(v2), "IS_EX"))
		type = MCASTDSN_RECORD_IS_EX;
	else if (!strcmp(kvar_string_val(v2), "TO_IN"))
		type = MCASTDSN_RECORD_TO_IN;
	else if (!strcmp(kvar_string_val(v2), "TO_EX"))
		type = MCASTDSN_RECORD_TO_EX;
	else if (!strcmp(kvar_string_val(v2), "ALLOW"))
		type = MCASTDSN_RECORD_ALLOW;
	else if (!strcmp(kvar_string_val(v2), "BLOCK"))
		type = MCASTDSN_RECORD_BLOCK;
	else
		goto leave;
	v2 = kvar_map_find(v1, "source");
	if (kvar_addr_val(v2)) {
		if (kvar_addr_fam(v2) != family)
			goto leave;
		srcs = kvar_addr_val(v2);
		nsrcs = 1;
	}
	v2 = kvar_map_find(v1, "host");
	if (kvar_addr_fam(v2) != family)
		goto leave;
	host = kvar_addr_val(v2);
	v2 = kvar_map_find(v1, "compat");
	if (kvar_bool_val(v2))
		compat = kvar_bool_val(v2);

	mcastdsn_snoop(ifindex, family, group, type, nsrcs, srcs, host, compat,
	               NULL);

	ret = -ENOMEM;
	if (mcastdnl_make(MCASTDNL_TYPE_FCALL, nlh->nlmsg_pid, nlh->nlmsg_seq,
	                  MCASTD_MAXNLMSGPAYLOAD, &buf.data.nlh, &skb))
		goto leave;
	v0 = NLMSG_DATA(buf.data.nlh);
	kvar_list_init(&buf, v0, KAMX_FCALL_PROPERTY_COUNT);
	mcastdnl_unicast(skb, buf.data.nlh, KVAR_LENGTH(v0), nlh->nlmsg_pid);
	ret = 0;
leave:
	return ret;
}

/**
 * struct amx_kvar - Helper struct to pass info for a kvar
 * @type: indicates if 'val' holds a boolean, an (u)int or a string. Contrary to
 *        a 'struct kvar', it does not allow all values of kvartype: only
 *        KVARTYPE_BOOL, KVARTYPE_INT, KVARTYPE_UINT and KVARTYPE_STRING are
 *        allowed.
 * @val: the actual value
 */
struct amx_kvar
{
	__u16 type;
	union {
		int b;
		__s32 i;
		__u32 u;
		const char *s;
	} val;
};

/**
 * mcastdsn_amx_send_value_common() - Send value to US
 * @path: path to object in mcastd data model. Examples: "MCASTD.Status",
 *        "MCASTD.Intf.lan".
 * @name: name of parameter in data model. This must be a parameter
 *        belonging to the object identified by 'path'.
 * @value: new value for the parameter
 *
 * Send a MC netlink message to US to notify US that the value of the parameter
 * identified by 'path' and 'name' has changed to 'value'.
 */
static void mcastdsn_amx_send_value_common(const char *path, const char *name,
                                           struct amx_kvar *value)
{
	struct sk_buff *skb;
	struct kvar *v0, *v1, *v2;
	struct kvarbuf buf = { .size = MCASTD_MAXNLMSGSIZE };

	if ((value->type != KVARTYPE_BOOL) &&
		(value->type != KVARTYPE_INT) &&
		(value->type != KVARTYPE_UINT) &&
		(value->type != KVARTYPE_STRING)) {
		ERRMSG("%s.%s: value->type=%d: not supported", path, name, value->type);
		return;
	}

	if (mcastdnl_make(MCASTDNL_TYPE_NEWOBJ, 0, 0, MCASTD_MAXNLMSGPAYLOAD,
	                  &buf.data.nlh, &skb))
		return;

	v0 = NLMSG_DATA(buf.data.nlh);
	kvar_list_init(&buf, v0, KAMX_OBJECT_PROPERTY_COUNT);
	/* 1) path */
	kvar_list_edit(&buf, v0, KAMX_OBJECT_PROPERTY_PATH, &v1);
	kvar_string_set(&buf, v1, path);
	kvar_list_done(&buf, v0, KAMX_OBJECT_PROPERTY_PATH);
	/* 2) parameters */
	kvar_list_edit(&buf, v0, KAMX_OBJECT_PROPERTY_PARAMS, &v1);
	kvar_map_init(&buf, v1, 1);
	kvar_map_edit(&buf, v1, 0, name, &v2);
	switch (value->type) {
	case KVARTYPE_BOOL:
		kvar_bool_set(&buf, v2, value->val.b);
		break;
	case KVARTYPE_INT:
		kvar_int_set(&buf, v2, value->val.i);
		break;
	case KVARTYPE_UINT:
		kvar_uint_set(&buf, v2, value->val.u);
		break;
	case KVARTYPE_STRING:
		kvar_string_set(&buf, v2, value->val.s);
		break;
	default:
		break;
	}
	kvar_map_done(&buf, v1, 0);
	kvar_list_done(&buf, v0, KAMX_OBJECT_PROPERTY_PARAMS);

	mcastdnl_multicast(skb, buf.data.nlh, KVAR_LENGTH(v0),
	                   0, MCASTDNL_GROUP_AMX_ALWAYS, GFP_KERNEL);
}

#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL

/**
 * mcastdsn_amx_send_status_value() - Send status value to US
 * @name: name of the status parameter
 * @value: new value of the parameter
 *
 * Send a MC netlink message to US to notify US that the value of a status
 * parameter has changed.
 *
 * The value of 'name' must match the name of a parameter of the object
 * 'MCASTD.Status' in the mcastd data model.
 */
void mcastdsn_amx_send_status_value(const char *name, u32 value)
{
	struct amx_kvar v;

	v.type = KVARTYPE_UINT;
	v.val.u = value;

	mcastdsn_amx_send_value_common("MCASTD.Status", name, &v);
}
#endif /* CONFIG_MCASTD_CORE_ACCESS_CONTROL */

/**
 * mcastdsn_amx_send_iface_bool_value() - Send bool value for an interface to US
 * @iface: name of the mcastd interface. This should match with one of the
 *         interfaces in the mcastd data model, e.g. 'lan' if MCASTD.Intf.lan
 *         exists.
 * @name: name of parameter in data model
 * @value: parameter to pass boolean value. Possible values: 0 and 1.
 */
void mcastdsn_amx_send_iface_bool_value(const char *iface, const char *name, int value)
{
	struct amx_kvar v;
	char path[MDB_IFACE_NAMESIZE + 32];

	v.type = KVARTYPE_BOOL;
	v.val.b = value;

	snprintf(path, MDB_IFACE_NAMESIZE + 32, "MCASTD.Intf.%s", iface);

	mcastdsn_amx_send_value_common(path, name, &v);
}

/*
 * Set general properties such as SnoopingEnable for an interface.
 *
 * Extract the name of the interface and the properties from @a nlh. Find
 * the interface in mdb using the name. Return if interface does not exist,
 * else call mcastdsn_set_intf(..).
 */
static int mcastdsn_amx_set_intf(struct nlmsghdr *nlh)
{
	struct mdb_iface *iface;
	struct mcastdsn_iface config;
	struct kvar *v0, *v1, *v2;
	const char *key;
	mdb_lock();
	v0 = NLMSG_DATA(nlh);
	v1 = kvar_list_val(v0, KAMX_OBJECT_PROPERTY_PATH);
	key = kvar_string_val(v1) + strlen("MCASTD.Intf.");
	iface = mdb_iface_find(key);
	if (!iface)
		goto leave;
	v1 = kvar_list_val(v0, KAMX_OBJECT_PROPERTY_PARAMS);
	memset(&config, 0, sizeof(struct mcastdsn_iface));
	switch (nlh->nlmsg_type) {
	case MCASTDNL_TYPE_NEWOBJ:
		v2 = kvar_map_find(v1, "SnoopingEnable");
		config.snooping_enabled = kvar_bool_val(v2);
		v2 = kvar_map_find(v1, "QuerierEnable");
		config.querier_enabled = kvar_bool_val(v2);
		v2 = kvar_map_find(v1, "QuerierVersion");
		config.querier_version = kvar_uint_val(v2);
		v2 = kvar_map_find(v1, "FastLeaveEnable");
		config.fast_leave_enabled = kvar_bool_val(v2);
		v2 = kvar_map_find(v1, "ForceCompat");
		config.force_compat = kvar_bool_val(v2);
#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL
		v2 = kvar_map_find(v1, "MaxGroups");
		config.max_groups = kvar_int_val(v2);
		v2 = kvar_map_find(v1, "UpstreamRate");
		config.upstream_rate = kvar_uint_val(v2);
#endif
		break;
	case MCASTDNL_TYPE_DELOBJ:
		break;
	default:
		goto leave;
	}
	mcastdsn_set_intf(iface, &config);
leave:
	mdb_unlock();
	return 0;
}

static int mcastdsn_amx_set_tuner(struct nlmsghdr *nlh)
{
	struct kvarbuf buf = { .size = MCASTD_MAXNLMSGSIZE };
	struct kvar *v0, *v1, *v2;
	struct sk_buff *skb;
	int i = 0;
	mdb_lock();
	v0 = NLMSG_DATA(nlh);
	v1 = kvar_list_val(v0, KAMX_OBJECT_PROPERTY_PARAMS);
	mcastdsn_RV = kvar_uint_val(kvar_map_find(v1, "RV"));
	mcastdsn_RV = mcastdsn_RV ? mcastdsn_RV : 2;
	mcastdsn_QI = kvar_uint_val(kvar_map_find(v1, "QI"));
	mcastdsn_QI = mcastdsn_QI ? mcastdsn_QI : 125000;
	mcastdsn_QRI = kvar_uint_val(kvar_map_find(v1, "QRI"));
	mcastdsn_QRI = mcastdsn_QRI ? mcastdsn_QRI : 10000;
	mcastdsn_GMI = mcastdsn_RV * mcastdsn_QI + mcastdsn_QRI;
	mcastdsn_SQI = kvar_uint_val(kvar_map_find(v1, "SQI"));
	mcastdsn_SQI = mcastdsn_SQI ? mcastdsn_SQI : mcastdsn_QI / 4;
	mcastdsn_SQC = kvar_uint_val(kvar_map_find(v1, "SQC"));
	mcastdsn_SQC = mcastdsn_SQC ? mcastdsn_SQC : mcastdsn_RV;
	mcastdsn_LMQI = kvar_uint_val(kvar_map_find(v1, "LMQI"));
	mcastdsn_LMQI = mcastdsn_LMQI ? mcastdsn_LMQI : 1000;
	/* 0 is a valid value for mcastdsn_LMQC */
	mcastdsn_LMQC = kvar_uint_val(kvar_map_find(v1, "LMQC"));
	mcastdsn_LMQT = mcastdsn_LMQI * mcastdsn_LMQC;
	mcastdsn_OHPI = mcastdsn_RV * mcastdsn_QI + mcastdsn_QRI;

	// feedback
	if (mcastdnl_make(MCASTDNL_TYPE_NEWOBJ, 0, 0, MCASTD_MAXNLMSGPAYLOAD,
	                  &buf.data.nlh, &skb))
		goto leave;
	v0 = NLMSG_DATA(buf.data.nlh);
	kvar_list_init(&buf, v0, KAMX_OBJECT_PROPERTY_COUNT);
	/* 1) path */
	kvar_list_edit(&buf, v0, KAMX_OBJECT_PROPERTY_PATH, &v1);
	kvar_string_set(&buf, v1, "MCASTD.Tuner");
	kvar_list_done(&buf, v0, KAMX_OBJECT_PROPERTY_PATH);
	/* 2) parameters */
	kvar_list_edit(&buf, v0, KAMX_OBJECT_PROPERTY_PARAMS, &v1);
	kvar_map_init(&buf, v1, 10);
	i = 0;
	kvar_map_edit(&buf, v1, i, "RV", &v2);
	kvar_uint_set(&buf, v2, mcastdsn_RV);
	kvar_map_done(&buf, v1, i++);
	kvar_map_edit(&buf, v1, i, "QI", &v2);
	kvar_uint_set(&buf, v2, mcastdsn_QI);
	kvar_map_done(&buf, v1, i++);
	kvar_map_edit(&buf, v1, i, "QRI", &v2);
	kvar_uint_set(&buf, v2, mcastdsn_QRI);
	kvar_map_done(&buf, v1, i++);
	kvar_map_edit(&buf, v1, i, "GMI", &v2);
	kvar_uint_set(&buf, v2, mcastdsn_GMI);
	kvar_map_done(&buf, v1, i++);
	kvar_map_edit(&buf, v1, i, "SQI", &v2);
	kvar_uint_set(&buf, v2, mcastdsn_SQI);
	kvar_map_done(&buf, v1, i++);
	kvar_map_edit(&buf, v1, i, "SQC", &v2);
	kvar_uint_set(&buf, v2, mcastdsn_SQC);
	kvar_map_done(&buf, v1, i++);
	kvar_map_edit(&buf, v1, i, "LMQI", &v2);
	kvar_uint_set(&buf, v2, mcastdsn_LMQI);
	kvar_map_done(&buf, v1, i++);
	kvar_map_edit(&buf, v1, i, "LMQC", &v2);
	kvar_uint_set(&buf, v2, mcastdsn_LMQC);
	kvar_map_done(&buf, v1, i++);
	kvar_map_edit(&buf, v1, i, "LMQT", &v2);
	kvar_uint_set(&buf, v2, mcastdsn_LMQT);
	kvar_map_done(&buf, v1, i++);
	kvar_map_edit(&buf, v1, i, "OHPI", &v2);
	kvar_uint_set(&buf, v2, mcastdsn_OHPI);
	kvar_map_done(&buf, v1, i++);
	kvar_list_done(&buf, v0, KAMX_OBJECT_PROPERTY_PARAMS);
	mcastdnl_multicast(skb, buf.data.nlh, KVAR_LENGTH(v0),
	                 0, MCASTDNL_GROUP_AMX_ALWAYS, GFP_KERNEL);
leave:
	mdb_unlock();
	return 0;
}

static void mcastdsn_amx_make_query(struct mdb_iface *iface)
{
	struct kvarbuf *buf = &mcastdsn_amx_query_buf;
	struct kvar *v0, *v1;
	if (mcastdnl_make(MCASTDNL_TYPE_NOTIFY, 0, 0, MCASTD_MAXNLMSGPAYLOAD,
	                  &buf->data.nlh, &mcastdsn_amx_query_skb))
		return;
	v0 = NLMSG_DATA(buf->data.nlh);
	kvar_list_init(buf, v0, KAMX_NOTIFY_PROPERTY_COUNT);
	kvar_list_edit(buf, v0, KAMX_NOTIFY_PROPERTY_PATH, &v1);
	v1->hdr.type = KVARTYPE_STRING;
	v1->hdr.cnt = strlen("MCASTD.Intf.") + strlen(iface->name);
	v1->hdr.len = v1->hdr.cnt + 1;
	strcpy(v1->val.s, "MCASTD.Intf.");
	strcat(v1->val.s, iface->name);
	kvar_list_done(buf, v0, KAMX_NOTIFY_PROPERTY_PATH);
	kvar_list_edit(buf, v0, KAMX_NOTIFY_PROPERTY_TYPE, &v1);
	kvar_uint_set(buf, v1, KAMX_NOTIFY_TYPE_QUERY);
	kvar_list_done(buf, v0, KAMX_NOTIFY_PROPERTY_TYPE);
	kvar_list_edit(buf, v0, KAMX_NOTIFY_PROPERTY_NAME, &v1);
	kvar_string_set(buf, v1, "query");
	kvar_list_done(buf, v0, KAMX_NOTIFY_PROPERTY_NAME);
}

static void mcastdsn_amx_iface_query(struct mdb_iface *iface)
{
	struct kvarbuf *buf = &mcastdsn_amx_query_buf;
	struct kvar *v0, *v1, *v2;
	mcastdsn_amx_make_query(iface);
	if (!buf->data.nlh)
		return;
	v0 = NLMSG_DATA(buf->data.nlh);
	kvar_list_edit(buf, v0, KAMX_NOTIFY_PROPERTY_PARAMS, &v1);
	kvar_map_init(buf, v1, 1);
	kvar_map_edit(buf, v1, 0, "level", &v2);
	kvar_string_set(buf, v2, "iface");
	kvar_map_done(buf, v1, 0);
	kvar_list_done(buf, v0, KAMX_NOTIFY_PROPERTY_PARAMS);
	mcastdnl_multicast(mcastdsn_amx_query_skb, buf->data.nlh,
	                   KVAR_LENGTH(v0), 0,
	                   MCASTDNL_GROUP_AMX_OPTIONAL, GFP_KERNEL);
	buf->data.nlh = NULL;
}

static void mcastdsn_amx_group_query(struct mdb_iface *iface,
                                     struct mdb_address *group, int sflag)
{
	struct kvarbuf *buf = &mcastdsn_amx_query_buf;
	struct kvar *v0, *v1, *v2;
	mcastdsn_amx_make_query(iface);
	if (!buf->data.nlh)
		return;
	v0 = NLMSG_DATA(buf->data.nlh);
	kvar_list_edit(buf, v0, KAMX_NOTIFY_PROPERTY_PARAMS, &v1);
	kvar_map_init(buf, v1, 3);
	kvar_map_edit(buf, v1, 0, "level", &v2);
	kvar_string_set(buf, v2, "group");
	kvar_map_done(buf, v1, 0);
	kvar_map_edit(buf, v1, 1, "group", &v2);
	kvar_addr_set(buf, v2, group->family, group->data);
	kvar_map_done(buf, v1, 1);
	kvar_map_edit(buf, v1, 2, "sflag", &v2);
	kvar_bool_set(buf, v2, sflag);
	kvar_map_done(buf, v1, 2);
	kvar_list_done(buf, v0, KAMX_NOTIFY_PROPERTY_PARAMS);
	mcastdnl_multicast(mcastdsn_amx_query_skb, buf->data.nlh,
	                   KVAR_LENGTH(v0), 0,
	                   MCASTDNL_GROUP_AMX_OPTIONAL, GFP_KERNEL);
	buf->data.nlh = NULL;
}

static void mcastdsn_amx_source_query_init(struct mdb_iface *iface,
                                           struct mdb_address *group, int sflag)
{
	struct kvarbuf *buf = &mcastdsn_amx_query_buf;
	struct kvar *v0, *v1, *v2;
	mcastdsn_amx_make_query(iface);
	if (!buf->data.nlh)
		return;
	v0 = NLMSG_DATA(buf->data.nlh);
	kvar_list_edit(buf, v0, KAMX_NOTIFY_PROPERTY_PARAMS, &v1);
	kvar_map_init(buf, v1, 4);
	kvar_map_edit(buf, v1, 0, "level", &v2);
	kvar_string_set(buf, v2, "source");
	kvar_map_done(buf, v1, 0);
	kvar_map_edit(buf, v1, 1, "group", &v2);
	kvar_addr_set(buf, v2, group->family, group->data);
	kvar_map_done(buf, v1, 1);
	kvar_map_edit(buf, v1, 2, "sflag", &v2);
	kvar_bool_set(buf, v2, sflag);
	kvar_map_done(buf, v1, 2);
	kvar_map_edit(buf, v1, 3, "srcs", &v2);
	kvar_list_init(buf, v2, 10);
	mcastdsn_amx_query_params = v1;
	mcastdsn_amx_query_srcs = v2;
	mcastdsn_amx_query_srclen = 0;
	mcastdsn_amx_query_iface = iface;
	mcastdsn_amx_query_group = group;
	mcastdsn_amx_query_sflag = sflag;
}

static void mcastdsn_amx_source_query_done(void)
{
	struct kvarbuf *buf = &mcastdsn_amx_query_buf;
	struct kvar *v0 = NLMSG_DATA(buf->data.nlh);
	if (!buf->data.nlh)
		return;
	if (mcastdsn_amx_query_srclen) {
		kvar_map_done(buf, mcastdsn_amx_query_params, 3);
		kvar_list_done(buf, NLMSG_DATA(buf->data.nlh),
		               KAMX_NOTIFY_PROPERTY_PARAMS);
		mcastdnl_multicast(mcastdsn_amx_query_skb, buf->data.nlh,
		                   KVAR_LENGTH(v0), 0,
		                   MCASTDNL_GROUP_AMX_OPTIONAL, GFP_KERNEL);
	} else {
		nlmsg_free(mcastdsn_amx_query_skb);
	}
	buf->data.nlh = NULL;
	mcastdsn_amx_query_srclen = 0;
}

static void mcastdsn_amx_source_query_push(struct mdb_address *source)
{
	struct kvarbuf *buf = &mcastdsn_amx_query_buf;
	struct kvar *v;
	if (mcastdsn_amx_query_srclen == 10) {
		mcastdsn_amx_source_query_done();
		mcastdsn_amx_source_query_init(mcastdsn_amx_query_iface,
		                               mcastdsn_amx_query_group,
		                               mcastdsn_amx_query_sflag);
	}
	if (!buf->data.nlh)
		return;
	kvar_list_edit(buf, mcastdsn_amx_query_srcs, mcastdsn_amx_query_srclen, &v);
	kvar_addr_set(buf, v, source->family, source->data);
	kvar_list_done(buf, mcastdsn_amx_query_srcs, mcastdsn_amx_query_srclen++);
}

static struct mcastdsn_querier mcastdsn_amx_querier = {
	.iface_query = mcastdsn_amx_iface_query,
	.group_query = mcastdsn_amx_group_query,
	.source_query_init = mcastdsn_amx_source_query_init,
	.source_query_push = mcastdsn_amx_source_query_push,
	.source_query_done = mcastdsn_amx_source_query_done,
};

int mcastd_snooper_amx_init(void)
{
	mcastd_object_register("MCASTD.Tuner", mcastdsn_amx_set_tuner);
	mcastd_object_register("MCASTD.Intf.*", mcastdsn_amx_set_intf);
	mcastd_function_register("MCASTD", "snoop", mcastdsn_amx_snoop);
	mcastdsn_register_querier(MCASTDSN_PROTO_AMX, &mcastdsn_amx_querier);
	return 0;
}

void mcastd_snooper_amx_cleanup(void)
{
	mcastdsn_unregister_querier(MCASTDSN_PROTO_AMX);
	mcastd_object_unregister("MCASTD.Tuner", mcastdsn_amx_set_tuner);
	mcastd_object_unregister("MCASTD.Intf.*", mcastdsn_amx_set_intf);
	mcastd_function_unregister("MCASTD", "snoop", mcastdsn_amx_snoop);
}

