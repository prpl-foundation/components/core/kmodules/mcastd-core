/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/string.h>
#include <linux/netdevice.h>

#include <linux/if_bridge.h> /* br_fdb_find_port() */
#include <linux/if_vlan.h> /* skb_vlan_tag_get_id() */
#include <linux/rtnetlink.h> /* rtnl_lock() */

#define IDENT "snooper"
#include <mcastd/kernel.h>
#include "snooper.h"
#include "snooper_amx.h" /* mcastdsn_amx_send_iface_bool_value() */

#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL
#include "access_control.h" /* MCASTD_ACCESS_OK_UNAUTHORIZED */
#include "whitelist.h" /* mcastdwl_whitelisted() */
#endif

#ifndef vlan_tx_tag_present
#define vlan_tx_tag_present skb_vlan_tag_present
#endif
#ifndef vlan_tx_tag_get
#define vlan_tx_tag_get skb_vlan_tag_get
#endif

#define F_IN 1 // IN = include (e.g. group mode is include)
#define F_EX 0 // EX = exclude
#define F_NA 8 // F_NA indicates that mcastdsn_snoop() iterates over @a srcs
#define F_DC 9 // DC = don't care

#define A_IN       (1 << MCASTDSN_ACTION_IN)
#define A_EX       (1 << MCASTDSN_ACTION_EX)
#define A_Q        (1 << MCASTDSN_ACTION_Q) // Q = query
#define A_GRPTIMER (1 << MCASTDSN_ACTION_GRPTIMER)
#define A_HOSTINV  (1 << MCASTDSN_ACTION_HOSTINV)
#define A_HOSTALL  (1 << MCASTDSN_ACTION_HOSTALL)

#define R_N  0 // N = no
#define R_Y  1 // Y = yes (e.g. record exists)
#define R_DC 2 // DC = don't care

enum mcastdsn_action {
	MCASTDSN_ACTION_IN,
	MCASTDSN_ACTION_EX,
	MCASTDSN_ACTION_Q,
	MCASTDSN_ACTION_GRPTIMER,
	MCASTDSN_ACTION_HOSTINV,
	MCASTDSN_ACTION_HOSTALL,
	MCASTDSN_ACTION_COUNT
};

enum mcastdsn_ref {
	MCASTDSN_REF_TIMER,
	MCASTDSN_REF_CTIMER,  /* compatibility timer */
	MCASTDSN_REF_GQTIMER, /* timer for group-specific queries */
	MCASTDSN_REF_SQTIMER  /* timer for group-and-source-specific queries */
};

/**
 * struct mcastdsn_group - Auxiliary data registered by snooper at group level.
 * @refs: indicates which parts of the snooper use the group. It's a refmask
 *        of mcastdsn_ref values. As long as this refmask is not 0, the snooper
 *        uses the group, and mcastd does not remove the group. The 4 mcastdsn_ref
 *        values correspond with the 4 timers in this struct.
 */
struct mcastdsn_group {
	int refs;
	struct mcastd_timer timer;   /* group is of no interest when timer expires */
	unsigned short nq;           /* nr of group-specific queries left */
	int sflag;                   /* Suppress flag */
	struct mcastd_timer ctimer;  /* compatibility timer */
	struct mcastd_timer gqtimer; /* timer for group-specific queries */
	struct mcastd_timer sqtimer; /* timer for group-and-source-specific queries */
};

/**
 * struct mcastdsn_source - Auxiliary data registered by snooper at source level.
 * @refs: indicates which parts of the snooper use the source. It's a refmask
 *        of mcastdsn_ref values. As long as this refmask is not 0, the snooper
 *        uses the source, and mcastd does not remove the source. The snooper at
 *        most sets following flags in 'refs': MCASTDSN_REF_TIMER and
 *        MCASTDSN_REF_SQTIMER. The 1st value corresponds with the timer in this
 *        struct. If the latter flag is set, the snooper uses the sqtimer of
 *        the mcastdsn_group of the group this source belongs to.
 */
struct mcastdsn_source {
	int refs;
	struct mcastd_timer timer; /* source is of no interest when timer expires */
	unsigned short nq; /* nr of group-and-source-specific queries left */
	int sflag;
};

/*
 * Auxiliary data registered by snooper at host level.
 */
struct mcastdsn_host {
	struct mcastd_timer timer; /* host is of no interest when timer expires */
};

struct mcastdsn_env {
	struct mdb_address gaddr;
	struct mdb_address saddr;
	struct mdb_address haddr;
	struct mdb_iface *irec;
	struct mcastdsn_iface *iaux;  /* pointer to auxiliary data of snooper in irec */
	struct mdb_group *grec;
	struct mcastdsn_group *gaux;  /* pointer to auxiliary data of snooper in grec */
	struct mdb_source *srec;
	struct mcastdsn_source *saux; /* pointer to auxiliary data of snooper in srec */
	struct mdb_host *hrec;
	struct mcastdsn_host *haux;   /* pointer to auxiliary data of snooper in hrec */
	int actions;
	int compat;
	int bridge_port; /* ifindex of bridge port on which IGMP/MLD report arrived */
};

struct mcastdsn_matrix {
	int gfilter;
	int sfilter;
	int sreport;
	int actions;
};

typedef void (*mcastdsn_action_fn)(struct mcastdsn_env *e);

static struct mcastdsn_querier *mcastdsn_querier[MCASTDSN_PROTO_COUNT];
static bool s_shutting_down = false;

unsigned long mcastdsn_RV;
unsigned long mcastdsn_QI;
unsigned long mcastdsn_QRI;
unsigned long mcastdsn_GMI;
unsigned long mcastdsn_SQI;
unsigned long mcastdsn_SQC;
unsigned long mcastdsn_LMQI;
unsigned long mcastdsn_LMQC;
unsigned long mcastdsn_LMQT;
unsigned long mcastdsn_OHPI;

/*
 * Call interface query function for @a irec for all protocols (IGMP, MLD, AMX).
 */
static void mcastdsn_iface_query(struct mdb_iface *irec)
{
	int i;
	for (i = 0; i < MCASTDSN_PROTO_COUNT; i++)
		if (mcastdsn_querier[i] && mcastdsn_querier[i]->iface_query)
			mcastdsn_querier[i]->iface_query(irec);
}

/*
 * Call group query function for interface @a irec and address @a group for all
 * protocols (IGMP, MLD, AMX).
 */
static void mcastdsn_group_query(struct mdb_iface *irec,
                                 struct mdb_address *group,
                                 int sflag)
{
	int i;
	for (i = 0; i < MCASTDSN_PROTO_COUNT; i++)
		if (mcastdsn_querier[i] && mcastdsn_querier[i]->group_query)
			mcastdsn_querier[i]->group_query(irec, group, sflag);
}

/*
 * Call source query init function for interface @a irec and address @a group
 * for all protocols (IGMP, MLD, AMX).
 */
static void mcastdsn_source_query_init(struct mdb_iface *irec,
                                       struct mdb_address *group,
                                       int sflag)
{
	int i;
	for (i = 0; i < MCASTDSN_PROTO_COUNT; i++)
		if (mcastdsn_querier[i] && mcastdsn_querier[i]->source_query_init)
			mcastdsn_querier[i]->source_query_init(irec, group,
			                                       sflag);
}

/*
 * Call source query push function for @a source for all protocols (IGMP, MLD,
 * AMX).
 */
static void mcastdsn_source_query_push(struct mdb_address *source)
{
	int i;
	for (i = 0; i < MCASTDSN_PROTO_COUNT; i++)
		if (mcastdsn_querier[i] && mcastdsn_querier[i]->source_query_push)
			mcastdsn_querier[i]->source_query_push(source);
}

/*
 * Call source query done function for all protocols (IGMP, MLD, AMX).
 */
static void mcastdsn_source_query_done(void)
{
	int i;
	for (i = 0; i < MCASTDSN_PROTO_COUNT; i++)
		if (mcastdsn_querier[i] && mcastdsn_querier[i]->source_query_done)
			mcastdsn_querier[i]->source_query_done();
}

/*
 * Put the auxiliary data of the snooper for a certain interface in @a buf.
 *
 * @param[in] data: the auxiliary data. Pointer to a mcastdsn_iface.
 */
static int mcastdsn_iface2var(struct kvarbuf *buf, struct kvar *var,
                              void *data)
{
	struct kvar *v0 = var, *v1;
	struct mcastdsn_iface *iaux = data;
	__u16 nvars = 6; /* snooping_enabled, ..., sqc */
	int i = 0;

#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL
	nvars += 2; // max_groups and upstream_rate
#endif

	kvar_map_init(buf, v0, nvars);
	kvar_map_edit(buf, v0, i, "snooping_enabled", &v1);
	kvar_uint_set(buf, v1, iaux->snooping_enabled);
	kvar_map_done(buf, v0, i++);
	kvar_map_edit(buf, v0, i, "querier_enabled", &v1);
	kvar_uint_set(buf, v1, iaux->querier_enabled);
	kvar_map_done(buf, v0, i++);
	kvar_map_edit(buf, v0, i, "fast_leave_enabled", &v1);
	kvar_uint_set(buf, v1, iaux->fast_leave_enabled);
	kvar_map_done(buf, v0, i++);
	kvar_map_edit(buf, v0, i, "force_compat", &v1);
	kvar_bool_set(buf, v1, iaux->force_compat);
	kvar_map_done(buf, v0, i++);
	kvar_map_edit(buf, v0, i, "timer", &v1);
	kvar_uint_set(buf, v1, mcastd_timer_expires(&iaux->timer));
	kvar_map_done(buf, v0, i++);
	kvar_map_edit(buf, v0, i, "sqc", &v1);
	kvar_uint_set(buf, v1, iaux->sqc);
	kvar_map_done(buf, v0, i++);
#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL
	kvar_map_edit(buf, v0, i, "max_groups", &v1);
	kvar_int_set(buf, v1, iaux->max_groups);
	kvar_map_done(buf, v0, i++);
	kvar_map_edit(buf, v0, i, "upstream_rate", &v1);
	kvar_uint_set(buf, v1, iaux->upstream_rate);
	kvar_map_done(buf, v0, i);
#endif
	return 0;
}

/*
 * Put the auxiliary data of the snooper for a certain group in @a buf.
 *
 * @param[in] data: the auxiliary data. Pointer to mcastdsn_group.
 */
static int mcastdsn_group2var(struct kvarbuf *buf, struct kvar *var,
                              void *data)
{
	struct kvar *v0 = var, *v1;
	struct mcastdsn_group *gaux = data;
	__u16 nvars = 6; /* refs, timer, nq, ctimer, gqtimer, sqtimer */
	int i = 0;

	kvar_map_init(buf, v0, nvars);
	kvar_map_edit(buf, v0, i, "refs", &v1);
	kvar_int_set (buf, v1, gaux->refs);
	kvar_map_done(buf, v0, i++);
	kvar_map_edit(buf, v0, i, "timer", &v1);
	kvar_uint_set(buf, v1, mcastd_timer_expires(&gaux->timer));
	kvar_map_done(buf, v0, i++);
	kvar_map_edit(buf, v0, i, "nq", &v1);
	kvar_uint_set(buf, v1, gaux->nq);
	kvar_map_done(buf, v0, i++);
	kvar_map_edit(buf, v0, i, "ctimer", &v1);
	kvar_uint_set(buf, v1, mcastd_timer_expires(&gaux->ctimer));
	kvar_map_done(buf, v0, i++);
	kvar_map_edit(buf, v0, i, "gqtimer", &v1);
	kvar_uint_set(buf, v1, mcastd_timer_expires(&gaux->gqtimer));
	kvar_map_done(buf, v0, i++);
	kvar_map_edit(buf, v0, i, "sqtimer", &v1);
	kvar_uint_set(buf, v1, mcastd_timer_expires(&gaux->sqtimer));
	kvar_map_done(buf, v0, i++);
	return 0;
}

/*
 * Put the auxiliary data of the snooper for a certain source in @a buf.
 *
 * @param[in] data: the auxiliary data. Pointer to mcastdsn_source.
 */
static int mcastdsn_source2var(struct kvarbuf *buf, struct kvar *var,
                               void *data)
{
	struct kvar *v0 = var, *v1;
	struct mcastdsn_source *saux = data;
	int i = 0;

	kvar_map_init(buf, v0, 3);
	kvar_map_edit(buf, v0, i, "refs", &v1);
	kvar_int_set (buf, v1, saux->refs);
	kvar_map_done(buf, v0, i++);
	kvar_map_edit(buf, v0, i, "timer", &v1);
	kvar_uint_set(buf, v1, mcastd_timer_expires(&saux->timer));
	kvar_map_done(buf, v0, i++);
	kvar_map_edit(buf, v0, i, "nq", &v1);
	kvar_uint_set(buf, v1, saux->nq);
	kvar_map_done(buf, v0, i);
	return 0;
}

/*
 * Put the auxiliary data of the snooper for a certain host in @a buf.
 *
 * @param[in] data: the auxiliary data. Pointer to mcastdsn_host.
 */
static int mcastdsn_host2var(struct kvarbuf *buf, struct kvar *var,
                             void *data)
{
	struct kvar *v0 = var, *v1;
	struct mcastdsn_host *haux = data;
	kvar_map_init(buf, v0, 1);
	kvar_map_edit(buf, v0, 0, "timer", &v1);
	kvar_uint_set(buf, v1, mcastd_timer_expires(&haux->timer));
	kvar_map_done(buf, v0, 0);
	return 0;
}

/**
 * mcastdsn_check_subscriptions() - Notify US of IGMP/MLD subscriptions.
 *
 * @ifname: name of mcastd interface, e.g. 'lan'
 *
 * Check if the mcastd interface with name given by 'ifname' has IMGP/MLD
 * subscriptions or not. Notify US if this changes.
 */
static void mcastdsn_check_subscriptions(const char *ifname)
{
	struct mdb_iface *iface = mdb_iface_find(ifname);
	struct mcastdsn_iface *iaux;
	struct mdb_group *grec;
	int present = 0; /* MC subscriptions present */

	if (!iface) {
		mcastdsn_amx_send_iface_bool_value(ifname, "SubscriptionsPresent", 0);
		return;
	}

	iaux = mdb_iface_auxdata(iface, MDB_REF_SNOOPER_PRIV);
	if (!iaux) {
		ERRMSG("%s: no iaux", ifname);
		return;
	}

	/**
	 * Instead of just checking if iface->records is empty or not, check if the
	 * group records have MDB_REF_SNOOPER_PRIV set. The presence of that flag
	 * indicates an IGMP/MLD subscription is present. Normally all group records
	 * have that flag set.
	 */
	list_for_each_entry(grec, &iface->records, head) {
		if (grec->refs & MDB_REFMASK_SNOOPER_PRIV) {
			present = 1;
			break;
		}
	}

	if (present != iaux->mc_subscriptions_present) {
		DEBMSG("%s: mc_subscriptions_present: %d -> %d", ifname,
				iaux->mc_subscriptions_present, present);
		mcastdsn_amx_send_iface_bool_value(iface->name, "SubscriptionsPresent", present);
		iaux->mc_subscriptions_present = present;
	}
}

/**
 * mdb_commit_wrapper() - wrapper function around mdb_commit().
 *
 * First call mdb_commit(). Then call functions which should be run for the
 * snooper after mdb_commit() has finished. mdb_commit() might e.g. delete
 * 'group'. And then the snooper might have to do certain things. For now call
 * the function mcastdsn_check_subscriptions().
 */
static void mdb_commit_wrapper(struct mdb_group *group, struct sk_buff *skb)
{
	/**
	 * mdb_commit() might delete 'group', and even 'group->iface'. Therefore
	 * store the name of group->iface so we can check afterwards if it still
	 * exists.
	 */
	struct mdb_iface *iface = group->iface;
	char iface_name[MDB_IFACE_NAMESIZE];
	strncpy(iface_name, iface->name, MDB_IFACE_NAMESIZE);
	
	mdb_commit(group, skb);

	mcastdsn_check_subscriptions(iface_name);
}

/*
 * Cancel host timer and clear MDB_REF_SNOOPER_IN.
 */
static void mcastdsn_host_expire(struct mdb_host *hrec)
{
	struct mcastdsn_host *haux;
	if (!(hrec->refs & MDB_REFMASK_SNOOPER_IN))
		return;
	MCASTD_ASSERT(hrec->refs & MDB_REFMASK_SNOOPER_PRIV);
	haux = mdb_host_auxdata(hrec, MDB_REF_SNOOPER_PRIV);
	mcastd_timer_cancel(&haux->timer);
	mdb_host_clrref(hrec, MDB_REF_SNOOPER_IN);
}

/*
 * Call mcastdsn_host_expire() and commit the group.
 */
static void mcastdsn_host_timer_cb(void *data)
{
	struct mdb_host *hrec = data;
	MCASTD_ASSERT(hrec->refs & MDB_REFMASK_SNOOPER_IN);
	DEBMSG("host %s expires", mdb_address_to_string(&hrec->address));
	mcastdsn_host_expire(hrec);
	mdb_commit_wrapper(hrec->source->group, NULL);
}

/*
 * Clear MDB_REF_SNOOPER_PRIV in host record, commit group.
 */
static void mcastdsn_host_timer_destroy(void *data)
{
	struct mdb_host *hrec = data;
	MCASTD_ASSERT(hrec->refs & MDB_REFMASK_SNOOPER_PRIV);
	mdb_host_clrref(hrec, MDB_REF_SNOOPER_PRIV);
	mdb_commit_wrapper(hrec->source->group, NULL);
}

/*
 * Declare mdb_source expired.
 *
 * @attention function expects MDB_REFMASK_SNOOPER_IN.
 *
 * The function:
 * - returns if MDB_REFMASK_SNOOPER_IN is not set.
 * - cancels the source timer.
 * - cancels all host timers for all hosts of this source and clears their
 *   MDB_REF_SNOOPER_IN flag.
 * - clears MDB_REF_SNOOPER_IN flag of this source.
 * - sets MDB_REF_SNOOPER_EX in the source record if it's set for its group.
 * - checks if group has other sources with MDB_REFMASK_SNOOPER_IN set.
 *   If not, it clears the MDB_REF_SNOOPER_IN flag of the group.
 *
 * So analogously to mcastdsn_host_expire(..) this function clears the
 * MDB_REF_SNOOPER_IN flag.
 */
static void mcastdsn_source_expire(struct mdb_source *srec)
{
	struct mdb_group *grec = srec->group;
	struct mdb_host *hrec;
	struct mcastdsn_source *saux;
	if (!(srec->refs & MDB_REFMASK_SNOOPER_IN))
		return;
	MCASTD_ASSERT(srec->refs & MDB_REFMASK_SNOOPER_PRIV);
	saux = mdb_source_auxdata(srec, MDB_REF_SNOOPER_PRIV);
	mcastd_timer_cancel(&saux->timer);
	list_for_each_entry(hrec, &srec->hosts, head)
		mcastdsn_host_expire(hrec);
	mdb_source_clrref(srec, MDB_REF_SNOOPER_IN);
	if (srec->group->refs & MDB_REFMASK_SNOOPER_EX) {
		mdb_source_setref(srec, MDB_REF_SNOOPER_EX);
		return;
	}
	list_for_each_entry(srec, &grec->sources, head)
		if (srec->refs & MDB_REFMASK_SNOOPER_IN)
			break;
	if (&srec->head != &grec->sources)
		return;
	mdb_group_clrref(grec, MDB_REF_SNOOPER_IN);
}

/*
 * Call mcastdsn_source_expire(..) and commit the group.
 *
 * @attention function expects MDB_REFMASK_SNOOPER_IN.
 */
static void mcastdsn_source_timer_cb(void *data)
{
	struct mdb_source *srec = data;
	MCASTD_ASSERT(srec->refs & MDB_REFMASK_SNOOPER_IN);
	DEBMSG("source %s expires", mdb_address_to_string(&srec->address));
	mcastdsn_source_expire(srec);
	mdb_commit_wrapper(srec->group, NULL);
}

/*
 * Destroy source timer.
 *
 * The function:
 * - clears MCASTDSN_REF_TIMER in the auxiliary data of this source record.
 * - if no other flags are set in the auxiliary data of this source record,
 *   it clears MDB_REF_SNOOPER_PRIV and commits the parent group.
 */
static void mcastdsn_source_timer_destroy(void *data)
{
	struct mdb_source *srec = data;
	struct mcastdsn_source *saux;
	MCASTD_ASSERT(srec->refs & MDB_REFMASK_SNOOPER_PRIV);
	saux = mdb_source_auxdata(srec, MDB_REF_SNOOPER_PRIV);
	saux->refs &= ~(1 << MCASTDSN_REF_TIMER);
	if (!saux->refs) {
		mdb_source_clrref(srec, MDB_REF_SNOOPER_PRIV);
		mdb_commit_wrapper(srec->group, NULL);
	}
}

/*
 * Declare group expired.
 *
 * @attention function expects MDB_REFMASK_SNOOPER_EX.
 *
 * The function:
 * - returns if MDB_REFMASK_SNOOPER_EX is not set.
 * - cancels the group timer.
 * - if the 1st source record of the group is for the any address (all zeroes),
 *   - call mcastdsn_host_expire() for all hosts of the any source record and
 *   - clear MDB_REF_SNOOPER_IN of the any source record.
 * - clears MDB_REF_SNOOPER_EX of this group record.
 * - clears MDB_REF_SNOOPER_EX for all its source records.
 * - sets MDB_REF_SNOOPER_IN if any of its source records has this flag set.
 */
static void mcastdsn_group_expire(struct mdb_group *grec)
{
	struct mdb_source *srec;
	struct mdb_host *hrec;
	struct mcastdsn_group *gaux;
	gaux = mdb_group_auxdata(grec, MDB_REF_SNOOPER_PRIV);
	if (!(grec->refs & MDB_REFMASK_SNOOPER_EX))
		return;
	MCASTD_ASSERT(grec->refs & MDB_REFMASK_SNOOPER_PRIV);
	mcastd_timer_cancel(&gaux->timer);
	srec = mdb_group_get_any_source(grec);
	if (srec) {
		list_for_each_entry(hrec, &srec->hosts, head)
			mcastdsn_host_expire(hrec);
		mdb_source_clrref(srec, MDB_REF_SNOOPER_IN);
	}
	mdb_group_clrref(grec, MDB_REF_SNOOPER_EX);
	list_for_each_entry(srec, &grec->sources, head) {
		mdb_source_clrref(srec, MDB_REF_SNOOPER_EX);
		if (srec->refs & MDB_REFMASK_SNOOPER_IN)
			mdb_group_setref(grec, MDB_REF_SNOOPER_IN);
	}
}

/*
 * Call mcastdsn_group_expire(..) and commit the group.
 *
 * @attention function expects MDB_REFMASK_SNOOPER_EX.
 */
static void mcastdsn_group_timer_cb(void *data)
{
	struct mdb_group *grec = data;
	MCASTD_ASSERT(grec->refs & MDB_REFMASK_SNOOPER_EX);
	DEBMSG("group %s expires", mdb_address_to_string(&grec->address));
	mcastdsn_group_expire(grec);
	mdb_commit_wrapper(grec, NULL);
}

/*
 * Handle timeout of timer for sending group-specific queries.
 *
 * Possibly send a group-specific query and reschedule to possibly send again.
 *
 * If nq != 0, call mcastdsn_group_query(..), decrement nq and reschedule timer.
 *
 * nq stands for # of queries left.
 *
 * If function reschedules timer, it reschedules it with timeout == mcastdsn_LMQI
 * which is default 1 second.
 */
static void mcastdsn_group_gqtimer_cb(void *data)
{
	struct mdb_group *grec = data;
	struct mcastdsn_group *gaux;
	gaux = mdb_group_auxdata(grec, MDB_REF_SNOOPER_PRIV);
	DEBMSG("gqtimer expired nq[%d]", gaux->nq);
	if (gaux->nq) {
		DEBMSG("Send Q(%s)%s", mdb_address_to_string(&grec->address),
		       gaux->sflag ? "[S]" : "");
		mcastdsn_group_query(grec->iface, &grec->address,
		                     gaux->sflag);
		gaux->sflag = 0;
		gaux->nq--;
		DEBMSG("schedule gqtimer %lu ms", mcastdsn_LMQI);
		/* Reschedule timer. If timer expires in mcastdsn_LMQI ms, this
		 * function is called again. If gaux->nq is 0, the function will
		 * not reschedule the timer. Then mcastdsn_group_gqtimer_destroy()
		 * will be called. */
		mcastd_timer_schedule(&gaux->gqtimer, mcastdsn_LMQI);
	}
}

/*
 * Send 0, 1 or more group-and-source-specific queries.
 *
 * The function iterates over all source records of the group. It sends a query
 * if some conditions are ok:
 * - MDB_REFMASK_SNOOPER_PRIV must be set for the source record
 * - nq must be != 0. If not, the function clears MCASTDSN_REF_SQTIMER in the
 *   auxiliary data of the snooper in the source record. If all flags are
 *   cleared, it clears MDB_REF_SNOOPER_PRIV of the source record.
 * - @a sflag in source record must match value of input parameter @a sflag.
 *
 * If the function sends 1 or more group-and-source-specific queries:
 * - it first calls mcastdsn_source_query_init(..)
 * - it calls mcastdsn_source_query_done() just before returning.
 *
 * @return 1 if it sent at least 1 query.
 * @return 0 if it didn't send any queries.
 */
static int mcastdsn_source_sendq(struct mdb_group *grec, int sflag)
{
	struct mdb_source *srec;
	struct mcastdsn_source *saux;
	int again = 0;
	list_for_each_entry(srec, &grec->sources, head) {
		if (!(srec->refs & MDB_REFMASK_SNOOPER_PRIV))
			continue;
		saux = mdb_source_auxdata(srec, MDB_REF_SNOOPER_PRIV);
		if (!saux->nq) {
			saux->refs &= ~(1 << MCASTDSN_REF_SQTIMER);
			if (!saux->refs)
				mdb_source_clrref(srec, MDB_REF_SNOOPER_PRIV);
			continue;
		}
		if (saux->sflag != sflag)
			continue;
		DEBMSG("Send Q(%s, %s)%s",
		       mdb_address_to_string(&grec->address),
		       mdb_address_to_string(&srec->address),
		       sflag ? "[S]" : "");
		if (!again)
			mcastdsn_source_query_init(grec->iface, &grec->address,
			                           sflag);
		mcastdsn_source_query_push(&srec->address);
		saux->nq--;
		saux->sflag = 0;
		again = 1;
	}
	if (again)
		mcastdsn_source_query_done();
	return again;
}

/*
 * Handle timeout of timer for sending group-and-source-specific queries.
 *
 * The function calls mcastdsn_source_sendq(..) twice:
 * - 1st time with sflag = 0
 * - 2nd time with sflag = 1
 *
 * If one of those calls triggers the sending of a group-and-source-specific
 * query, the function reschedules the timer with timeout mcastdsn_LMQI.
 */
static void mcastdsn_group_sqtimer_cb(void *data)
{
	struct mdb_group *grec = data;
	struct mcastdsn_group *gaux;
	int again = 0;
	gaux = mdb_group_auxdata(grec, MDB_REF_SNOOPER_PRIV);
	again |= mcastdsn_source_sendq(grec, 0);
	again |= mcastdsn_source_sendq(grec, 1);
	if (again)
		mcastd_timer_schedule(&gaux->sqtimer, mcastdsn_LMQI);
}

/*
 * Clear snooper flag @a ref and release group if no snooper flags are set.
 *
 * The function first clears the snooper flag @a ref in the auxiliary data of
 * this group record. Then the behavior depends on whether the snooper still
 * has refs on the group record or not:
 *
 * - If the snooper doesn't have any refs anymore on the group, it clears
 *   MDB_REF_SNOOPER_PRIV and commits the group. This triggers the deletion of
 *   the group if there are no other refs on the group (which is normally the
 *   case).
 * - If the only ref the snooper still has on the group is ctimer, the function
 *   cancels the ctimer because it doesn't make sense to only keep the ctimer
 *   if the snooper no longer uses the group for anything else. The function
 *   calls mcastd_timer_cancel() for the ctimer. This will trigger
 *   mcastdsn_group_ctimer_destroy(), which triggers this mcastdsn_group_release()
 *   again, leading to the deletion of the group.
 */
static void mcastdsn_group_release(struct mdb_group *grec, enum mcastdsn_ref ref)
{
	struct mcastdsn_group *gaux;
	MCASTD_ASSERT(grec->refs & MDB_REFMASK_SNOOPER_PRIV);
	gaux = mdb_group_auxdata(grec, MDB_REF_SNOOPER_PRIV);
	gaux->refs &= ~(1 << ref);

	if (!gaux->refs) {
		mdb_group_clrref(grec, MDB_REF_SNOOPER_PRIV);
		mdb_commit_wrapper(grec, NULL);
	}
	else if (gaux->refs == (1 << MCASTDSN_REF_CTIMER)) {
		mcastd_timer_cancel(&gaux->ctimer);
	}
}

static void mcastdsn_group_timer_destroy(void *data)
{
	mcastdsn_group_release(data, MCASTDSN_REF_TIMER);
}

static void mcastdsn_group_gqtimer_destroy(void *data)
{
	mcastdsn_group_release(data, MCASTDSN_REF_GQTIMER);
}

static void mcastdsn_group_sqtimer_destroy(void *data)
{
	mcastdsn_group_release(data, MCASTDSN_REF_SQTIMER);
}

static void mcastdsn_group_ctimer_destroy(void *data)
{
	mcastdsn_group_release(data, MCASTDSN_REF_CTIMER);
}

/*
 * Send a general query for the interface and reschedule the timer.
 *
 * If sqc != 0, decrease it.
 * If sqc != 0, reschedule timer with timeout mcastdsn_SQI, else use mcastdsn_QI
 * as timeout.
 */
static void mcastdsn_iface_qtimer_cb(void *data)
{
	struct mdb_iface *irec = data;
	struct mcastdsn_iface *iaux = mdb_iface_auxdata(irec, MDB_REF_SNOOPER_PRIV);
	MCASTD_ASSERT(irec->refs & MDB_REFMASK_SNOOPER_PRIV);
	MCASTD_ASSERT(irec->ifindex);
	MCASTD_ASSERT(iaux->querier_enabled);
	DEBMSG("%s: send Q()", irec->name);
	mcastdsn_iface_query(irec);
	if (iaux->sqc)
		iaux->sqc--;
	DEBMSG("%s: schedule iface_qtimer %lu ms", irec->name,
			iaux->sqc ? mcastdsn_SQI : mcastdsn_QI);
	mcastd_timer_schedule(&iaux->timer, iaux->sqc ? mcastdsn_SQI : mcastdsn_QI);
}

/*
 * Clear MDB_REF_SNOOPER_PRIV and destroy interface if no longer in use.
 */
static void mcastdsn_iface_qtimer_destroy(void *data)
{
	struct mdb_iface *irec = data;
	MCASTD_ASSERT(irec->refs & MDB_REFMASK_SNOOPER_PRIV);
	mdb_iface_clrref(irec, MDB_REF_SNOOPER_PRIV);
	mdb_iface_release(irec);
}

/*
 * Set/create host record for source @a e->srec and host address @a address.
 *
 * The function schedules the host timer with timeout @a expires ms. This is
 * typically the GMI (default: 260 s).
 *
 * The function:
 * - gets host record with address @a address in children of @a e->srec.
 * - if it does not exist, creates it and adds it to children of @a e->srec.
 * - sets MDB_REF_SNOOPER_IN in host record.
 * - if MDB_REF_SNOOPER_PRIV is not set, initializes host timer.
 * - sets MDB_REF_SNOOPER_PRIV in host record.
 * - (re)schedules the timer with timeout = @a expires ms.
 */
static void mcastdsn_host_set(struct mcastdsn_env *e, struct mdb_address *address,
                              unsigned long expires)
{
	e->hrec = mdb_host_find(e->srec, address);
	if (!e->hrec) {
		struct mdb_host * hrec_any = NULL;
		struct mdb_source *srec_any;
		e->hrec = mdb_host_create(e->srec, address);

		/*
		 * Logic tells us we need to create a new host record for this source,
		 * but wait ... maybe we are actually adding/copying a record from the ANY source to another source entry?
		 * if so copy the port entry from the any src host record ... else it will remain 0
		 *
		 * If e->src points to the ANY source, srec_any will point to the same
		 * record as e->srec, and hrec_any will point to the same record as
		 * e->hrec. Only copy the port if the new mdb_host is different from the
		 * existing hrec_any.
		 */
		srec_any = mdb_group_get_any_source(e->grec);
		if (srec_any) {
			hrec_any = mdb_host_find(srec_any, address);
		}
		if (e->hrec) {
			if (hrec_any && (hrec_any != e->hrec)) {
				e->hrec->port = hrec_any->port;
			}
			else if (e->bridge_port && mdb_address_equals(address, &(e->haddr))) {
				/* The IP addres in the new mdb_host is equal to the IP address
				 * of the incoming IGMP/MLD report. Copy port on which IGMP/MLD
				 * report arrived to host record. */
				e->hrec->port = e->bridge_port;
			}
		}
	}
	if (!e->hrec)
		return;
	mdb_host_setref(e->hrec, MDB_REF_SNOOPER_IN);

	e->haux = mdb_host_auxdata(e->hrec, MDB_REF_SNOOPER_PRIV);
	if (!(e->hrec->refs & MDB_REFMASK_SNOOPER_PRIV)) {
		mcastd_timer_init(&e->haux->timer, mcastdsn_host_timer_cb,
		                  mcastdsn_host_timer_destroy, e->hrec, &mdb_mutex);
	}
	mdb_host_setref(e->hrec, MDB_REF_SNOOPER_PRIV);
	mcastd_timer_schedule(&e->haux->timer, expires);
}

/*
 * Create group record for interface @a e->irec and address @a e->gaddr if it
 * does not yet exist.
 *
 * If MDB_REFMASK_SNOOPER_PRIV is not set yet, initialize all group timers.
 *
 * @return 0 on success
 * @return -1 on error
 */
static int mcastdsn_group_require(struct mcastdsn_env *e)
{
	if (!e->grec)
		e->grec = mdb_group_create(e->irec, &e->gaddr);
	if (!e->grec)
		return -1;
	if (!e->gaux)
		e->gaux = mdb_group_auxdata(e->grec, MDB_REF_SNOOPER_PRIV);
	if (e->grec->refs & MDB_REFMASK_SNOOPER_PRIV)
		return 0;
	mcastd_timer_init(&e->gaux->timer, mcastdsn_group_timer_cb,
	                  mcastdsn_group_timer_destroy, e->grec, &mdb_mutex);
	mcastd_timer_init(&e->gaux->gqtimer, mcastdsn_group_gqtimer_cb,
	                  mcastdsn_group_gqtimer_destroy, e->grec, &mdb_mutex);
	mcastd_timer_init(&e->gaux->sqtimer, mcastdsn_group_sqtimer_cb,
	                  mcastdsn_group_sqtimer_destroy, e->grec, &mdb_mutex);
	mcastd_timer_init(&e->gaux->ctimer, NULL,
	                  mcastdsn_group_ctimer_destroy, e->grec, &mdb_mutex);
	return 0;
}

/*
 * Create parent group record, create source record, set up timers and hosts.
 *
 * - Create parent group record if it does not yet exist.
 * - Create source record if it does not yet exist.
 * - Initialize source timer if MDB_REFMASK_SNOOPER_PRIV is not set.
 * - Set MDB_REF_SNOOPER_IN on parent group if there's no IN or EX set yet.
 * - If A_IN is set, clear SNOOPER_EX and set SNOOPER_IN on source record.
 * - If A_EX is set, set SNOOPER_EX on source record.
 * - If A_EX is set, return. So then the function skips host inclusion and
 *   timer setup. Note that source records in EX mode have no host records.
 * - (Re-)schedule source timer (typically with timeout = GMI = default 260 s).
 * - If A_HOSTINV is not set, create host record.
 * - If A_HOSTINV or A_HOSTALL is set, find ANY source in parent group record.
 *   If it exists, in essence re-create all host records below it under the new
 *   source record (except the host record with the address @a e->haddr if
 *   A_HOSTINV is set).
 */
static void mcastdsn_source_set(struct mcastdsn_env *e)
{
	struct mdb_source *srec;
	struct mdb_host *hrec;
	struct mcastdsn_host *haux;
	unsigned long expires = mcastdsn_GMI;

	if (mcastdsn_group_require(e))
		return;

	if (!e->srec)
		e->srec = mdb_source_create(e->grec, &e->saddr);
	if (!e->srec)
		return;
	e->saux = mdb_source_auxdata(e->srec, MDB_REF_SNOOPER_PRIV);
	if (!(e->srec->refs & MDB_REFMASK_SNOOPER_PRIV)) {
		mcastd_timer_init(&e->saux->timer, mcastdsn_source_timer_cb,
		                  mcastdsn_source_timer_destroy, e->srec,
		                  &mdb_mutex);
	}
	if (!(e->grec->refs & MDB_REFMASK_SNOOPER)) // not in or ex yet -> in!
		mdb_group_setref(e->grec, MDB_REF_SNOOPER_IN);

	if (e->actions & A_IN) {
		mdb_source_clrref(e->srec, MDB_REF_SNOOPER_EX);
		mdb_source_setref(e->srec, MDB_REF_SNOOPER_IN);
	} else if (e->actions & A_EX) {
		mdb_source_setref(e->srec, MDB_REF_SNOOPER_EX);
		return; // skip host inclusion / timer setup
	}

	if (e->actions & A_GRPTIMER)
		expires = mcastd_timer_expires(&e->gaux->timer);

	mdb_source_setref(e->srec, MDB_REF_SNOOPER_PRIV);
	e->saux->refs |= 1 << MCASTDSN_REF_TIMER;
	mcastd_timer_schedule(&e->saux->timer, expires);

	if (!(e->actions & A_HOSTINV))
		mcastdsn_host_set(e, &e->haddr, expires);

	do if (e->actions & (A_HOSTINV | A_HOSTALL)) {
		srec = mdb_group_get_any_source(e->grec);
		if (!srec || !(srec->refs & MDB_REFMASK_SNOOPER))
			break;
		list_for_each_entry(hrec, &srec->hosts, head) {
			if (!(hrec->refs & MDB_REFMASK_SNOOPER))
				continue;
			if ((e->actions & A_HOSTINV) && mdb_address_equals(
					&hrec->address, &e->haddr))
				continue;
			haux = mdb_host_auxdata(hrec, MDB_REF_SNOOPER_PRIV);
			mcastdsn_host_set(e, &hrec->address,
			                  mcastd_timer_expires(&haux->timer));
		}
		e->hrec = NULL;
		e->haux = NULL;
	} while (0);
}

/*
 * Snooper has reason to believe a source is no longer of interest.
 *
 * Snooper does not immediately remove the source. It updates timers to figure
 * out in the next few seconds whether indeed the source is no longer of
 * interest. Then it can remove the source.
 *
 * The function:
 * - reschedules source timer with timeout = mcastdsn_LMQT (default: 2 s)
 *   (if the remaining timeout is larger).
 * - calls mcastdsn_host_expire() (for the host which sent message indicating
 *   the source is no longer of interest).
 * - searches for hosts with MDB_REFMASK_SNOOPER set in the source. If there
 *   are no such hosts and if fast_leave_enabled is set for the parent
 *   interface, call mcastdsn_source_expire() already now.
 * - schedules mcastdsn_LMQC (default: 2) group-and-source-specific queries if
 *   querier_enabled is set.
 *
 * The q in this function name stands for query.
 */
static void mcastdsn_source_q(struct mcastdsn_env *e)
{
	struct mdb_host *hrec;
	if (!e->srec || !e->grec)
		return;
	DEBMSG("grec: %s", mdb_address_to_string(&(e->gaddr)));
	MCASTD_ASSERT(e->srec->refs & MDB_REFMASK_SNOOPER_IN);
	if (!e->saux)
		e->saux = mdb_source_auxdata(e->srec, MDB_REF_SNOOPER_PRIV);
	if (e->iaux->querier_enabled)
		e->saux->nq = mcastdsn_LMQC;
	if (e->saux->nq) {
		/* If nq != 0, mcastd sends nq Group-and-Source-Specific Queries.
		 * mcastd should receive all replies within mcastdsn_LMQT ms. */
		if (mcastd_timer_expires(&e->saux->timer) > mcastdsn_LMQT) {
			e->saux->sflag = 1;
			DEBMSG("grec: %s: schedule saux->timer in %d ms",
			       mdb_address_to_string(&(e->gaddr)), mcastdsn_LMQT);
			mcastd_timer_schedule(&e->saux->timer, mcastdsn_LMQT);
		}
	}
	if (!e->gaux)
		e->gaux = mdb_group_auxdata(e->grec, MDB_REF_SNOOPER_PRIV);
	if (!e->hrec)
		e->hrec = mdb_host_find(e->srec, &e->haddr);
	if (e->hrec)
		mcastdsn_host_expire(e->hrec);
	list_for_each_entry(hrec, &e->srec->hosts, head)
		if (hrec->refs & MDB_REFMASK_SNOOPER)
			break;
	if (&hrec->head == &e->srec->hosts &&
			e->iaux->fast_leave_enabled) {
		/* mcastdsn_source_expire() normally cancels timer which was set
		 * to expire in mcastdsn_LMQT ms a few lines above */
		DEBMSG("grec: %s: call mcastdsn_source_expire()",
		       mdb_address_to_string(&(e->gaddr)));
		mcastdsn_source_expire(e->srec);
	}
	if (e->saux->nq) {
		mdb_source_setref(e->srec, MDB_REF_SNOOPER_PRIV);
		e->saux->refs |= 1 << MCASTDSN_REF_SQTIMER;
		mdb_group_setref(e->grec, MDB_REF_SNOOPER_PRIV);
		e->gaux->refs |= 1 << MCASTDSN_REF_SQTIMER;
		DEBMSG("schedule sqtimer immediately");
		mcastd_timer_schedule(&e->gaux->sqtimer, 0);
	}
}

/*
 * Create group with ref SNOOPER_EX, create any source, setup group timer,
 * create host.
 *
 * Because this function creates a group with ref MDB_REF_SNOOPER_EX, this
 * function is typically called when the snooper processes an IGMP/MLD packet
 * of type IS_EX or TO_EX.
 *
 * The function:
 * - creates the group record (if it does not yet exist)
 * - creates the ANY source record and adds it to the head of the children
 *   of the group (if the ANY source record does not yet exist)
 * - clears MDB_REF_SNOOPER_IN on the group.
 * - sets MDB_REF_SNOOPER_EX and MDB_REF_SNOOPER_PRIV on the group.
 * - sets MDB_REF_SNOOPER_IN on the ANY source.
 * - (re-)schedules the group timer with timeout mcastdsn_GMI (default: 260 s)
 * - calls mcastdsn_host_set(..) to create host under the ANY source record.
 */
static void mcastdsn_group_set(struct mcastdsn_env *e)
{
	if (mcastdsn_group_require(e))
		return;

	e->srec = mdb_group_get_any_source(e->grec);
	if (!e->srec) {
		e->srec = mdb_group_create_any_source(e->grec);
	}
	if (!e->srec)
		return;

	mdb_group_clrref(e->grec, MDB_REF_SNOOPER_IN);
	mdb_group_setref(e->grec, MDB_REF_SNOOPER_EX);
	mdb_source_setref(e->srec, MDB_REF_SNOOPER_IN);

	mdb_group_setref(e->grec, MDB_REF_SNOOPER_PRIV);
	e->gaux->refs |= 1 << MCASTDSN_REF_TIMER;
	mcastd_timer_schedule(&e->gaux->timer, mcastdsn_GMI);

	mcastdsn_host_set(e, &e->haddr, mcastdsn_GMI);
}

/*
 * Snooper has reason to believe a group is no longer of interest.
 *
 * The snooper only calls this function on a group with MDB_REFMASK_SNOOPER_EX
 * set and when it receives an IGMP/MLD packet with type = TO_IN.
 *
 * Snooper does not immediately remove the group. It updates timers to figure
 * out in the next few seconds whether indeed the group is no longer of
 * interest. Then it can remove the group.
 *
 * The function:
 * - reschedules group timer with timeout = mcastdsn_LMQT (default: 2 s)
 *   (if the remaining timeout is larger).
 * - gets the any soure record, and looks for the host record with address
 *   @a e->haddr under that soure record. If the host record exists and has
 *   MDB_REFMASK_SNOOPER set, it calls mcastdsn_host_expire(..).
 * - searches for hosts with MDB_REFMASK_SNOOPER set under the any source. If
 *   there are no such hosts and if fast_leave_enabled is set for the parent
 *   interface, call mcastdsn_group_expire() already now.
 * - schedules mcastdsn_LMQC (default: 2) group-specific queries if
 *   querier_enabled is set.
 */
static void mcastdsn_group_q(struct mcastdsn_env *e)
{
	struct mdb_host *hrec;
	if (!e->grec)
		return;
	DEBMSG("grec: %s", mdb_address_to_string(&(e->gaddr)));
	MCASTD_ASSERT(e->grec->refs & MDB_REFMASK_SNOOPER_EX);
	if (!e->gaux)
		e->gaux = mdb_group_auxdata(e->grec, MDB_REF_SNOOPER_PRIV);
	if (e->iaux->querier_enabled)
		e->gaux->nq = mcastdsn_LMQC;
	if (e->gaux->nq) {
		/* If nq != 0, mcastd sends nq Group-Specific Queries.
		 * mcastd should receive all replies within mcastdsn_LMQT ms. */
		if (mcastd_timer_expires(&e->gaux->timer) > mcastdsn_LMQT) {
			e->gaux->sflag = 1;
			DEBMSG("grec: %s: schedule gaux->timer in %d ms",
			       mdb_address_to_string(&(e->gaddr)), mcastdsn_LMQT);
			mcastd_timer_schedule(&e->gaux->timer, mcastdsn_LMQT);
		}
	}
	if (!e->srec)
		e->srec = mdb_group_get_any_source(e->grec);
	if (!e->hrec)
		e->hrec = mdb_host_find(e->srec, &e->haddr);
	if (e->hrec && !(e->hrec->refs & MDB_REFMASK_SNOOPER))
		e->hrec = NULL;
	if (e->hrec)
		mcastdsn_host_expire(e->hrec);
	if (e->srec)
		list_for_each_entry(hrec, &e->srec->hosts, head)
			if (hrec->refs & MDB_REFMASK_SNOOPER)
				break;
	if ((!e->srec || &hrec->head == &e->srec->hosts) &&
			e->iaux->fast_leave_enabled) {
		/* mcastdsn_group_expire() normally cancels timer which was set
		 * to expire in mcastdsn_LMQT ms a few lines above */
		DEBMSG("grec: %s: call mcastdsn_group_expire() [e->srec=%d]",
			mdb_address_to_string(&(e->gaddr)), e->srec ? 1 : 0);
		mcastdsn_group_expire(e->grec);
	}
	if (e->gaux->nq) {
		mdb_group_setref(e->grec, MDB_REF_SNOOPER_PRIV);
		e->gaux->refs |= 1 << MCASTDSN_REF_GQTIMER;
		DEBMSG("schedule gqtimer immediately");
		mcastd_timer_schedule(&e->gaux->gqtimer, 0);
	}
}

#ifdef CONFIG_MCASTD_CORE_DEBUG
static const char *mcastdsn_f2str(int filter) {
	switch (filter) {
	case F_IN: return "F_IN";
	case F_EX: return "F_EX";
	case F_NA: return "F_NA";
	case F_DC: return "F_DC";
	default:   return "F_??";
	}
}

static const char *mcastdsn_r2str(int report) {
	switch (report) {
	case R_N:  return "R_N ";
	case R_Y:  return "R_Y ";
	case R_DC: return "R_DC";
	default:   return "R_??";
	}
}
#endif

/*
 * Run 0, 1 or more action functions to update the mdb.
 *
 * @param[in] matrix : list of mcastdsn_matrix elements corresponding to one
 *    of the IGMP/MLD report types (IS_IN, IS_EX, ...). E.g. if this function
 *    is triggered by an IGMPv3 message of type IS_IN, then all elements in
 *    list @a matrix correspond to type IS_IN. The function will iterate
 *    through this list looking for a match. If no match is found, it stops
 *    at the element with: gfilter = F_DC and sfilter = F_DC and
 *    sreport = R_DC. Therefore, the last (or only) element in the list must
 *    have those values. The field 'actions' of that entry can be 0 (=no
 *    actions) or indicate an action.
 * @param[in] match : indicates how function must find a match in @a matrix.
 * @param[in] action_fn : mapping of actions (A_IN, A_EX, ..) to functions.
 *    The caller passes a different value depending on whether it calls this
 *    function because of a source record or a group record.
 */
static void mcastdsn_matrix_run(struct mcastdsn_env *e,
                                struct mcastdsn_matrix *matrix,
                                struct mcastdsn_matrix *match,
                                mcastdsn_action_fn *action_fn)
{
	struct mcastdsn_matrix *m;
	int i;
	for (m = matrix;; m++) {
		if (m->gfilter != F_DC && match->gfilter != m->gfilter)
			continue;
		if (m->sfilter != F_DC && match->sfilter != m->sfilter)
			continue;
		if (m->sreport != R_DC && match->sreport != m->sreport)
			continue;
		break;
	}
	e->actions = m->actions;
	e->saux = NULL;
	e->hrec = NULL;
	e->haux = NULL;
	DEBMSG("    %s %s %s ->%s%s%s%s%s%s",
	       mcastdsn_f2str(match->gfilter),
	       mcastdsn_f2str(match->sfilter),
	       mcastdsn_r2str(match->sreport),
	       (e->actions & A_IN      )?" A_IN"      :"",
	       (e->actions & A_EX      )?" A_EX"      :"",
	       (e->actions & A_Q       )?" A_Q"       :"",
	       (e->actions & A_GRPTIMER)?" A_GRPTIMER":"",
	       (e->actions & A_HOSTINV )?" A_HOSTINV" :"",
	       (e->actions & A_HOSTALL )?" A_HOSTALL" :"");
	for (i = 0; i < MCASTDSN_ACTION_COUNT; i++) {
		if (!(e->actions & 1 << i))
			continue;
		if (action_fn[i])
			action_fn[i](e);
	}
}

/*
 * Actions to create/change/delete group records.
 *
 * If the snooper receives an IGMP/MLD packet of type IS_EX or TO_EX, it will
 * select the action A_EX. The mcastdsn_group_action_fn array below indicates
 * that the snooper then calls the function mcastdsn_group_set.
 *
 * If the snooper receives a packet of type TO_IN, it only has an action if the
 * group mentioned in the packet has the flag MDB_REFMASK_SNOOPER_EX set. Then
 * it selects the action A_Q. The mcastdsn_group_action_fn array below indicates
 * that the snooper then calls the function mcastdsn_group_q.
 */
static struct mcastdsn_matrix mcastdsn_group_matrix[MCASTDSN_RECORD_COUNT][2] = {
	{    // IS_IN ->
		{ F_DC, F_DC, R_DC, 0            }, /* sentinel */
	}, { // IS_EX ->
		{ F_DC, F_DC, R_DC, A_EX         }, /* 1 and only element for IS_EX */
	}, { // TO_IN ->
		{ F_EX, F_DC, R_DC,         A_Q  },
		{ F_DC, F_DC, R_DC, 0            }, /* sentinel */
	}, { // TO_EX ->
		{ F_DC, F_DC, R_DC, A_EX         }, /* 1 and only element for TO_EX */
	}, { // ALLOW ->
		{ F_DC, F_DC, R_DC, 0            }, /* sentinel */
	}, { // BLOCK ->
		{ F_DC, F_DC, R_DC, 0            }, /* sentinel */
	}
};

static mcastdsn_action_fn mcastdsn_group_action_fn[MCASTDSN_ACTION_COUNT] = {
	[MCASTDSN_ACTION_EX] = mcastdsn_group_set,
	[MCASTDSN_ACTION_Q]  = mcastdsn_group_q,
};

static struct mcastdsn_matrix mcastdsn_source_matrix[MCASTDSN_RECORD_COUNT][5] = {
	{    // IS_IN ->
		{ F_DC, F_NA, R_Y , A_IN                     | A_HOSTALL },
		{ F_DC, F_DC, R_Y , A_IN                                 },
		{ F_DC, F_DC, R_DC, 0                                    }, /* sentinel */
	}, { // IS_EX ->
		{ F_IN, F_NA, R_Y , A_EX                                 },
		{ F_DC, F_DC, R_N , A_IN                                 },
		{ F_EX, F_NA, R_Y , A_IN                     | A_HOSTINV },
		{ F_DC, F_DC, R_DC, 0                                    }, /* sentinel */
	}, { // TO_IN ->
		{ F_DC, F_NA, R_Y , A_IN                     | A_HOSTALL },
		{ F_DC, F_DC, R_Y , A_IN                                 },
		{ F_DC, F_IN, R_N ,         A_Q                          },
		{ F_DC, F_DC, R_DC, 0                                    }, /* sentinel */
	}, { // TO_EX ->
		{ F_IN, F_NA, R_Y , A_EX                                 },
		{ F_DC, F_DC, R_N , A_IN                                 },
		{ F_EX, F_NA, R_Y , A_IN  | A_Q | A_GRPTIMER | A_HOSTINV },
		{ F_DC, F_IN, R_Y ,         A_Q                          },
		{ F_DC, F_DC, R_DC, 0                                    }, /* sentinel */
	}, { // ALLOW ->
		{ F_DC, F_NA, R_Y , A_IN                     | A_HOSTALL },
		{ F_DC, F_DC, R_Y , A_IN                                 },
		{ F_DC, F_DC, R_DC, 0                                    }, /* sentinel */
	}, { // BLOCK ->
		{ F_EX, F_NA, R_Y , A_IN  | A_Q | A_GRPTIMER | A_HOSTINV },
		{ F_DC, F_IN, R_Y ,         A_Q                          },
		{ F_DC, F_DC, R_DC, 0                                    }, /* sentinel */
	}
};

static mcastdsn_action_fn mcastdsn_source_action_fn[MCASTDSN_ACTION_COUNT] = {
	[MCASTDSN_ACTION_IN] = mcastdsn_source_set,
	[MCASTDSN_ACTION_EX] = mcastdsn_source_set,
	[MCASTDSN_ACTION_Q]  = mcastdsn_source_q,
};

/**
 * Get the ifindex of 'dev', or its bridge if 'dev' is a bridge port.
 *
 * Return: dev->ifindex if 'dev' is not a bridge port; the ifindex of the
 *         bridge to which 'dev' belongs if 'dev' is a bridge port.
 */
int mcastdsn_get_ifindex_of_bridge(struct net_device * dev)
{
	int ifindex = dev->ifindex;

	if (dev->priv_flags & IFF_BRIDGE_PORT) {
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(3,9,0))
		struct net_device *upper_dev;
		rcu_read_lock();
		upper_dev = netdev_master_upper_dev_get_rcu(dev);
		if (upper_dev) {
			ifindex = upper_dev->ifindex;
		}
		rcu_read_unlock();
#else
		if (dev->master)
			ifindex = dev->master->ifindex;
#endif
	}
	return ifindex;
}

/**
 * Commit 4d4fd36126d66d6091ca5aaabab262b5da3849c5 in the linux kernel with
 * subject "net: bridge: Publish bridge accessor functions" from 2018-04-29
 * adds br_fdb_find_port(). The commit follows v4.17-rc2 and precedes v4.18-rc1.
 * SaH must add the function itself to older kernels and define
 * SAH_KERNEL_SUPPORTS_BR_FDB_FIND_PORT to indicate it did. Then mcastd can
 * determine at compile time if the kernel supports br_fdb_find_port() or not.
 */
#if (LINUX_VERSION_CODE < KERNEL_VERSION(4,18,0))
#ifndef SAH_KERNEL_SUPPORTS_BR_FDB_FIND_PORT
#error "kernel does not support br_fdb_find_port()"
#endif
#endif

/**
 * Commit df8a39defad46b83694ea6dd868d332976d62cc0 in the linux kernel with
 * subject "net: rename vlan_tx_* helpers since "tx" is misleading there" from
 * 2015-01-13 adds skb_vlan_tag_present() and skb_vlan_tag_get_id(). The commit
 * follows v3.19-rc3 and precedes v4.0-rc1. Older kernels use other macro names
 * or do not have such macros. Let's do not take into account the VLAN ID in
 * the IGMP/MLD report for those older kernels.
 */
#if (LINUX_VERSION_CODE < KERNEL_VERSION(4,0,0))
#define skb_vlan_tag_present(skb) 0
#define skb_vlan_tag_get_id(skb) 0
#endif

/**
 * Get the ifindex of bridge port on which IGMP/MLD report arrived.
 *
 * @ifindex_bridge: ifindex of bridge on which IGMP/MLD report arrived
 * @eth_src_addr: Ethernet source address of IGMP/MLD report
 * @vlan_id: VLAN ID of IGMP/MLD report if VLAN tag is present
 *
 * The function calls br_fdb_find_port() to query the Forwarding Database (FD)
 * of the bridge on which the report arrived. The FD normally has an entry
 * indicating on which port of the bridge it learned the Ethernet source address
 * of the report.
 *
 * The function first passes 0 as value for the VLAN ID when calling the
 * kernel function to query the FDB: the IGMP/MLD report highly likely
 * arrived via an untagged (W)LAN port. If the kernel can't find a port
 * for the combination of @a eth_src_addr and VLAN ID = 0, the function
 * does a 2nd attempt, and it passess @a vlan_id as value for the VLAN ID.
 *
 * Return:
 * * ifindex of bridge port on which IGMP/MLD report arrived. This is > 0.
 * * 0 if FD doesn't return a result
 * * -1 upon error
 */
static int mcastdsn_get_incoming_bridge_port_core(int ifindex_bridge,
                                                  char eth_src_addr[ETH_ALEN], __u16 vlan_id)
{
	int result = 0;
	struct net_device *br_dev;
	struct net_device *port_dev;
	__u16 vlan = 0;

	br_dev = dev_get_by_index(&init_net, ifindex_bridge);
	if (br_dev) {
		rtnl_lock();
#ifdef SAH_KERNEL_SUPPORTS_BR_FDB_FIND_PORT_INCLUDE_EXPIRED
		port_dev = sah_br_fdb_find_port_include_expired(br_dev, eth_src_addr, vlan);
#else
		port_dev = br_fdb_find_port(br_dev, eth_src_addr, vlan);
#endif
		if (!port_dev && vlan_id) {
			vlan = vlan_id;
			port_dev = br_fdb_find_port(br_dev, eth_src_addr, vlan);
		}
		rtnl_unlock();
		dev_put(br_dev);
		br_dev = NULL;
		if (port_dev) {
			DEBMSG("port=%d [vlan=%d]", port_dev->ifindex, vlan);
			result = port_dev->ifindex;
		}
		else {
			// Caller logs msg at error or debug level
		}
	}
	else {
		ERRMSG("Failed to get net_device of bridge");
		result = -1;
	}
	return result;
}

static unsigned char f_lastSrcMacAddrSeen[ETH_ALEN] = { 0 };

#define BRIDGE_PORT_NOT_FOUND_LOG_MSG \
	"Failed to get incoming port for src_addr=%02X:%02X:%02X:%02X:%02X:%02X [vlan=%d]"

/**
 * Get the ifindex of bridge port on which IGMP/MLD report arrived.
 *
 * @ifindex_bridge: ifindex of bridge on which IGMP/MLD report arrived
 * @skb: the IGMP/MLD report
 *
 * This is a wrapper function around mcastdsn_get_incoming_bridge_port_core().
 *
 * Return:
 * * ifindex of bridge port on which IGMP/MLD report arrived. This is > 0.
 * * 0 if Linux can't find Ethernet source address in Forwarding Database.
 * * -1 upon error
 */
static int mcastdsn_get_incoming_bridge_port(int ifindex_bridge, struct sk_buff *skb)
{
	int bridge_port = 0;
	struct ethhdr *eh = NULL;
	__u16 vlan_id = 0;

	eh = eth_hdr(skb);
	if (skb_vlan_tag_present(skb)) {
		vlan_id = skb_vlan_tag_get_id(skb);
	}
	bridge_port = mcastdsn_get_incoming_bridge_port_core(ifindex_bridge,
	                                                     eh->h_source, vlan_id);

	if (0 == bridge_port) {
		bool bSameSrcMacAddress =
		   (memcmp(eh->h_source, f_lastSrcMacAddrSeen, ETH_ALEN) == 0) ? true : false;

		if (bSameSrcMacAddress) {
			/* Although HGW saw IGMP/MLD report with this source MAC address
			 * before, Linux didn't learn on which port of the bridge it
			 * arrived => log error.
			 */
			ERRMSG(BRIDGE_PORT_NOT_FOUND_LOG_MSG,
					eh->h_source[0], eh->h_source[1], eh->h_source[2],
					eh->h_source[3], eh->h_source[4], eh->h_source[5],
					vlan_id);
		}
		else {
			/* In case of tests which send an IGMP/MLD report without
			 * prior communication, it's possible that Linux did not
			 * learn the source MAC address => log msg at debug level.
			 */
			DBGMSG(BRIDGE_PORT_NOT_FOUND_LOG_MSG,
					eh->h_source[0], eh->h_source[1], eh->h_source[2],
					eh->h_source[3], eh->h_source[4], eh->h_source[5],
					vlan_id);
		}
		memcpy(f_lastSrcMacAddrSeen, eh->h_source, ETH_ALEN);
	}
	return bridge_port;
}

/**
 * mcastdsn_snoop - Process (one record of) the IGMP/MLD report.
 *
 * Notice:
 * - If the report is an IGMPv3 one, snooper_igmp.c calls this function one time
 *   for each Group Record in the IGMPv3 report.
 * - If the report is an MLDv2 one, snooper_mld.c calls this function one time
 *   for each Multicast Address Record in the MLDv2 report.
 *
 * @param[in] ifindex : netdev index of interface on which packet was received.
 *                      If the parameter 'skb' is not NULL, this field is equal
 *                      to skb->dev->ifindex.
 * @param[in] family : indicates if it's an IGMP (IPv4) or MLD (IPv6) report
 * @param[in] grp : the group address in the IGMP/MLD report
 * @param[in] type : IS_IN, IS_EX, TO_IN, ...
 * @param[in] nsrcs : number of unicast source addresses in the IGMP/MLD report
 * @param[in] srcs : the unicast source addresses
 * @param[in] hst : IPv4 or IPv6 source address of the IGMP/MLD report
 * @param[in] compat : set to 1 if it's an IGMPv2 or MLDv1 packet
 * @param[in] skb : the IGMP/MLD report. Is NULL when called from snooper_amx.c
 *                  for testing purposes.
 *
 * The function typically updates the mdb and notifies the subscribers of the
 * mdb if something changed in the mdb.
 */
void mcastdsn_snoop(int ifindex, __u8 family, __u8 *grp, __u8 type, __u16 nsrcs,
                    __u8 *srcs, __u8 *hst, int compat, struct sk_buff *skb)
{
	struct mcastdsn_env env;
	struct mcastdsn_matrix match;
	size_t addrsize;
	struct net_device *dev = NULL;
	int i;

	memset(&env, 0 ,sizeof(struct mcastdsn_env));
	env.gaddr.family = env.saddr.family = env.haddr.family = family;
	addrsize = mdb_address_size(&env.gaddr);
	memcpy(env.gaddr.data, grp, addrsize);
	memcpy(env.haddr.data, hst, addrsize);
	env.compat = compat;

	if (!compat && ((MCASTDSN_RECORD_TO_IN == type) ||
	                (MCASTDSN_RECORD_TO_EX == type)) ) {
		DEBMSG("ifindex=%d family=%u group=%s type=%s"
		                 "host=%s nsrcs=%d", ifindex, family,
		                 mdb_address_to_string(&env.gaddr),
		                 mcastdsn_record_string(type),
		                 mdb_address_to_string(&env.haddr), nsrcs);
	}
	else {
		DEBMSG("ifindex=%d family=%u group=%s type=%s",
		       ifindex, family, mdb_address_to_string(&env.gaddr),
		       mcastdsn_record_string(type));
		DEBMSG("  host=%s compat=%d, nsrcs=%d",
		        mdb_address_to_string(&env.haddr), compat, nsrcs);
	}
	for (i = 0; i < nsrcs; i++) {
		memcpy(env.saddr.data, &srcs[i * addrsize], addrsize);
		DEBMSG("        srcs[%d]=%s", i, mdb_address_to_string(&env.saddr));
	}

	if (type < MCASTDSN_RECORD_MIN || type > MCASTDSN_RECORD_MAX)
		return;

	/**
	 * Get the bridge's ifindex in case this is a bridge port.
	 *
	 * - If mcastd got the report via ebtables (via family AF_BRIDGE), the ifindex
	 *   in the report refers to a bridge port.
	 * - If mcastd got the report via ip[6]tables (via family AF_INET[6]), the
	 *   ifindex in the report already refers to the bridge.
	 */
	dev = dev_get_by_index(&init_net, ifindex);
	ifindex = mcastdsn_get_ifindex_of_bridge(dev);
	dev_put(dev);
	dev = NULL;
	if (skb) {
		int bridge_port = mcastdsn_get_incoming_bridge_port(ifindex, skb);
		if (bridge_port > 0) {
			env.bridge_port = bridge_port;
		}
		else {
			return;
		}
	}

	mdb_lock();

	list_for_each_entry(env.irec, &mdb_ifaces, head) {
		if (env.irec->family == family &&
				env.irec->ifindex == ifindex)
			break;
	}
	if (&env.irec->head == &mdb_ifaces) {
		mdb_unlock();
		return;
	}
	env.iaux = mdb_iface_auxdata(env.irec, MDB_REF_SNOOPER_PRIV);

	if (!env.iaux->snooping_enabled) {
		mdb_unlock();
		return;
	}
	env.grec = mdb_group_find(env.irec, &env.gaddr);
	if (env.grec)
		env.gaux = mdb_group_auxdata(env.grec, MDB_REF_SNOOPER_PRIV);

	if (env.grec && (type == MCASTDSN_RECORD_TO_EX ||
			type == MCASTDSN_RECORD_BLOCK)) {
		/* If the ctimer is running, don't exclude or block any sources. */
		if (mcastd_timer_expires(&env.gaux->ctimer))
			nsrcs = 0;
	}

	if (env.grec && (env.grec->refs & MDB_REFMASK_SNOOPER_EX))
		match.gfilter = F_EX;
	else
		match.gfilter = F_IN;

	/* The function calls 1 or more times mcastdsn_matrix_run(..) to update
	 * the mdb.
	 *
	 * 1. First it iterates over all source records under the group, and
	 *    potentially calls mcastdsn_matrix_run(..) for each of them.
	 * 2. Then it iterates over all sources in @a srcs, and potentially calls
	 *    mcastdsn_matrix_run(..) for each of them.
	 * 3. It finishes by running mcastdsn_matrix_run(..) for the group record.
	 */

    /* 1. Iterate over source records. */
	if (env.grec) list_for_each_entry(env.srec, &env.grec->sources, head) {
		if (!(env.srec->refs & MDB_REFMASK_SNOOPER))
			continue;
		if (mdb_address_any(&env.srec->address))
			continue;
		MCASTD_ASSERT(env.srec->address.family == family);
		/* Find out if address of source record occurs in @a srcs. This list
		   comes from the IGMP/MLD message. */
		for (i = 0; i < nsrcs; i++)
			if (!memcmp(env.srec->address.data, &srcs[i * addrsize],
					addrsize))
				break;
		if (env.srec->refs & MDB_REFMASK_SNOOPER_IN)
			match.sfilter = F_IN;
		else
			match.sfilter = F_EX;
		/* Set match.sreport to R_Y if address of source record occurs in
		   source list @a srcs, else set it to R_N. */
		match.sreport = i < nsrcs ? R_Y : R_N;
		DEBMSG("Run matrix group=%s source=%s",
				mdb_address_to_string(&env.gaddr),
				mdb_address_to_string(&env.srec->address));
		mcastdsn_matrix_run(&env,
		                    mcastdsn_source_matrix[type - MCASTDSN_RECORD_MIN],
		                    &match, mcastdsn_source_action_fn);
	}

	/* 2. Iterate over @a srcs. */
	match.sfilter = F_NA;
	match.sreport = R_Y;
	for (i = 0; i < nsrcs; i++) {
		memcpy(env.saddr.data, &srcs[i * addrsize], addrsize);
		if (mdb_address_any(&env.saddr))
			continue;
		env.srec = mdb_source_find(env.grec, &env.saddr);
		if (env.srec && (env.srec->refs & MDB_REFMASK_SNOOPER))
			continue;
		DEBMSG("Run matrix group=%s source=%s",
				mdb_address_to_string(&env.gaddr),
				mdb_address_to_string(&env.saddr));
		mcastdsn_matrix_run(&env,
		                    mcastdsn_source_matrix[type - MCASTDSN_RECORD_MIN],
		                    &match, mcastdsn_source_action_fn);
	}

	/* 3. Call mcastdsn_matrix_run(..) for the group. */
	env.srec = NULL;
	match.sfilter = F_DC;
	match.sreport = R_DC;
	DEBMSG("Run matrix group=%s", mdb_address_to_string(&env.gaddr));
	mcastdsn_matrix_run(&env,
	                    mcastdsn_group_matrix[type - MCASTDSN_RECORD_MIN],
	                    &match, mcastdsn_group_action_fn);

	if (env.grec && compat) {
		mdb_group_setref(env.grec, MDB_REF_SNOOPER_PRIV);
		env.gaux->refs |= 1 << MCASTDSN_REF_CTIMER;
		mcastd_timer_schedule(&env.gaux->ctimer, mcastdsn_OHPI);
	}

#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL
	/* For now access control is only implemented for IGMP/IPv4/AF_INET. */
	if (env.grec && (AF_INET == family) && mcastdwl_get_force_igmp_fwd()) {
		unsigned short vlanid = 0;
		int ret;
		if (vlan_tx_tag_present(skb)) {
			vlanid = vlan_tx_tag_get(skb) & VLAN_VID_MASK;
		}

		if (nsrcs == 0) {
			ret = mcastdwl_whitelisted(vlanid, env.gaddr.data32[0], INADDR_ANY);
			if (ret == MCASTD_ACCESS_OK_UNAUTHORIZED) {
				env.grec->unauthorized = true;
			}
		}
		else {
			for (i = 0; i < nsrcs; i++) {
				memcpy(env.saddr.data, &srcs[i * addrsize], addrsize);
				ret = mcastdwl_whitelisted(vlanid, env.gaddr.data32[0], env.saddr.data32[0]);
				if (ret == MCASTD_ACCESS_OK_UNAUTHORIZED) {
					env.grec->unauthorized = true;
				}
			}
		}
	}
#endif

	if (env.grec) {
		mdb_commit_wrapper(env.grec, skb);
	}

	mdb_unlock();
}

void mcastdsn_register_querier(enum mcastdsn_proto proto,
                               struct mcastdsn_querier *querier)
{
	mdb_lock();
	mcastdsn_querier[proto] = querier;
	mdb_unlock();
}

void mcastdsn_unregister_querier(enum mcastdsn_proto proto)
{
	mdb_lock();
	mcastdsn_querier[proto] = NULL;
	mdb_unlock();
}

static void mcastdsn_remove_group(struct mdb_group *grec)
{
	struct mcastdsn_group *gaux;
	struct mdb_source *srec;
	struct mcastdsn_source *saux;

	list_for_each_entry(srec, &grec->sources, head) {
		if (mdb_address_any(&srec->address))
			continue;
		saux = mdb_source_auxdata(srec, MDB_REF_SNOOPER_PRIV);
		saux->nq = 0;
		saux->refs &= ~(1 << MCASTDSN_REF_SQTIMER);
		mcastdsn_source_expire(srec);
	}
	gaux = mdb_group_auxdata(grec, MDB_REF_SNOOPER_PRIV);
	gaux->nq = 0;
	if (grec->refs & MDB_REFMASK_SNOOPER_PRIV) {
		mcastd_timer_cancel(&gaux->gqtimer);
		mcastd_timer_cancel(&gaux->sqtimer);
		mcastd_timer_cancel(&gaux->ctimer);
	}
	mcastdsn_group_expire(grec);
}

#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL
/**
 * Immediately apply the removal of the whitelist entry.
 * @wl_entry: whitelist entry which will be removed imminently
 *
 * Remove all group records (and all their children) whose address is in the
 * address range allowed by the whitelist entry. Instead of really removing
 * them, declare them expired. Then mcastd will continue handling them as if
 * they expired via the 'natural' way; hence mcastd will remove them from the
 * mdb and notify all subscribers on the mdb of the changes.
 *
 * @attention The implementation assumes that the address ranges in the
 *            whitelist do not overlap.
 */
void mcastdsn_free_whlist_apply(const struct mcastdsn_whlist_entry *const wl_entry)
{
	struct mdb_iface *irec;
	struct mdb_group *grec;

	list_for_each_entry(irec, &mdb_ifaces, head) {
		if (irec->ifindex != wl_entry->ifindex) {
			// For debugging
			//DBGMSG("%s: ifindex [%d] != ifindex of wl entry [%d] => skip",
			//	irec->name, irec->ifindex, wl_entry->ifindex);
			continue;
		}
		if (irec->address.family == AF_INET6) {
			/* Implementation does not handle whitelists for IPv6 addresses */
			// For debugging
			//DBGMSG("%s: family = AF_INET6 => skip", irec->name);
			continue;
		}
		if (irec->family != wl_entry->start_addr.family) {
			// For debugging
			//DBGMSG("%s: family [%d] != family of wl entry [%d] => skip",
			//	irec->name, irec->family, wl_entry->start_addr.family);
			continue;
		}
		// For debugging
		//DBGMSG("Handle mdb_iface %s [ifindex=%d]", irec->name, irec->ifindex);
		list_for_each_entry(grec, &irec->records, head) {
			if ( (grec->address.data32[0] >= wl_entry->start_addr.data32[0]) &&
				 (grec->address.data32[0] <= wl_entry->end_addr.data32[0]) ) {
				DBGMSG("Remove grec %s", mdb_address_to_string(&grec->address));
				mcastdsn_remove_group(grec);
			}
			else {
				// For debugging
				//DBGMSG("Ignore grec %s", mdb_address_to_string(&grec->address));
			}
		}
	}
}
#endif /* CONFIG_MCASTD_CORE_ACCESS_CONTROL */

void mcastdsn_set_intf(struct mdb_iface *iface, struct mcastdsn_iface *config)
{
	struct mcastdsn_iface *iaux = mdb_iface_auxdata(iface, MDB_REF_SNOOPER_PRIV);
	iaux->snooping_enabled = config->snooping_enabled;
	iaux->querier_enabled = config->querier_enabled;
	iaux->fast_leave_enabled = config->fast_leave_enabled;
	iaux->force_compat = config->force_compat;
	iaux->querier_version = config->querier_version;
#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL
	iaux->max_groups = config->max_groups;
	iaux->upstream_rate = config->upstream_rate;
#endif
	if (!iaux->snooping_enabled || !iaux->querier_enabled
			|| !iface->ifindex
			|| mdb_address_any(&iface->address)) {
		mcastd_timer_cancel(&iaux->timer);
	} else if (mcastd_timer_state(&iaux->timer) != MCASTD_TIMER_RUNNING) {
		mcastd_timer_init(&iaux->timer, mcastdsn_iface_qtimer_cb,
		                  mcastdsn_iface_qtimer_destroy,
		                  iface, &mdb_mutex);
		iaux->sqc = mcastdsn_SQC;
		mdb_iface_setref(iface, MDB_REF_SNOOPER_PRIV);
		DEBMSG("schedule iface qtimer %d ms", iaux->sqc ? 0 : mcastdsn_QI);
		mcastd_timer_schedule(&iaux->timer, iaux->sqc ? 0 : mcastdsn_QI);
	}
}

/**
 * Structure to handle interface which went down or changed.
 * @ifindex: identifies interface which went down or changed
 */
struct mcastdsn_handle_dev_down_data
{
    int ifindex;
};

/**
 * Handle IF went down or changed.
 *
 * If a (W)LAN port or the bridge to which it belongs (and on which mcastd is
 * snooping) changed or went down, remove corresponding host entries, i.e.
 * let them expire.
 *
 * @param[in] data : object of type 'struct mcastdsn_handle_dev_down_data' with
 *     the ifindex of the interface which went down or changed.
 *
 * mcastd calls this function when it has received an event notifying an
 * interface went down or changed.
 */
static void mcastdsn_handle_dev_down_run(void *data)
{
	struct mdb_group *group = NULL;
	struct mdb_source *src = NULL;
	struct mdb_host *h = NULL;

	bool exp_host = false;
	bool exp_source = false;
	size_t cnt_source;
	size_t cnt_host;
	struct mcastdsn_handle_dev_down_data *dev_down_data = data;
	bool bridge_down; /* bridge changed or went down */

	mdb_lock();

	list_for_each_entry(group, &mdb_groups, mhead) {
		exp_source = false;
		cnt_source = 0;
		bridge_down =
			(group->iface->ifindex == dev_down_data->ifindex);
		list_for_each_entry(src, &group->sources, head) {
			cnt_source++;
			exp_host = false;
			cnt_host = 0;
			list_for_each_entry(h, &src->hosts, head) {
				cnt_host++;
				if ((h->refs & MDB_REFMASK_SNOOPER) &&
					((h->port == dev_down_data->ifindex) ||
					 bridge_down)) {
					DBGMSG("host expire due to intf down/changed event for "
							"ifindex=%d (port=%d bridge_down=%d)",
							dev_down_data->ifindex, h->port, bridge_down);
					exp_host = true;
					mcastdsn_host_expire(h);
				}
			}

			if ((exp_host) && (cnt_host == 1)) {
				DBGMSG("source expire due to only host expires");

				exp_source = true;
				
				/* this part is taken from mcastdsn_remove_group */
				if (!mdb_address_any(&src->address)) {
					struct mcastdsn_source *saux = mdb_source_auxdata(src, MDB_REF_SNOOPER_PRIV);
					saux->nq = 0;
					saux->refs &= ~(1 << MCASTDSN_REF_SQTIMER);
					mcastdsn_source_expire(src);
				}
			}
		}

		if ((exp_source) && (cnt_source == 1)) {
			DBGMSG("group expire due to only source expires");
			mcastdsn_remove_group(group);
		}
	}

	mdb_unlock();
}

/**
 * mcastd snooper has finished handling intf down/changed event.
 *
 * @param[in] data : object of type 'struct mcastdsn_handle_dev_down_data' with
 *     the ifindex of the interface which went down or changed.
 */
static void mcastdsn_handle_dev_down_destroy(void *data)
{
	kfree((struct mcastdsn_handle_dev_down_data *)data);
}

/**
 * netdevice notifier callback
 */
static int mcastdsn_netdev_notifier(struct notifier_block *nb,
                                    unsigned long event, void *ptr)
{
	struct net_device *dev = NULL;

	if (s_shutting_down)
		return NOTIFY_OK;

#if LINUX_VERSION_CODE < KERNEL_VERSION(3,11,0)
	dev = (struct net_device *)ptr;
#else
	dev = netdev_notifier_info_to_dev(ptr);
#endif

	if (unlikely(!dev))
		return NOTIFY_OK;

	{
		struct mcastd_work work;
		struct mcastdsn_handle_dev_down_data *work_data;

		switch (event)
		{
		case NETDEV_DOWN:
		case NETDEV_GOING_DOWN:
		case NETDEV_CHANGE:
			DBGMSG("%s for %s ifindex=%d",  netdev_cmd_to_name(event),
					dev->name, dev->ifindex);
			work.run     = mcastdsn_handle_dev_down_run;
			work.destroy = mcastdsn_handle_dev_down_destroy;
			work_data = kmalloc(sizeof(*work_data), GFP_ATOMIC);
			if (work_data) {
				work_data->ifindex = dev->ifindex;
				work.data = work_data;
				mcastd_work_schedule(&work, GFP_ATOMIC);
			}
			break;
		default:
			break;
		}
	}

	return NOTIFY_OK;
}

static struct notifier_block netdev_notifier = {
	.notifier_call = mcastdsn_netdev_notifier,
};

static struct mdb_auxinfo mcastdsn_ifaceinfo = {
	.level = MDB_LEVEL_IFACE,
	.ref = MDB_REF_SNOOPER_PRIV,
	.name = "snooper",
	.size = sizeof(struct mcastdsn_iface),
	.data2var = mcastdsn_iface2var
};

static struct mdb_auxinfo mcastdsn_groupinfo = {
	.level = MDB_LEVEL_GROUP,
	.ref = MDB_REF_SNOOPER_PRIV,
	.name = "snooper",
	.size = sizeof(struct mcastdsn_group),
	.data2var = mcastdsn_group2var
};

static struct mdb_auxinfo mcastdsn_sourceinfo = {
	.level = MDB_LEVEL_SOURCE,
	.ref = MDB_REF_SNOOPER_PRIV,
	.name = "snooper",
	.size = sizeof(struct mcastdsn_source),
	.data2var = mcastdsn_source2var
};

static struct mdb_auxinfo mcastdsn_hostinfo = {
	.level = MDB_LEVEL_HOST,
	.ref = MDB_REF_SNOOPER_PRIV,
	.name = "snooper",
	.size = sizeof(struct mcastdsn_host),
	.data2var = mcastdsn_host2var
};

/*
 * Initialize the snooper module
 *
 * The function:
 * - initializes parameters and timer values for the protocols IGMP and MLD.
 * - registers auxiliary data with the mdb (for all levels: interface, group,
 *   source, host). It uses MDB_REF_SNOOPER_PRIV as module identifier.
 * - registers a netdevice notifier to become aware netdev events
 */
int mcastd_snooper_init(void)
{
	mcastdsn_RV = 2;
	mcastdsn_QI = 125000;
	mcastdsn_QRI = 10000;
	mcastdsn_GMI = mcastdsn_RV * mcastdsn_QI + mcastdsn_QRI;
	mcastdsn_SQI = mcastdsn_QI / 4;
	mcastdsn_SQC = mcastdsn_RV;
	mcastdsn_LMQI = 1000;
	mcastdsn_LMQC = mcastdsn_RV;
	mcastdsn_LMQT = mcastdsn_LMQI * mcastdsn_LMQC;
	mcastdsn_OHPI = mcastdsn_RV * mcastdsn_QI + mcastdsn_QRI;
	mdb_lock();
	mdb_register_aux(&mcastdsn_ifaceinfo);
	mdb_register_aux(&mcastdsn_groupinfo);
	mdb_register_aux(&mcastdsn_sourceinfo);
	mdb_register_aux(&mcastdsn_hostinfo);
	mdb_unlock();

	register_netdevice_notifier(&netdev_notifier);

	return 0;
}

void mcastd_snooper_cleanup(void)
{
	struct mdb_iface *irec;
	struct mcastdsn_iface config;
	struct mdb_group *grec;

	s_shutting_down = true;
	unregister_netdevice_notifier(&netdev_notifier);

	memset(&config, 0, sizeof(struct mcastdsn_iface));
	mdb_lock();
	list_for_each_entry(irec, &mdb_ifaces, head) {
		list_for_each_entry(grec, &irec->records, head) {
			mcastdsn_remove_group(grec);
		}
		mcastdsn_set_intf(irec, &config);
	}
	mdb_unregister_aux(&mcastdsn_ifaceinfo);
	mdb_unregister_aux(&mcastdsn_groupinfo);
	mdb_unregister_aux(&mcastdsn_sourceinfo);
	mdb_unregister_aux(&mcastdsn_hostinfo);
	mdb_unlock();
}
