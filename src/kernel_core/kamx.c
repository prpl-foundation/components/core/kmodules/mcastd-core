/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <linux/module.h>
#include <linux/netlink.h>
#include <linux/list.h>
#include <linux/errno.h>
#include <linux/string.h>
#include <linux/slab.h>

#include <mcastd/kernel/kamx.h>

#define IDENT "core"
#include <mcastd/kernel/debug.h>

struct kamx_objh {
	struct list_head head;
	const char *path;
	mcastdnl_input_cb input;
};

struct kamx_funh {
	struct list_head head;
	const char *path;
	const char *name;
	mcastdnl_input_cb input;
};

static LIST_HEAD(kamx_objh);
static LIST_HEAD(kamx_funh);

static int kamx_path_match(const char *pattern, const char *path)
{
	for (; *pattern || *path; pattern++, path++) {
		if (*pattern == '*') {
			while (*path && *path != '.')
				path++;
			path--;
		} else if (*pattern != *path) {
			return 0;
		}
	}
	return 1;
}

static int kamx_object_input(struct nlmsghdr *nlh)
{
	struct kvar *root;
	struct kvar *path;
	struct kamx_objh *object;
	if (!nlh->nlmsg_pid)
		return 0; // ignore looped msgs silently
	if (unlikely(kamx_sanity_check(nlh))) {
		DEBMSG("Message sanity check failed!");
		return -EINVAL;
	}
	root = NLMSG_DATA(nlh);
	path = kvar_list_val(root, KAMX_OBJECT_PROPERTY_PATH);
	if (unlikely(!path || path->hdr.type != KVARTYPE_STRING)) {
		DEBMSG("Bad message format: No path present");
		return -EINVAL;
	}
	list_for_each_entry(object, &kamx_objh, head) {
		if (!kamx_path_match(object->path, path->val.s))
			continue;
		if (unlikely(!object->input))
			continue;
		object->input(nlh);
	}
	return 0;
}

static int kamx_function_input(struct nlmsghdr *nlh)
{
	struct kvar *root;
	struct kvar *path;
	struct kvar *name;
	struct kamx_funh *function;
	if (!nlh->nlmsg_pid)
		return 0; // ignore looped msgs silently
	if (unlikely(kamx_sanity_check(nlh))) {
		DEBMSG("Message sanity check failed!");
		return -EINVAL;
	}
	root = NLMSG_DATA(nlh);
	path = kvar_list_val(root, KAMX_FCALL_PROPERTY_PATH);
	if (unlikely(!path || path->hdr.type != KVARTYPE_STRING)) {
		DEBMSG("Bad message format: No path present");
		return -EINVAL;
	}
	name = kvar_list_val(root, KAMX_FCALL_PROPERTY_NAME);
	if (unlikely(!name || name->hdr.type != KVARTYPE_STRING)) {
		DEBMSG("Bad message format: No function name present");
		return -EINVAL;
	}
	list_for_each_entry(function, &kamx_funh, head) {
		if (!kamx_path_match(function->path, path->val.s))
			continue;
		if (strcmp(function->name, name->val.s))
			continue;
		if (unlikely(!function->input))
			continue;
		return function->input(nlh);
	}
	return -ENOTSUPP;
}

int mcastd_object_register(const char *path, mcastdnl_input_cb input)
{
	struct kamx_objh *object;
	int ret = 0;
	mcastdnl_lock();
	object = kmalloc(sizeof(struct kamx_objh), GFP_KERNEL);
	if (!object) {
		ERRMSG("kmalloc() failed");
		ret = -ENOMEM;
		goto leave;
	}
	object->path = path;
	object->input = input;
	list_add_tail(&object->head, &kamx_objh);
leave:
	mcastdnl_unlock();
	return ret;
}

int mcastd_object_unregister(const char *path, mcastdnl_input_cb input)
{
	struct kamx_objh *object;
	int ret = 0;
	mcastdnl_lock();
	list_for_each_entry(object, &kamx_objh, head) {
		if (strcmp(object->path, path))
			continue;
		if (object->input != input)
			continue;
		break;
	}
	if (&object->head == &kamx_objh) {
		ERRMSG("Object registration not found: %s", path);
		ret = -ENOENT;
		goto leave;
	}
	list_del(&object->head);
	kfree(object);
leave:
	mcastdnl_unlock();
	return ret;
}

int mcastd_function_register(const char *path, const char *name,
                             mcastdnl_input_cb input)
{
	struct kamx_funh *function;
	int ret = 0;
	mcastdnl_lock();
	function = kmalloc(sizeof(struct kamx_funh), GFP_KERNEL);
	if (!function) {
		ERRMSG("kmalloc() failed");
		ret = -ENOMEM;
		goto leave;
	}
	function->path = path;
	function->name = name;
	function->input = input;
	list_add_tail(&function->head, &kamx_funh);
leave:
	mcastdnl_unlock();
	return ret;
}

int mcastd_function_unregister(const char *path, const char *name,
                               mcastdnl_input_cb input)
{
	struct kamx_funh *function;
	int ret = 0;
	mcastdnl_lock();
	list_for_each_entry(function, &kamx_funh, head) {
		if (strcmp(function->path, path))
			continue;
		if (strcmp(function->name, name))
			continue;
		if (function->input != input)
			continue;
		break;
	}
	if (&function->head == &kamx_funh) {
		ERRMSG("Function registration not found: %s.%s()", path, name);
		ret = -ENOENT;
		goto leave;
	}
	list_del(&function->head);
	kfree(function);
leave:
	mcastdnl_unlock();
	return ret;
}

int mcastd_kamx_init(void)
{
	mcastdnl_register(MCASTDNL_TYPE_NEWOBJ, kamx_object_input);
	mcastdnl_register(MCASTDNL_TYPE_DELOBJ, kamx_object_input);
	mcastdnl_register(MCASTDNL_TYPE_FCALL, kamx_function_input);
	return 0;
}

void mcastd_kamx_cleanup(void)
{
	struct kamx_objh *object;
	struct kamx_funh *function;
	mcastdnl_unregister(MCASTDNL_TYPE_NEWOBJ);
	mcastdnl_unregister(MCASTDNL_TYPE_DELOBJ);
	mcastdnl_unregister(MCASTDNL_TYPE_FCALL);
	mcastdnl_lock();
	while (!list_empty(&kamx_objh)) {
		object = (struct kamx_objh *)kamx_objh.next;
		ERRMSG("Late cleanup of object handler for %s at %p.",
		       object->path, object->input);
		list_del(&object->head);
		kfree(object);
	}
	while (!list_empty(&kamx_funh)) {
		function = (struct kamx_funh *)kamx_funh.next;
		ERRMSG("Late cleanup of function handler for %s.%s() at %p.",
		       function->path, function->name, function->input);
		list_del(&function->head);
		kfree(function);
	}
	mcastdnl_unlock();
}

EXPORT_SYMBOL(mcastd_object_register);
EXPORT_SYMBOL(mcastd_object_unregister);
EXPORT_SYMBOL(mcastd_function_register);
EXPORT_SYMBOL(mcastd_function_unregister);

