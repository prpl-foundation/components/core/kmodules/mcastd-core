/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __MCASTD_KERNEL_WHITELIST_H__
#define __MCASTD_KERNEL_WHITELIST_H__

#ifdef CONFIG_MCASTD_CORE_ACCESS_CONTROL

#include <linux/types.h>
#include <mcastd/kernel/mdb.h> /* struct mdb_address */

/**
 * struct mcastdwl_whlist_entry - whitelist entry
 * @head: member to add entry in a list
 * @ifindex: ifindex of the IF to which the entry belongs. This is typically
 *           the ifindex of the LAN bridge.
 * @id: ID of the entry
 * @vlanid: VLAN ID of the MC stream. The HGW should check on the WAN side if
 *          the MC stream arrives on the correct VLAN. Because mcastd snoops on
 *          the LAN side, mcastd can not check if the VLAN ID is correct. That's
 *          also why mcastdwl_whitelisted() ignores vlanid.
 * @bw: imputed group bandwidth in bps
 * @start_addr: start address of allowed MC range
 * @end_addr: end address of allowed MC range
 * @src_addr: allowed source address; 0.0.0.0 means all sources allowed
 * @rcu: to allow for rcu operations
 */
struct mcastdwl_whlist_entry {
	struct list_head head;
	int ifindex;
	__u16 id;
	__u16 vlanid;
	__u32 bw;
	struct mdb_address start_addr;
	struct mdb_address end_addr;
	struct mdb_address src_addr;
	struct rcu_head rcu;
};

/*
 * Functions called from whitelist_amx.c
 */

int mcastdwl_set_whitelist(const struct mdb_iface *const iface, __u16 id,
                           __u16 vlan_id, __u8 *start_addr, __u8 *end_addr,
                           __u8 *src_addr, __u32 grp_bw);
void mcastdwl_clear_whitelist(const struct mdb_iface *const iface, __u16 id);
void mcastdwl_flush_whitelist(const struct mdb_iface *const iface);

void mcastdwl_get_whitelist(const struct mdb_iface *const iface,
                            struct list_head *ret_list);
void mcastdwl_free_whitelist(struct list_head *list);
void mcastdwl_set_force_igmp_fwd(bool force_igmp_fwd);

/*
 * Functions called from snooper.c, snooper_igmp.c and bwctrl.c.
 */

int mcastdwl_whitelist_empty(void);
int mcastdwl_whitelisted(__u16 vlanid, __be32 addr, __be32 src_addr);
__u32 mcastdwl_lookup_bw(__be32 addr);
bool mcastdwl_get_force_igmp_fwd(void);

#endif /* CONFIG_MCASTD_CORE_ACCESS_CONTROL */

#endif /* __MCASTD_KERNEL_WHITELIST_H__ */
